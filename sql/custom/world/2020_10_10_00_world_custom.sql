--
DROP TABLE IF EXISTS `rank_titles`;
CREATE TABLE `rank_titles` (
`rank`  int(11) UNSIGNED NOT NULL ,
`title`  text(0) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
PRIMARY KEY (`rank`));

DELETE FROM `rank_titles` WHERE `rank` IN (1, 5, 10);
INSERT INTO `rank_titles` (`rank`, `title`) VALUES
(1, 'Воин'),
(5, 'Рыцарь'),
(10, 'Граф');
