--
-- Titles menu(menu = 59015(SALE))
DELETE FROM `gossip_menu` WHERE `MenuID` = 59015;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(59015, 75010, 0);

DELETE FROM `npc_text` WHERE `ID` = 75010;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (75010, 'In this section, you can purchase titles. Each rank is worth 200 |TInterface\\Icons\\almaz:20:20:-3:-3|t', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 75010;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(75010, 'ruRU', 'В данном разделе вы сможете приобрести звания. Каждое звание стоит 200 |TInterface\\Icons\\almaz:20:20:-3:-3|t', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59015;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59015, 0, 0, 'Gladiator', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 1, 0, 'The ruthless Gladiator', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 2, 0, 'The Vengeful Gladiator', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 3, 0, 'The Exalted One', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 4, 0, 'The Cruel Gladiator', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 5, 0, 'Lord of the Arena', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 6, 0, 'The Sea Devil', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 7, 0, 'Tykver', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 8, 0, 'All-conquering', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 9, 0, 'Hop Maker', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 10, 0, 'Madly In love', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 11, 0, 'The benefactor', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 12, 0, 'Wonderful', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 13, 0, 'The Deadly Gladiator', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 14, 0, 'Angry Gladiator', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 15, 0, 'The Wanderer', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 16, 0, 'The Relentless Gladiator', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 17, 0, 'The Angry Gladiator', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59015, 18, 0, '<- Prev page', 0, 1, 1, 59013, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59015;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59015, 0, 'ruRU', 'Гладиатор', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 1, 'ruRU', 'Безжалостный гладиатор', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 2, 'ruRU', 'Мстительный гладиатор', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 3, 'ruRU', 'Превозносимый', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 4, 'ruRU', 'Жестокий гладиатор', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 5, 'ruRU', 'Повелитель арены', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 6, 'ruRU', 'Морской дьявол', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 7, 'ruRU', 'Тыквер', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 8, 'ruRU', 'Всепобеждающий', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 9, 'ruRU', 'Хмелевар', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 10, 'ruRU', 'Безумно Влюбленный', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 11, 'ruRU', 'Благодетель.', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 12, 'ruRU', 'Чудесный', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 13, 'ruRU', 'Смертоносный гладиатор', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 14, 'ruRU', 'Гневный гладиатор', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 15, 'ruRU', 'Странник', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 16, 'ruRU', 'Неумолимый гладиатор', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 17, 'ruRU', 'Разгневанный гладиатор', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59015, 18, 'ruRU', '<- Назад', NULL);
