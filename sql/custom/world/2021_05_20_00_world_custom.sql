--
-- Cleanup SmartAI stuff
UPDATE `creature_template` SET `AIName` = '' WHERE `entry` = 70000;
DELETE FROM `smart_scripts` WHERE `entryorguid` = 70000 AND `source_type` = 0;
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 15 AND `SourceGroup` = 59000;

-- Rewrite gossip options
DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59000;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59000, 0, 0, 'Info about server.', 0, 1, 1, 59001, 0, 0, 0, 'Are you sure you want to teleport to your class area?', 0, 0),
(59000, 1, 0, 'Teleport to the area of your class.', 0, 1, 1, 0, 0, 0, 0, NULL, 0, 0),
(59000, 2, 0, 'PvP-stats.', 0, 1, 1, 59016, 0, 0, 0, NULL, 0, 0),
(59000, 3, 0, 'Payable skins system.', 0, 1, 1, 59010, 0, 0, 0, NULL, 0, 0),
(59000, 4, 0, '|cff0000ffShop|r.', 0, 1, 1, 59013, 0, 0, 0, NULL, 0, 0);

-- Rewrite gossip option locales
DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59000;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59000, 0, 'ruRU', 'Информация о сервере.', NULL),
(59000, 1, 'ruRU', 'Телепортироваться в зону вашего класса.', 'Вы действительно хотите телепортироваться в зону своего класса?'),
(59000, 2, 'ruRU', 'PvP-статистика.', NULL),
(59000, 3, 'ruRU', 'Система внешнего вида.', NULL),
(59000, 4, 'ruRU', '|cff0000ffМагазин|r.', NULL);

-- Add error creature texts
DELETE FROM `creature_text` WHERE `CreatureID` = 70000 AND `GroupID` IN (29, 30, 31, 32);
INSERT INTO `creature_text` (`CreatureID`, `GroupID`, `ID`, `Text`, `Type`,`Language`, `Probability`, `Emote`, `Duration`, `Sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES
(70000, 29, 0, 'You cannot do this in combat.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 30, 0, 'You cannot do this in the battleground.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 31, 0, 'You cannot do this in the arena.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 32, 0, 'You are dead and you cannot do this.', 15, 0, 100, 0, 0, 0, 0, 0, '');

-- Add locales for error creature texts
DELETE FROM `creature_text_locale` WHERE `CreatureID` = 70000 AND `GroupID` IN (29, 30, 31, 32);
INSERT INTO `creature_text_locale` (`CreatureID`, `GroupID`, `ID`, `Locale`, `Text`) VALUES
(70000, 29, 0, 'ruRU', 'Вы не можете сделать этого в бою.'),
(70000, 30, 0, 'ruRU', 'Вы не можете сделать этого на поле боя.'),
(70000, 31, 0, 'ruRU', 'Вы не можете сделать этого на арене.'),
(70000, 32, 0, 'ruRU', 'Вы мертвы и не можете этого сделать.');
