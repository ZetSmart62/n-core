--
DROP TABLE IF EXISTS `payable_skin_template`;
CREATE TABLE `payable_skin_template` (
`id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
`name` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' ,
`displayId`  mediumint(8) UNSIGNED NOT NULL ,
`auras`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`spells`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ,
`classmask`  int(10) UNSIGNED NOT NULL DEFAULT 0,
`gender`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
`price`  int(10) UNSIGNED NOT NULL ,
PRIMARY KEY (`id`));

DROP TABLE IF EXISTS `payable_skin_creature_vendors`;
CREATE TABLE `payable_skin_creature_vendors` (
`npcEntryOrGuid` mediumint(8) NOT NULL,
`payableSkin` int(10) UNSIGNED NOT NULL,
PRIMARY KEY (`npcEntryOrGuid`));
