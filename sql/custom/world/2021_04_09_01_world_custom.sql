--
DELETE FROM `trinity_string` WHERE `entry` BETWEEN 30033 AND 30036;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc8`) VALUES
(30033, '[Success]', '[Успех]'),
(30034, '[Error]', '[Ошибка]'),
(30035, '[|cff008000Success|r]', '[|cff008000Успех|r]'),
(30036, '[|cffff0000Error|r]', '[|cffff0000Ошибка|r]');

--

DELETE FROM `trinity_string` WHERE `entry` IN (30005, 30006, 30007, 30009, 30010, 30011, 30013, 30014, 30016, 30018);
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc8`) VALUES
(30005, '%s This skin (ID: %u) does not exist.', '%s Такого скина (ID: %u) не существует.'),
(30006, '%s Player %s has no skin %s.', '%s У игрока %s нет скина %s.'),
(30007, '%s Skin %s is already activated for player %s.', '%s Скин %s уже активирован у игрока %s.'),
(30009, '%s You have activated skin %s for player %s.', '%s Вы активировали скин %s игроку %s.'),
(30010, '%s Skin %s is not available for player %s. Use .custom skin add force to add forcibly.', '%s Скин %s недоступен игроку %s. Используйте .custom skin add force, чтобы добавить принудительно.'),
(30011, '%s Player %s already has skin %s.', '%s Игрок %s уже имеет скин %s.'),
(30013, '%s You added skin %s to player %s.', '%s Вы добавили скин %s игроку %s.'),
(30014, '%s Player %s has no active skin.', '%s У игрока %s нет активного скина.'),
(30016, '%s You have reset active skin for player %s.', '%s Вы сбросили активный скин игроку %s.'),
(30018, '%s You have removed skin %s for player %s.', '%s Вы удалили скин %s у игрока %s.');
