--
DELETE FROM `creature_text` WHERE `CreatureID` = 70000;
INSERT INTO `creature_text` (`CreatureID`, `GroupID`, `ID`, `Text`, `Type`,`Language`, `Probability`, `Emote`, `Duration`, `Sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES
(70000, 0, 0, 'You have successfully activated the skin.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 1, 0, 'You have already activated this skin.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 2, 0, 'Your skin has been successfully reset.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 3, 0, 'You don\'t have an active skin.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 4, 0, 'Unfortunately, you don\'t have enough diamonds.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 5, 0, 'You have successfully purchased a character rename service. To use it, you need to re-enter the game.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 6, 0, 'You already have a character rename service. To use it, you need to re-enter the game.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 7, 0, 'You have successfully purchased a character change race service. To use it, you need to re-enter the game.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 8, 0, 'You already have a character change race service. To use it, you need to re-enter the game.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 9, 0, 'You have successfully purchased a character level boost service. You are now level cap, congratulations!', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 10, 0, 'Your character has already reached the maximum level.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 11, 0, 'You have successfully purchased an unbind instances service. All instance saves unbinded from your character.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 12, 0, 'You have successfully purchased arena points.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 13, 0, 'You already have too many arena points.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 14, 0, 'You have successfully purchased honor points.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 15, 0, 'You already have too many honor points.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 16, 0, 'You have successfully purchased the profession skill increase service.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 17, 0, 'You do not have this profession.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 18, 0, 'You already have the maximum skill level for this profession.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 19, 0, 'You have successfully purchased a title.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 20, 0, 'You already have this title.', 15, 0, 100, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_text_locale` WHERE `CreatureID` = 70000;
INSERT INTO `creature_text_locale` (`CreatureID`, `GroupID`, `ID`, `Locale`, `Text`) VALUES
(70000, 0, 0, 'ruRU', 'Вы успешно применили скин.'),
(70000, 1, 0, 'ruRU', 'Вы уже активировали этот скин.'),
(70000, 2, 0, 'ruRU', 'Ваш скин был успешно сброшен.'),
(70000, 3, 0, 'ruRU', 'У Вас нет активного скина.'),
(70000, 4, 0, 'ruRU', 'К сожалению у вас недостаточно алмазов.'),
(70000, 5, 0, 'ruRU', 'Вы успешно приобрели услугу смены имени персонажа. Чтобы воспользоваться ею, вам нужно перезайти в игровой мир.'),
(70000, 6, 0, 'ruRU', 'У вас уже есть услуга смены имени персонажа. Чтобы воспользоваться ею, вам нужно перезайти в игровой мир.'),
(70000, 7, 0, 'ruRU', 'Вы успешно приобрели услугу смены расы персонажа. Чтобы воспользоваться ею, вам нужно перезайти в игровой мир.'),
(70000, 8, 0, 'ruRU', 'У вас уже есть услуга смены расы персонажа. Чтобы воспользоваться ею, вам нужно перезайти в игровой мир.'),
(70000, 9, 0, 'ruRU', 'Вы успешно приобрели услугу повышения уровня персонажа. Теперь у вас максимальный уровень, поздравляем!'),
(70000, 10, 0, 'ruRU', 'Ваш персонаж уже достиг максимального уровня.'),
(70000, 11, 0, 'ruRU', 'Вы успешно приобрели услугу сброса сохранения подземелий. Все сохранения откреплены от вашего персонажа.'),
(70000, 12, 0, 'ruRU', 'Вы успешно приобрели очки арены.'),
(70000, 13, 0, 'ruRU', 'У вас уже есть слишком много очков арены.'),
(70000, 14, 0, 'ruRU', 'Вы успешно приобрели очки чести.'),
(70000, 15, 0, 'ruRU', 'У вас уже есть слишком много очков чести.'),
(70000, 16, 0, 'ruRU', 'Вы успешно приобрели услугу повышения навыка профессии.'),
(70000, 17, 0, 'ruRU', 'У вас нет этой профессии.'),
(70000, 18, 0, 'ruRU', 'Вы уже имеете максимальный уровень навыка для этой профессии.'),
(70000, 19, 0, 'ruRU', 'Вы успешно приобрели звание.'),
(70000, 20, 0, 'ruRU', 'Вы уже имеете это звание.');


UPDATE `gossip_menu_option` SET `OptionText` = '|cff0000ffShop|r.' WHERE `MenuID` = 59000 AND `OptionID` = 3;
