--
DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59000 AND `OptionID` IN (1);
DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59000 AND `OptionID` IN (1);

INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59000, 1, 0, 'Teleport to the area of your class.', 0, 1, 1, 0, 0, 0, 0, NULL, 0, 0);

INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59000, 1, 'ruRU', 'Телепортироваться в зону вашего класса.', NULL);
