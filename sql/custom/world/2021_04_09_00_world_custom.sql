--
DROP TABLE IF EXISTS `payable_skin_template`;
CREATE TABLE `payable_skin_template` (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0',
  `nameColor` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `displayId` mediumint UNSIGNED NOT NULL,
  `auras` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `spells` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `classmask` int UNSIGNED NOT NULL DEFAULT 0,
  `gender` tinyint UNSIGNED NOT NULL DEFAULT 0,
  `price` int UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
);

INSERT INTO `payable_skin_template` (`id`, `name`, `nameColor`, `displayId`, `auras`, `spells`, `classmask`, `gender`, `price`)  VALUES
(1, 'Алекстраза', 'ff00ff00', 33000, '36160', NULL, 384, 2, 599),
(2, 'Араккоа', 'ffda70d6', 33002, '36161', '79405 79406 79407', 33, 0, 899),
(3, 'Артас', 'ffffffff', 33003, NULL, NULL, 51, 1, 249),
(4, 'Лич Кинг', 'ffda70d6', 22235, '36161', '79408 79409 79411', 51, 1, 899),
(5, 'Кровавая королева Лана\'тель', 'ffda70d6', 33004, '36161', '79412 79413 79414', 400, 2, 899),
(6, 'Клейтон Дабин Младший', 'ff00ff00', 24104, '36160', NULL, 412, 0, 599),
(7, 'Презренный-костолом', 'ffffffff', 15513, NULL, NULL, 412, 1, 249);
