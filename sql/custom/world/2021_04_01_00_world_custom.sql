--
DELETE FROM `trinity_string` WHERE `entry` BETWEEN 30005 AND 30023;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc8`) VALUES
(30005, '[|cffff0000Error|r] This skin (ID: %u) does not exist.', '[|cffff0000Ошибка|r] Такого скина (ID: %u) не существует.'),
(30006, '[|cffff0000Error|r] Player %s has no skin %s.', '[|cffff0000Ошибка|r] У игрока %s нет скина %s.'),
(30007, '[|cffff0000Error|r] Skin %s is already activated for player %s.', '[|cffff0000Ошибка|r] Скин %s уже активирован у игрока %s.'),
(30008, '%s activated skin %s for you.', '%s активировал Вам скин %s.'),
(30009, '[|cff008000Success|r] You have activated skin %s for player %s.', '[|cff008000Успех|r] Вы активировали скин %s игроку %s.'),
(30010, '[|cffff0000Error|r] Skin %s is not available for player %s. Use .custom skin add force to add forcibly.', '[|cffff0000Ошибка|r] Скин %s недоступен игроку %s. Используйте .custom skin add force, чтобы добавить принудительно.'),
(30011, '[|cffff0000Error|r] Player %s already has skin %s.', '[|cffff0000Ошибка|r] Игрок %s уже имеет скин %s.'),
(30012, '%s added you skin %s.', '%s добавил вам скин %s.'),
(30013, '[|cff008000Success|r] You added skin %s to player %s.', '[|cff008000Успех|r] Вы добавили скин %s игроку %s.'),
(30014, '[|cffff0000Error|r] Player %s has no active skin.', '[|cffff0000Ошибка|r] У игрока %s нет активного скина.'),
(30015, '%s reset your active skin.', '%s сбросил вам активный скин.'),
(30016, '[|cff008000Success|r] You have reset active skin for player %s.', '[|cff008000Успех|r] Вы сбросили активный скин игроку %s.'),
(30017, '%s removed your skin %s.', '%s удалил вам скин %s.'),
(30018, '[|cff008000Success|r] You have removed skin %s for player %s.', '[|cff008000Успех|r] Вы удалили скин %s у игрока %s.'),
(30019, 'Player %s has no skins', 'У игрока %s нет скинов.'),
(30020, 'List of skins of player %s:', 'Список скинов игрока %s:'),
(30021, ' * %s%s', ' * %s%s'),
(30022, '%u - %s%s%s', '%u - %s%s%s'),
(30023, 'No skins found.', 'Скинов не найдено.');
