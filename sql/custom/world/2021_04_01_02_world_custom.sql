--
DELETE FROM `trinity_string` WHERE `entry` BETWEEN 30024 AND 30032;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc8`) VALUES
(30024, '### USAGE: .custom lookup skin <name part>\nLooks up a payable skins by <name part> and returns all matches with their ID\'s.', '### ИСПОЛЬЗОВАНИЕ: .custom lookup skin <name part>\nИщет скины по части названия и выводит все совпадения с их ID\'ами.'),
(30025, '### USAGE: .custom reload all\nReload all custom systems.', '### ИСПОЛЬЗОВАНИЕ: .custom reload all\nПерезагружает все кастомные системы.'),
(30026, '### USAGE: .custom reload ranks\nReload custom ranks system.', '### ИСПОЛЬЗОВАНИЕ: .custom reload ranks\nПерезагружает кастомную систему рангов.'),
(30027, '### USAGE: .custom reload skins\nReload custom payable skins system.', '### ИСПОЛЬЗОВАНИЕ: .custom reload skins\nПерезагружает кастомную систему скинов.'),
(30028, '### USAGE: .custom skin activate [<player>] <payableSkinId>', '### ИСПОЛЬЗОВАНИЕ: .custom skin activate [<player>] <payableSkinId>\nАктивирует указанный скин себе, цели или указанному игроку.'),
(30029, '### USAGE: .custom skin add [force] [<player>] <payableSkinId>', '### ИСПОЛЬЗОВАНИЕ: .custom skin add [force] [<player>] <payableSkinId>\nДобавляет указанный скин себе, цели или указанному игроку. Используйте force, чтобы игнорировать недоступность скина игроку.'),
(30030, '### USAGE: .custom skin deactivate [<player>]', '### ИСПОЛЬЗОВАНИЕ: .custom skin deactivate [<player>]\nСбрасывает скин себе, цели или указанному игроку.'),
(30031, '### USAGE: .custom skin list [<player>]', '### ИСПОЛЬЗОВАНИЕ: .custom skin list [<player>]\nВыводит список скинов вашего персонажа, цели или указанного игрока.'),
(30032, '### USAGE: .custom skin remove [<player>] <payableSkinId>', '### ИСПОЛЬЗОВАНИЕ: .custom skin remove [<player>] <payableSkinId>\nУдаляет указанный скин у вас, цели или указанного игрока');
