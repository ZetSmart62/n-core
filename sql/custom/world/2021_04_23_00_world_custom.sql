--
DELETE FROM `trinity_string` WHERE `entry` = 30038;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc8`) VALUES
(30038, 'PvP-stats of player %s:$b$bRank: %s$bWinrate: %u/%u (%.2f%%)', 'PvP-статистика игрока %s:$b$bРанг: %s$bПроцент побед: %u/%u (%.2f%%)');

--

DELETE FROM `creature_text` WHERE `CreatureID` = 70000 AND `GroupID` BETWEEN 21 AND 25;
INSERT INTO `creature_text` (`CreatureID`, `GroupID`, `ID`, `Text`, `Type`,`Language`, `Probability`, `Emote`, `Duration`, `Sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES
(70000, 21, 0, 'The character was not found. Make sure you select a player.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 22, 0, 'A character with that name was not found.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 23, 0, 'You need to wait another %u sec.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 24, 0, 'You are not in a group.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 25, 0, 'You are not a member of the guild.', 15, 0, 100, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_text_locale` WHERE `CreatureID` = 70000 AND `GroupID` BETWEEN 21 AND 25;
INSERT INTO `creature_text_locale` (`CreatureID`, `GroupID`, `ID`, `Locale`, `Text`) VALUES
(70000, 21, 0, 'ruRU', 'Персонаж не найден. Удостоверьтесь, что вы выбрали игрока.'),
(70000, 22, 0, 'ruRU', 'Персонаж с таким именем не найден.'),
(70000, 23, 0, 'ruRU', 'Нужно подождать еще %u сек.'),
(70000, 24, 0, 'ruRU', 'Вы не состоите в группе.'),
(70000, 25, 0, 'ruRU', 'Вы не являетесь членом гильдии.');

--

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59016;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59016, 0, 0, 'Show my PvP-stats.', 0, 1, 1, 0, 0, 0, 0, NULL, 0, 0),
(59016, 1, 0, 'Show PvP-stats of my target.', 0, 1, 1, 0, 0, 0, 0, NULL, 0, 0),
(59016, 2, 0, 'Show PvP-stats of another player.', 0, 1, 1, 0, 0, 1, 0, NULL, 0, 0),
(59016, 3, 0, '<- Prev page', 0, 1, 1, 59000, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59016;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59016, 0, 'ruRU', 'Показать мою PvP-статистику.', NULL),
(59016, 1, 'ruRU', 'Показать PvP-статистику моей цели.', NULL),
(59016, 2, 'ruRU', 'Показать PvP-статистику другого игрока.', NULL),
(59016, 3, 'ruRU', '<- Назад', NULL);

--

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59017;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59017, 0, 0, 'Share to /say channel', 0, 1, 1, 0, 0, 0, 0, NULL, 0, 0),
(59017, 1, 0, 'Share to /group channel', 0, 1, 1, 0, 0, 0, 0, NULL, 0, 0),
(59017, 2, 0, 'Share to /guild channel', 0, 1, 1, 0, 0, 0, 0, NULL, 0, 0),
(59017, 3, 0, '<- Prev page', 0, 1, 1, 0, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59017;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59017, 0, 'ruRU', 'Поделиться в канал /сказать', NULL),
(59017, 1, 'ruRU', 'Поделиться в канал /группа', NULL),
(59017, 2, 'ruRU', 'Поделиться в канал /гильдия', NULL),
(59017, 3, 'ruRU', '<- Назад', NULL);

--

DELETE FROM `trinity_string` WHERE `entry` = 30039;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc8`) VALUES
(30039, '|cffffffff%s shared PvP-stats:|r %s|cffffffff, %u/%u (%.2f%%).|r', '|cffffffff%s делится PvP-статистикой:|r %s|cffffffff, %u/%u (%.2f%%).|r');

DELETE FROM `trinity_string` WHERE `entry` = 30040;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc8`) VALUES
(30040, '|cffaaaaff[Group] %s shared PvP-stats:|r %s|cffaaaaff, %u/%u (%.2f%%).|r', '|cffaaaaff[Группа] %s делится PvP-статистикой:|r %s|cffaaaaff, %u/%u (%.2f%%).|r');

DELETE FROM `trinity_string` WHERE `entry` = 30041;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc8`) VALUES
(30041, '|cff40ff40[Guild] %s shared PvP-stats:|r %s|cff40ff40, %u/%u (%.2f%%).|r', '|cff40ff40[Гильдия] %s делится PvP-статистикой:|r %s|cff40ff40, %u/%u (%.2f%%).|r');
