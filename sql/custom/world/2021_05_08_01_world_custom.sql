-- Creature
DELETE FROM `creature_template` WHERE `entry` = 75016;
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `dmgschool`, `BaseAttackTime`, `RangeAttackTime`, `BaseVariance`, `RangeVariance`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `HoverHeight`, `HealthModifier`, `ManaModifier`, `ArmorModifier`, `DamageModifier`, `ExperienceModifier`, `RacialLeader`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `spell_school_immune_mask`, `flags_extra`, `ScriptName`, `VerifiedBuild`) VALUES
(75016, 0, 0, 0, 0, 0, 35008, 0, 0, 0, 'Lady Sacrolache', 'Epic Skin', NULL, 58013, 5, 6, 0, 35, 1, 1, 0.85714, 1, 0, 0, 2000, 2000, 1, 1, 1, 0, 2048, 0, 3, 1, 1, 30, 0, 0, 5880, 0, 0, 0, '', 1, 1, 1, 1, 1, 1, 1, 0, 100, 1, 0, 0, 0, 'npc_payable_skins_vendor', 0);

DELETE FROM `creature_template_locale` WHERE `entry` = 75016;
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `Title`, `VerifiedBuild`) VALUES
(75016, 'ruRU', 'Леди Сакролаш', 'Эпический скин', 0);
