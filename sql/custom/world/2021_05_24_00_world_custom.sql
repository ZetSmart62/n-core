--
DROP TABLE IF EXISTS `class_area`;
CREATE TABLE `class_area` (
  `class` tinyint UNSIGNED NOT NULL,
  `map` smallint UNSIGNED NOT NULL DEFAULT 0,
  `position_x` float NOT NULL DEFAULT 0,
  `position_y` float NOT NULL DEFAULT 0,
  `position_z` float NOT NULL DEFAULT 0,
  `position_o` float NOT NULL DEFAULT 0,
  `comment` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`class`));
  
INSERT INTO `class_area` (`class`, `map`, `position_x`, `position_y`, `position_z`, `position_o`, `comment`) VALUES
(1, 822, -10742.7, 429.452, 24.4129, 0, 'Warrior'),
(2, 823, 605.839, 637.187, 380.747, 0, 'Paladin'),
(3, 824, 1417.2, 1276.64, 33.0049, 0, 'Hunter'),
(4, 824, 1417.2, 1276.64, 33.0049, 0, 'Rogue'),
(5, 823, 605.839, 637.187, 380.747, 0, 'Priest'),
(6, 822, -10742.7, 429.452, 24.4129, 0, 'Death Knight'),
(7, 825, 3581.05, 5547.01, 324.3, 0, 'Shaman'),
(8, 732, 2784.49, 5998.42, 4.16715, 0, 'Mage'),
(9, 732, 2784.49, 5998.42, 4.16715, 0, 'Warlock'),
(11, 822, 3581.05, 5547.01, 324.3, 0, 'Druid');
