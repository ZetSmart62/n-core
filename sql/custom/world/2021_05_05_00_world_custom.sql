--
DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59013 AND `OptionID` IN (8, 9);
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59013, 8, 0, 'Repair all items in 5 |TInterface\\Icons\\almaz:20:20:-3:-3|t', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 5 diamonds?', 0, 0),
(59013, 9, 0, '<- Prev page', 0, 1, 1, 59000, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59013 AND `OptionID` IN (8, 9);
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59013, 8, 'ruRU', 'Отремонтировать все предметы за 5 |TInterface\\Icons\\almaz:20:20:-3:-3|t', 'Вы уверены, что хотите потратить 5 алмазов?'),
(59013, 9, 'ruRU', '<- Назад', NULL);

DELETE FROM `creature_text` WHERE `CreatureID` = 70000 AND `GroupID` IN (26);
INSERT INTO `creature_text` (`CreatureID`, `GroupID`, `ID`, `Text`, `Type`,`Language`, `Probability`, `Emote`, `Duration`, `Sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES
(70000, 26, 0, 'Your items have been successfully repaired.', 15, 0, 100, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_text_locale` WHERE `CreatureID` = 70000 AND `GroupID` IN (26);
INSERT INTO `creature_text_locale` (`CreatureID`, `GroupID`, `ID`, `Locale`, `Text`) VALUES
(70000, 26, 0, 'ruRU', 'Ваши предметы успешно отремонтированы.');
