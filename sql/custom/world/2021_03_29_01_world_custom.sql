--
-- Creature
DELETE FROM `creature_template` WHERE `entry` = 75007;
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `dmgschool`, `BaseAttackTime`, `RangeAttackTime`, `BaseVariance`, `RangeVariance`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `HoverHeight`, `HealthModifier`, `ManaModifier`, `ArmorModifier`, `DamageModifier`, `ExperienceModifier`, `RacialLeader`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `spell_school_immune_mask`, `flags_extra`, `ScriptName`, `VerifiedBuild`) VALUES
(75007, 0, 0, 0, 0, 0, 33004, 0, 0, 0, 'Blood Queen Lana\'tel', 'Epic Skin', NULL, 58005, 5, 6, 0, 35, 1, 1, 0.85714, 1, 0, 0, 2000, 2000, 1, 1, 1, 0, 2048, 0, 3, 1, 1, 30, 0, 0, 5880, 0, 0, 0, '', 1, 1, 1, 1, 1, 1, 1, 0, 100, 1, 0, 0, 0, 'npc_payable_skins_vendor', 0);

DELETE FROM `creature_template_locale` WHERE `entry` = 75007;
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `Title`, `VerifiedBuild`) VALUES
(75007, 'ruRU', 'Кровавая королева Лана\'тель', 'Эпический скин', 0);

DELETE FROM `creature_text` WHERE `CreatureID` = 75007;
INSERT INTO `creature_text` (`CreatureID`, `GroupID`, `ID`, `Text`, `Type`, `Language`, `Probability`, `Emote`, `Duration`, `Sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES (75007, 0, 0, '$n, You have successfully purchased the |cffDA70D6Blood Queen Lana\'tel|r', 15, 0, 100, 0, 0, 0, 0, 0, 'Payable skins Artas');

DELETE FROM `creature_text_locale` WHERE `CreatureID` = 75007;
INSERT INTO `creature_text_locale` (`CreatureID`, `GroupID`, `ID`, `Locale`, `Text`) VALUES (75007, 0, 0, 'ruRU', '$n, вы успешно приобрели скин |cffDA70D6Кровавой королевы Лана\'тель|r');

DELETE FROM `creature` WHERE `id` = 75007;
INSERT INTO `creature` (`id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `wander_distance`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`, `ScriptName`, `VerifiedBuild`) VALUES 
(75007, 732, 0, 0, 1, 1, 0, 0, 2750.68, 5973.29, 4.59842, 0.677011, 300, 0, 0, 1, 0, 0, 0, 0, 0, '', 0),
(75007, 823, 0, 0, 1, 1, 0, 0, 618.243, 663.809, 381.005, 3.77934, 300, 0, 0, 1, 0, 0, 0, 0, 0, '', 0);

-- Gossip

DELETE FROM `npc_text` WHERE `ID` = 75007;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (75007, 'This skin is designed for magicians, warlocks and female priests. When you buy this skin, you will get an aura and 3 abilities:$b1) Mad Lunge-Deals 75% weapon damage to the enemy and causes them to bleed, dealing additional damage every 3 seconds. within 15 sec.$b2) The Overshadowed Pact - Deals dark magic damage to you and nearby allies.$b3) Pall of Sorrow-Emits a wave of sadness and despair, once every 2 seconds. dealing damage to nearby enemies from dark magic.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 75007;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(75007, 'ruRU', 'Этот скин предназначен для магов, чернокнижников и жрецов женского пола. При покупке данного скина вы получите ауру и 3 способности:$b1) Безумный выпад - Наносит противнику 75% урона от оружия и заставляет его истекать кровью, нанося дополнительный урон раз в 3 сек. в течение 15 sec.$b2) Пакт Омраченных - Наносит урон от темной магии находящимся рядом союзникам.$b3) Покров скорби - Испускает волну печали и отчаяния, раз в 2 сек. нанося находящимся рядом противникам урон от темной магии.', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = 58005;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(58005, 75007, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 58005;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(58005, 0, 0, 'Purchase an |cffDA70D6Blood Queen Lana\'tel|r skin for 899 |TInterface\\Icons\\almaz:21:21:-3:-3|t', 0, 1, 1, 0, 0, 0, 0, 'Do you really want to purchase this skin?', 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 58005;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(58005, 0, 'ruRU', 'Приобрести скин |cffDA70D6Кровавой королевы Лана\'тель|r за 899 |TInterface\\Icons\\almaz:21:21:-3:-3|t', 'Вы действительно хотите приобрести этот скин?');

-- Payable skin

DELETE FROM `payable_skin_template` WHERE `id` = 5;
INSERT INTO `payable_skin_template` (`id`, `name`, `displayId`, `auras`, `spells`, `classmask`, `gender`, `price`) VALUES
(5, '|cffDA70D6Кровавая королева Лана\'тель|r', 33004, 36161, '79412 79413 79414', 400, 2, 899);

DELETE FROM `payable_skin_creature_vendors` WHERE `npcEntryOrGuid` = 75007;
INSERT INTO `payable_skin_creature_vendors` (`npcEntryOrGuid`, `payableSkin`) VALUES
(75007, 5);
