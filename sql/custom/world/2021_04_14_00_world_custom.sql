--
DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59000;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59000, 0, 0, 'Info about server.', 0, 1, 1, 59001, 0, 0, 0, NULL, 0, 0),
(59000, 1, 0, 'Teleport to the area of your class.', 0, 1, 1, 0, 0, 0, 0, NULL, 0, 0),
(59000, 2, 0, 'Teleport to the area of your class.', 0, 1, 1, 0, 0, 0, 0, NULL, 0, 0),
(59000, 3, 0, 'Teleport to the area of your class.', 0, 1, 1, 0, 0, 0, 0, NULL, 0, 0),
(59000, 4, 0, 'Teleport to the area of your class.', 0, 1, 1, 0, 0, 0, 0, NULL, 0, 0),
(59000, 5, 0, 'Teleport to the area of your class.', 0, 1, 1, 0, 0, 0, 0, NULL, 0, 0),
(59000, 6, 0, 'Payable skins system.', 0, 1, 1, 59010, 0, 0, 0, NULL, 0, 0),
(59000, 7, 0, '|cff0000ffShop|r.', 0, 1, 1, 59013, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59000;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59000, 0, 'ruRU', 'Информация о сервере.', NULL),
(59000, 1, 'ruRU', 'Телепортироваться в зону вашего класса.', NULL),
(59000, 2, 'ruRU', 'Телепортироваться в зону вашего класса.', NULL),
(59000, 3, 'ruRU', 'Телепортироваться в зону вашего класса.', NULL),
(59000, 4, 'ruRU', 'Телепортироваться в зону вашего класса.', NULL),
(59000, 5, 'ruRU', 'Телепортироваться в зону вашего класса.', NULL),
(59000, 6, 'ruRU', 'Система внешнего вида.', NULL),
(59000, 7, 'ruRU', '|cff0000ffМагазин|r.', NULL);

--

UPDATE `creature_template` SET `AIName` = 'SmartAI' WHERE `entry` = 70000;

DELETE FROM `smart_scripts` WHERE `entryorguid` = 70000 AND `source_type` = 0;
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `event_param5`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_param4`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
(70000, 0, 0, 0, 62, 0, 100, 0, 59000, 1, 0, 0, 0, 62, 822, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, -10742.7, 429.452, 24.4129, 0, 'Server manager - On Gossip Option 1 Selected - Teleport to Warriors and Death Knights area'),
(70000, 0, 1, 0, 62, 0, 100, 0, 59000, 2, 0, 0, 0, 62, 823, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 605.839, 637.187, 380.747, 0, 'Server manager - On Gossip Option 2 Selected - Teleport to Paladins and Priests area'),
(70000, 0, 2, 0, 62, 0, 100, 0, 59000, 3, 0, 0, 0, 62, 824, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 1417.2, 1276.64, 33.0049, 0, 'Server manager - On Gossip Option 3 Selected - Teleport to Hunters and Rogues area'),
(70000, 0, 3, 0, 62, 0, 100, 0, 59000, 4, 0, 0, 0, 62, 825, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 3581.05, 5547.01, 324.3, 0, 'Server manager - On Gossip Option 4 Selected - Teleport to Shamans and Druids area'),
(70000, 0, 4, 0, 62, 0, 100, 0, 59000, 5, 0, 0, 0, 62, 732, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 2784.49, 5998.42, 4.16715, 0, 'Server manager - On Gossip Option 5 Selected - Teleport to Mages and Warlocks area');

--

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` = 15 AND `SourceGroup` = 59000;
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
(15, 59000, 1, 0, 0, 15, 0, 33, 0, 0, 0, 0, 0, '', 'Server manager - Show gossip option 1 only for Warriors and Death Knights'),
(15, 59000, 2, 0, 0, 15, 0, 18, 0, 0, 0, 0, 0, '', 'Server manager - Show gossip option 2 only for Paladins and Priests'),
(15, 59000, 3, 0, 0, 15, 0, 12, 0, 0, 0, 0, 0, '', 'Server manager - Show gossip option 3 only for Hunters and Rogues'),
(15, 59000, 4, 0, 0, 15, 0, 1088, 0, 0, 0, 0, 0, '', 'Server manager - Show gossip option 4 only for Shamans and Druids'),
(15, 59000, 5, 0, 0, 15, 0, 384, 0, 0, 0, 0, 0, '', 'Server manager - Show gossip option 5 only for Mages and Warlocks');
