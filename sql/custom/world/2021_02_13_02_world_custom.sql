--
ALTER TABLE `access_requirement`
ADD COLUMN `rank`  int(11) UNSIGNED NOT NULL DEFAULT 0 AFTER `quest_failed_text`,
ADD COLUMN `rank_failed_text`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `rank`;
