--
-- Creature
DELETE FROM `creature_template` WHERE `entry` = 75019;
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `dmgschool`, `BaseAttackTime`, `RangeAttackTime`, `BaseVariance`, `RangeVariance`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `HoverHeight`, `HealthModifier`, `ManaModifier`, `ArmorModifier`, `DamageModifier`, `ExperienceModifier`, `RacialLeader`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `spell_school_immune_mask`, `flags_extra`, `ScriptName`, `VerifiedBuild`) VALUES
(75019, 0, 0, 0, 0, 0, 35014, 0, 0, 0, 'Berserk', 'Epic Skin', NULL, 58016, 5, 6, 0, 35, 1, 1, 0.85714, 1, 0, 0, 2000, 2000, 1, 1, 1, 0, 2048, 0, 3, 1, 1, 30, 0, 0, 5880, 0, 0, 0, '', 1, 1, 1, 1, 1, 1, 1, 0, 100, 1, 0, 0, 0, 'npc_payable_skins_vendor', 0);

DELETE FROM `creature_template_locale` WHERE `entry` = 75019;
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `Title`, `VerifiedBuild`) VALUES
(75019, 'ruRU', 'Берсерк', 'Эпический скин', 0);

DELETE FROM `creature_text` WHERE `CreatureID` = 75019;
INSERT INTO `creature_text` (`CreatureID`, `GroupID`, `ID`, `Text`, `Type`, `Language`, `Probability`, `Emote`, `Duration`, `Sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES (75019, 0, 0, '$n, You have successfully purchased the |cffda70d6Berserker|r', 15, 0, 100, 0, 0, 0, 0, 0, NULL);

DELETE FROM `creature_text_locale` WHERE `CreatureID` = 75019;
INSERT INTO `creature_text_locale` (`CreatureID`, `GroupID`, `ID`, `Locale`, `Text`) VALUES (75019, 0, 0, 'ruRU', '$n, вы успешно приобрели скин |cffda70d6Берсерка|r');

DELETE FROM `creature` WHERE `id` = 75019;
INSERT INTO `creature` (`id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `wander_distance`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`, `ScriptName`, `VerifiedBuild`) VALUES 
(75019, 824, 0, 0, 1, 1, 0, 0, 1426.3, 1248.16, 34.1072, 0.301615, 300, 0, 0, 120, 0, 0, 0, 0, 0, '', 0),
(75019, 823, 0, 0, 1, 1, 0, 0, 628.77, 642.321, 380.801, 3.38507, 300, 0, 0, 120, 0, 0, 0, 0, 0, '', 0),
(75019, 822, 0, 0, 1, 1, 0, 0, -10743.5, 442.44, 24.4945, 3.94192, 300, 0, 0, 120, 0, 0, 0, 0, 0, '', 0);

-- Gossip

DELETE FROM `npc_text` WHERE `ID` = 75019;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (75019, 'This skin is designed for female warriors, paladins, robbers, and death knights.. When you buy a skin, you will get an appearance, aura, and spells:$b$b1) Charolom-Dealing 100% weapon damage. The target is hit, and the spell\'s effectiveness is reduced by 75% for 6 seconds.$b2) Flurry-Increases attack speed by 100%. Increases movement speed by 100%. Duration-6 seconds$b3) Contagious disease-Affects the enemy with a disease for 12 seconds, in which he is once in 1 second. for 12 seconds, damage from the forces of nature is dealt. A sick target can infect one of its nearby allies.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 75019;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(75019, 'ruRU', 'Этот скин предназначен для воинов, паладинов, разбойников и рыцарей смерти женского пола. При покупке скина вы получите внешний вид, ауру и заклинания:$b$b1)Чаролом - Нанесение 100% урона от оружия. Цель оказывается ранена, и эффективность применения заклинаний снижается на 75% на 6 сек$b2)Шквал - Увеличивает скорость атаки на 100%. Увеличивает скорость передвижения на 100%. Время действия – 6 сек$b3)Заразная болезнь - Поражает противника болезнью на 12 сек, при которой ему раз в 1 сек. в течение 12 сек наносится урон от сил природы. Заболевшая цель может заразить одного из своих находящихся рядом союзников', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = 58016;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(58016, 75019, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 58016;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(58016, 0, 0, 'Purchase an |cffda70d6Berserker|r skin for 899 |TInterface\\Icons\\almaz:21:21:-3:-3|t', 0, 1, 1, 0, 0, 0, 0, 'Do you really want to purchase this skin?', 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 58016;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(58016, 0, 'ruRU', 'Приобрести скин |cffda70d6Берсерка|r за 899 |TInterface\\Icons\\almaz:21:21:-3:-3|t', 'Вы действительно хотите приобрести этот скин?');

-- Payable skin

DELETE FROM `payable_skin_template` WHERE `id` = 16;
INSERT INTO `payable_skin_template` (`id`, `name`, `nameColor`, `displayId`, `auras`, `spells`, `classmask`, `gender`, `price`) VALUES
(16, 'Берсерк', 'ffda70d6', 35014, '36161', '79422 79423 79424', 43, 2, 899);

DELETE FROM `payable_skin_creature_vendors` WHERE `npcEntryOrGuid` = 75019;
INSERT INTO `payable_skin_creature_vendors` (`npcEntryOrGuid`, `payableSkin`) VALUES
(75019, 16);
