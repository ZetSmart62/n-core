--
DELETE FROM `command` WHERE `name` IN ('reload ranks', 'reload payable_skins'); -- delete old commands

DELETE FROM `command` WHERE `name` = 'custom';
INSERT INTO `command` (`name`, `help`) VALUES ('custom', 'Syntax: .custom $subcommand\nType .custom to see the list of possible subcommands or .help custom $subcommand to see info on subcommands');

DELETE FROM `command` WHERE `name` = 'custom reload';
INSERT INTO `command` (`name`, `help`) VALUES ('custom reload', 'Syntax: .custom reload $subcommand\nType .custom reload to see the list of possible subcommands or .help custom reload $subcommand to see info on subcommands');

DELETE FROM `command` WHERE `name` = 'custom reload all';
INSERT INTO `command` (`name`, `help`) VALUES ('custom reload all', 'Syntax: .custom reload all\nReload all custom systems and DB tables with reload support added and that can be _safe_ reloaded.');

DELETE FROM `command` WHERE `name` = 'custom reload ranks';
INSERT INTO `command` (`name`, `help`) VALUES ('custom reload ranks', 'Syntax: .custom reload ranks\nReload ranks system.');

DELETE FROM `command` WHERE `name` = 'custom reload skins';
INSERT INTO `command` (`name`, `help`) VALUES ('custom reload skins', 'Syntax: .custom reload skins\nReload payable skins system.');
