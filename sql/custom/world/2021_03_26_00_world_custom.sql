-- Gossip menu NPC-Menager(menu = 59013(SALE))
DELETE FROM `gossip_menu` WHERE `MenuID` = 59013;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(59013, 75004, 0);

DELETE FROM `npc_text` WHERE `ID` = 75004;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (75004, 'In this section you will be able to purchase accessories for the gameplay. Take your pick.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 75004;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(75004, 'ruRU', 'В этом разделе ты сможешь приобрести облегчения для игрового процесса. Выбирай.', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59013;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59013, 0, 0, 'Increase the skill of professions', 0, 1, 1, 59014, 0, 0, 0, NULL, 0, 0),
(59013, 1, 0, 'Character rename for 400 |TInterface\\Icons\\almaz:20:20:-3:-3|t', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 400 diamonds?', 0, 0),
(59013, 2, 0, 'Change your race for 400 |TInterface\\Icons\\almaz:20:20:-3:-3|t', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 400 diamonds?', 0, 0),
(59013, 3, 0, 'Buy 80 LVL for 600 |TInterface\\Icons\\almaz:20:20:-3:-3|t', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 100 diamonds?', 0, 0),
(59013, 4, 0, 'Buy titles', 0, 1, 1, 59015, 0, 0, 0, NULL, 0, 0),
(59013, 5, 0, '<- Prev page', 0, 1, 1, 59000, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59013;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59013, 0, 'ruRU', 'Повысить навык профессий', NULL),
(59013, 1, 'ruRU', 'Сменить никнейм за 400 |TInterface\\Icons\\almaz:20:20:-3:-3|t', 'Вы уверены, что хотите потратить 400 алмазов?'),
(59013, 2, 'ruRU', 'Сменить расу за 400 |TInterface\\Icons\\almaz:20:20:-3:-3|t', 'Вы уверены, что хотите потратить 400 алмазов?'),
(59013, 3, 'ruRU', 'Купить 80 лвл за 600 |TInterface\\Icons\\almaz:20:20:-3:-3|t', 'Вы уверены, что хотите потратить 100 алмазов?'),
(59013, 4, 'ruRU', 'Купить звания', NULL),
(59013, 5, 'ruRU', '<- Назад', NULL);

-- Prof menu(menu = 59014(SALE))
DELETE FROM `gossip_menu` WHERE `MenuID` = 59014;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(59014, 75005, 0);

DELETE FROM `npc_text` WHERE `ID` = 75005;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (75005, 'By choosing a profession from this list you can upgrade it to 75 for 80 |TInterface\\Icons\\almaz:20:20:-3:-3|t', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 75005;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(75005, 'ruRU', 'Выбрав профессию с этого списка ты сможешь её прокачать на 75 за 80 |TInterface\\Icons\\almaz:20:20:-3:-3|t', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59014;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59014, 0, 0, 'Alchemy', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 80 diamonds?', 0, 0),
(59014, 1, 0, 'Blacksmithing', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 80 diamonds?', 0, 0),
(59014, 2, 0, 'Enchanting', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 80 diamonds?', 0, 0),
(59014, 3, 0, 'Engineering', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 80 diamonds?', 0, 0),
(59014, 4, 0, 'Herbalism', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 80 diamonds?', 0, 0),
(59014, 5, 0, 'Inscription', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 80 diamonds?', 0, 0),
(59014, 6, 0, 'Jewelcrafting', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 80 diamonds?', 0, 0),
(59014, 7, 0, 'Leatherworking', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 80 diamonds?', 0, 0),
(59014, 8, 0, 'Mining', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 80 diamonds?', 0, 0),
(59014, 9, 0, 'Skinning', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 80 diamonds?', 0, 0),
(59014, 10, 0, 'Tailoring', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 80 diamonds?', 0, 0),
(59014, 11, 0, 'Cooking', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 80 diamonds?', 0, 0),
(59014, 12, 0, 'First Aid', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 80 diamonds?', 0, 0),
(59014, 13, 0, 'Fishing', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 80 diamonds?', 0, 0),
(59014, 14, 0, '<- Prev page', 0, 1, 1, 59013, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59014;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59014, 0, 'ruRU', 'Алхимия', 'Вы уверены, что хотите потратить 80 алмазов?'),
(59014, 1, 'ruRU', 'Кузнечное дело', 'Вы уверены, что хотите потратить 80 алмазов?'),
(59014, 2, 'ruRU', 'Наложение чар', 'Вы уверены, что хотите потратить 80 алмазов?'),
(59014, 3, 'ruRU', 'Инженерное дело', 'Вы уверены, что хотите потратить 80 алмазов?'),
(59014, 4, 'ruRU', 'Травничество', 'Вы уверены, что хотите потратить 80 алмазов?'),
(59014, 5, 'ruRU', 'Начертание', 'Вы уверены, что хотите потратить 80 алмазов?'),
(59014, 6, 'ruRU', 'Ювелирное дело', 'Вы уверены, что хотите потратить 80 алмазов?'),
(59014, 7, 'ruRU', 'Кожевничество', 'Вы уверены, что хотите потратить 80 алмазов?'),
(59014, 8, 'ruRU', 'Горное дело.', 'Вы уверены, что хотите потратить 80 алмазов?'),
(59014, 9, 'ruRU', 'Снятие шкур.', 'Вы уверены, что хотите потратить 80 алмазов?'),
(59014, 10, 'ruRU', 'Портняжное дело.', 'Вы уверены, что хотите потратить 80 алмазов?'),
(59014, 11, 'ruRU', 'Кулинария.', 'Вы уверены, что хотите потратить 80 алмазов?'),
(59014, 12, 'ruRU', 'Первая помощь.', 'Вы уверены, что хотите потратить 80 алмазов?'),
(59014, 13, 'ruRU', 'Рыбная ловля.', 'Вы уверены, что хотите потратить 80 алмазов?'),
(59014, 14, 'ruRU', '<- Назад', NULL);
