--
-- LANG_PINFO_CHR_RANK
DELETE FROM `trinity_string` WHERE `entry` = 30003;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc8`) VALUES
(30003, '│ Rank: %u [%s]', '│ Ранг: %u [%s]');

-- LANG_PINFO_CHR_SKIN
DELETE FROM `trinity_string` WHERE `entry` = 30004;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc8`) VALUES
(30004, '│ Skin: %s (Id: %u)', '│ Скин: %s (Id: %u) ');
