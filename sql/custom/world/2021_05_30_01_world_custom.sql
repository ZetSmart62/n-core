--
UPDATE `creature_template` SET `ScriptName` = 'npc_server_menu' WHERE `entry` = 70000;

DELETE FROM `spell_script_names` WHERE `ScriptName` = 'spell_server_menu';
INSERT INTO `spell_script_names` (`spell_id`, `ScriptName`) VALUES (90000, 'spell_server_menu');

DELETE FROM `creature_text` WHERE `CreatureID` = 70000;
DELETE FROM `creature_text_locale` WHERE `CreatureID` = 70000;

DELETE FROM `trinity_string` WHERE `entry` BETWEEN 30042 AND 30074;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc8`) VALUES
(30042, "You have successfully activated the skin.", "Вы успешно применили скин."),
(30043, "You have already activated this skin.", "Вы уже активировали этот скин."),
(30044, "Your skin has been successfully reset.", "Ваш скин был успешно сброшен."),
(30045, "You don\'t have an active skin.", "У Вас нет активного скина."),
(30046, "Unfortunately, you don\'t have enough diamonds.", "К сожалению у вас недостаточно алмазов."),
(30047, "You have successfully purchased a character rename service. To use it, you need to re-enter the game.", "Вы успешно приобрели услугу смены имени персонажа. Чтобы воспользоваться ею, вам нужно перезайти в игровой мир."),
(30048, "You already have a character rename service. To use it, you need to re-enter the game.", "У вас уже есть услуга смены имени персонажа. Чтобы воспользоваться ею, вам нужно перезайти в игровой мир."),
(30049, "You have successfully purchased a character change race service. To use it, you need to re-enter the game.", "Вы успешно приобрели услугу смены расы персонажа. Чтобы воспользоваться ею, вам нужно перезайти в игровой мир."),
(30050, "You already have a character change race service. To use it, you need to re-enter the game.", "У вас уже есть услуга смены расы персонажа. Чтобы воспользоваться ею, вам нужно перезайти в игровой мир."),
(30051, "You have successfully purchased a character level boost service. You are now level cap, congratulations!", "Вы успешно приобрели услугу повышения уровня персонажа. Теперь у вас максимальный уровень, поздравляем!"),
(30052, "Your character has already reached the maximum level.", "Ваш персонаж уже достиг максимального уровня."),
(30053, "You have successfully purchased an unbind instances service. All instance saves unbinded from your character.", "Вы успешно приобрели услугу сброса сохранения подземелий. Все сохранения откреплены от вашего персонажа."),
(30054, "You have successfully purchased arena points.", "Вы успешно приобрели очки арены."),
(30055, "You already have too many arena points.", "У вас уже есть слишком много очков арены."),
(30056, "You have successfully purchased honor points.", "Вы успешно приобрели очки чести."),
(30057, "You already have too many honor points.", "У вас уже есть слишком много очков чести."),
(30058, "You have successfully purchased the profession skill increase service.", "Вы успешно приобрели услугу повышения навыка профессии."),
(30059, "You do not have this profession.", "У вас нет этой профессии."),
(30060, "You already have the maximum skill level for this profession.", "Вы уже имеете максимальный уровень навыка для этой профессии."),
(30061, "You have successfully purchased a title.", "Вы успешно приобрели звание."),
(30062, "You already have this title.", "Вы уже имеете это звание."),
(30063, "The character was not found. Make sure you select a player.", "Персонаж не найден. Удостоверьтесь, что вы выбрали игрока."),
(30064, "A character with that name was not found.", "Персонаж с таким именем не найден."),
(30065, "You need to wait another %u sec.", "Нужно подождать еще %u сек."),
(30066, "You are not in a group.", "Вы не состоите в группе."),
(30067, "You are not a member of the guild.", "Вы не являетесь членом гильдии."),
(30068, "Your items have been successfully repaired.", "Ваши предметы успешно отремонтированы."),
(30069, "You have successfully purchased the service of changing the character\'s faction. To use it, you need to re-enter the game world.", "Вы успешно приобрели услугу смены фракции персонажа. Чтобы воспользоваться ею, вам нужно перезайти в игровой мир."),
(30070, "You already have the service of changing the character\'s faction. To use it, you need to re-enter the game world.", "У вас уже есть услуга смены фракции персонажа. Чтобы воспользоваться ею, вам нужно перезайти в игровой мир."),
(30071, "You cannot do this in combat.", "Вы не можете сделать этого в бою."),
(30072, "You cannot do this in the battleground.", "Вы не можете сделать этого на поле боя."),
(30073, "You cannot do this in the arena.", "Вы не можете сделать этого на арене."),
(30074, "You are dead and you cannot do this.", "Вы мертвы и не можете этого сделать.");
