--
SET @NPC_SERVER_MANAGER := 70000;
SET @SERVER_MANAGER_MAIN_MENU := 59000;
SET @PVP_STATS_INFO_TEXT := 76013;
SET @PVP_STATS_INFO_MENU := 59016;
SET @PVP_STATS_TEXT := 76014;
SET @PVP_STATS_MENU := 59017;

SET @LANG_SERVER_MANAGER_PVP_STATS_DYNAMIC_TEXT := 30038;

-- Adding error message

DELETE FROM `creature_text` WHERE `CreatureID` = @NPC_SERVER_MANAGER AND `GroupID` = 21;
INSERT INTO `creature_text` (`CreatureID`, `GroupID`, `ID`, `Text`, `Type`,`Language`, `Probability`, `Emote`, `Duration`, `Sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES
(@NPC_SERVER_MANAGER, 21, 0, 'A character with that name was not found.', 15, 0, 100, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_text_locale` WHERE `CreatureID` = @NPC_SERVER_MANAGER AND `GroupID` = 21;
INSERT INTO `creature_text_locale` (`CreatureID`, `GroupID`, `ID`, `Locale`, `Text`) VALUES
(@NPC_SERVER_MANAGER, 21, 0, 'ruRU', 'Персонаж с таким именем не найден.');

-- Adding PvP-stats option to server manager main menu

DELETE FROM `gossip_menu_option` WHERE `MenuID` = @SERVER_MANAGER_MAIN_MENU AND `OptionID` IN (6, 7, 8);
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(@SERVER_MANAGER_MAIN_MENU, 6, 0, 'PvP-stats.', 0, 1, 1, @PVP_STATS_INFO_MENU, 0, 0, 0, NULL, 0, 0),
(@SERVER_MANAGER_MAIN_MENU, 7, 0, 'Payable skins system.', 0, 1, 1, 59010, 0, 0, 0, NULL, 0, 0),
(@SERVER_MANAGER_MAIN_MENU, 8, 0, '|cff0000ffShop|r.', 0, 1, 1, 59013, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = @SERVER_MANAGER_MAIN_MENU  AND `OptionID` IN (6, 7, 8);
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(@SERVER_MANAGER_MAIN_MENU, 6, 'ruRU', 'PvP-статистика.', NULL),
(@SERVER_MANAGER_MAIN_MENU, 7, 'ruRU', 'Система внешнего вида.', NULL),
(@SERVER_MANAGER_MAIN_MENU, 8, 'ruRU', '|cff0000ffМагазин|r.', NULL);

-- Creating PvP-stats info menu

DELETE FROM `npc_text` WHERE `ID` = @PVP_STATS_INFO_TEXT;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES
(@PVP_STATS_INFO_TEXT, 'Info abount PvP-stats system.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = @PVP_STATS_INFO_TEXT;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(@PVP_STATS_INFO_TEXT, 'ruRU', 'Здесь напишешь инфу о системе PvP-статистики.', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = @PVP_STATS_INFO_MENU;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(@PVP_STATS_INFO_MENU, @PVP_STATS_INFO_TEXT, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = @PVP_STATS_INFO_MENU;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(@PVP_STATS_INFO_MENU, 0, 0, 'Show my PvP-stats.', 0, 1, 1, 0, 0, 0, 0, NULL, 0, 0),
(@PVP_STATS_INFO_MENU, 1, 0, 'Show PvP-stats of another player.', 0, 1, 1, 0, 0, 1, 0, NULL, 0, 0),
(@PVP_STATS_INFO_MENU, 2, 0, '<- Prev page', 0, 1, 1, @SERVER_MANAGER_MAIN_MENU, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = @PVP_STATS_INFO_MENU;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(@PVP_STATS_INFO_MENU, 0, 'ruRU', 'Показать мою PvP-статистику.', NULL),
(@PVP_STATS_INFO_MENU, 1, 'ruRU', 'Показать PvP-статистику другого игрока.', NULL),
(@PVP_STATS_INFO_MENU, 2, 'ruRU', '<- Назад', NULL);

-- Creating PvP-stats menu

DELETE FROM `trinity_string` WHERE `entry` = @LANG_SERVER_MANAGER_PVP_STATS_DYNAMIC_TEXT;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc8`) VALUES
(@LANG_SERVER_MANAGER_PVP_STATS_DYNAMIC_TEXT, 'PvP-stats of player %s:$b$bRank: %s$bWSG: %u/%u (%.2f%%)$bArathi: %u/%u (%.2f%%)$bArena 5v5: %u/%u (%.2f%%)$b$bTotal: %u/%u (%.2f%%)', 'PvP-статистика игрока %s:$b$bРанг: %s$bВарсонг: %u/%u (%.2f%%)$bАрати: %u/%u (%.2f%%)$bАрена 5 на 5: %u/%u (%.2f%%)$b$bВсего: %u/%u (%.2f%%)');

DELETE FROM `npc_text` WHERE `ID` = @PVP_STATS_TEXT;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES
(@PVP_STATS_TEXT, 'This is a window for displaying dynamic text with PvP statistics. If you see this text, then something went wrong.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = @PVP_STATS_TEXT;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(@PVP_STATS_TEXT, 'ruRU', 'Это окно для отображения динамического текста с PvP-статистикой. Если Вы видите этот текст, значит что-то пошло не так.', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = @PVP_STATS_MENU;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(@PVP_STATS_MENU, @PVP_STATS_TEXT, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = @PVP_STATS_MENU;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(@PVP_STATS_MENU, 0, 0, '<- Prev page', 0, 1, 1, @PVP_STATS_INFO_MENU, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = @PVP_STATS_MENU;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(@PVP_STATS_MENU, 0, 'ruRU', '<- Назад', NULL);
