--
-- Creature
DELETE FROM `creature_template` WHERE `entry` = 75023;
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `dmgschool`, `BaseAttackTime`, `RangeAttackTime`, `BaseVariance`, `RangeVariance`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `HoverHeight`, `HealthModifier`, `ManaModifier`, `ArmorModifier`, `DamageModifier`, `ExperienceModifier`, `RacialLeader`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `spell_school_immune_mask`, `flags_extra`, `ScriptName`, `VerifiedBuild`) VALUES
(75023, 0, 0, 0, 0, 0, 35013, 0, 0, 0, 'Karstang', 'Elite Skin', NULL, 58020, 5, 6, 0, 35, 1, 1, 0.85714, 1, 0, 0, 2000, 2000, 1, 1, 1, 0, 2048, 0, 3, 1, 1, 30, 0, 0, 5880, 0, 0, 0, '', 1, 1, 1, 1, 1, 1, 1, 0, 100, 1, 0, 0, 0, 'npc_payable_skins_vendor', 0);

DELETE FROM `creature_template_locale` WHERE `entry` = 75023;
INSERT INTO `creature_template_locale` (`entry`, `locale`, `Name`, `Title`, `VerifiedBuild`) VALUES
(75023, 'ruRU', 'Карстанг', 'Элитный скин', 0);

DELETE FROM `creature_text` WHERE `CreatureID` = 75023;
INSERT INTO `creature_text` (`CreatureID`, `GroupID`, `ID`, `Text`, `Type`, `Language`, `Probability`, `Emote`, `Duration`, `Sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES (75023, 0, 0, '$n, You have successfully purchased the |cff00ff00Karstangа|r', 15, 0, 100, 0, 0, 0, 0, 0, NULL);

DELETE FROM `creature_text_locale` WHERE `CreatureID` = 75023;
INSERT INTO `creature_text_locale` (`CreatureID`, `GroupID`, `ID`, `Locale`, `Text`) VALUES (75023, 0, 0, 'ruRU', '$n, вы успешно приобрели скин |cff00ff00Карстанга|r');

DELETE FROM `creature` WHERE `id` = 75023;
INSERT INTO `creature` (`id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `wander_distance`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`, `ScriptName`, `VerifiedBuild`) VALUES 
(75023, 823, 0, 0, 1, 1, 0, 0, 613.935, 670.526, 381.089, 3.87123, 300, 0, 0, 120, 0, 0, 0, 0, 0, '', 0),
(75023, 825, 0, 0, 1, 1, 0, 0, 3526.06, 5515.13, 325.261, 5.1585, 300, 0, 0, 102, 0, 0, 0, 0, 0, '', 0),
(75023, 732, 0, 0, 1, 1, 0, 0, 2755.11, 5962.92, 2.70559, 0.267032, 300, 0, 0, 120, 0, 0, 0, 0, 0, '', 0);

-- Gossip

DELETE FROM `npc_text` WHERE `ID` = 75023;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (75023, 'This skin is designed for: Priests, Shamans, Magicians and Male Warlocks. When you buy a skin, you will get: Appearance and aura.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 75023;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(75023, 'ruRU', 'Этот скин предназначен для: Жрецов, Шаманов, Магов и Чёрнокнижников мужского пола. При покупке скина вы получите: Внешний вид и ауру.', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = 58020;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(58020, 75023, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 58020;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(58020, 0, 0, 'Purchase an |cff00ff00Karstangа|r skin for 599 |TInterface\\Icons\\almaz:21:21:-3:-3|t', 0, 1, 1, 0, 0, 0, 0, 'Do you really want to purchase this skin?', 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 58020;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(58020, 0, 'ruRU', 'Приобрести скин |cff00ff00Карстанга|r за 599 |TInterface\\Icons\\almaz:21:21:-3:-3|t', 'Вы действительно хотите приобрести этот скин?');

-- Payable skin

DELETE FROM `payable_skin_template` WHERE `id` = 20;
INSERT INTO `payable_skin_template` (`id`, `name`, `nameColor`, `displayId`, `auras`, `spells`, `classmask`, `gender`, `price`) VALUES
(20, 'Карстанг', 'ff00ff00', 35013, '36160', NULL, 464, 1, 599);

DELETE FROM `payable_skin_creature_vendors` WHERE `npcEntryOrGuid` = 75023;
INSERT INTO `payable_skin_creature_vendors` (`npcEntryOrGuid`, `payableSkin`) VALUES
(75023, 20);
