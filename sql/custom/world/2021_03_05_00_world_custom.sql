--
DELETE FROM `npc_text_locale` WHERE `ID` BETWEEN 70000 AND 70001;
INSERT INTO `npc_text_locale` (`ID`,`Locale`,`text0_0`) VALUES
('70000','ruRU','Информация о сервере'),
('70001','ruRU','Ранги - это система оценки для игроков нашего сервера, которая определяет, насколько опытен и не опытен игрок. $b$bИгроки награждаются одной звездой за каждую победу в рейтинговой игре, но они теряют звезду за каждое поражение в рейтинговой игре. $bКогда игрок имеет полные звезды на уровне внутри дивизиона, следующая победа приведет к продвижению на следующий уровень внутри дивизиона. $bОднако, когда игрок имеет полные звезды на уровне I в своем дивизионе (например: Воин I, Эпик I и Легенда I), они будут переведены в следующий дивизион. $bКогда у игрока нет звезд на ранге в пределах своего ранга, следующее поражение приведет к тому, что он будет понижен до предыдущего более низкого ранга (например: Эпик II до Эпик III или Легенда IV до Легенда V) в своем дивизионе.$b$b|cff0000ffСуществующие ранги в данный момент:|r');

DELETE FROM `npc_text_locale` WHERE `ID` BETWEEN 71000 AND 71000;
INSERT INTO `npc_text_locale` (`ID`,`Locale`,`text0_0`) VALUES
('71000','ruRU','Информация о сервере');

DELETE FROM `npc_text_locale` WHERE `ID` BETWEEN 71100 AND 71107;
INSERT INTO `npc_text_locale` (`ID`,`Locale`,`text0_0`) VALUES
('71100','ruRU','Информация о рангах'),
('71101','ruRU','Воин'),
('71102','ruRU','Элита'),
('71103','ruRU','Мастер'),
('71104','ruRU','Грандмастер'),
('71105','ruRU','Эпик'),
('71106','ruRU','Легенда'),
('71107','ruRU','Мифический');

DELETE FROM `npc_text_locale` WHERE `ID` BETWEEN 72000 AND 72006;
INSERT INTO `npc_text_locale` (`ID`,`Locale`,`text0_0`) VALUES
('72000','ruRU','|TInterface\\Icons\\icons_rang_voin:25:25:-3:-3|t|cffffffffВоин|r$b$b|cff0000ffОписание:|r$B$BВоин-это первый ранг в игре, который дается новым игрокам, когда они зашли в игру. $BВ этом ранге игроки начнут с самого низкого уровня, который является "Воин III". $bЧтобы подняться на следующий уровень, например от "Воина III" до "Воина II", требуется три звезды. $bЭтот ранг имеет только 3 дивизиона, и ему требуется 4 звезды, чтобы подняться до следующего дивизиона/ранга. $bРейтинг до Элиты требует ранга "Воин I" и 4 звезды для повышения.$b$b|cff0000ffНа этом ранге открываются следующие подземелья:|r$b$bКрепость Темного Клыка, Тюрьма Штормграда, Мёртвые Копи, Пищеры Стенаний, Лабиринты Иглошкурых, Непроглядная Пучина, Ульдаман, Гномреган, Затонувший храм, Курганы Иглошкурых, Монастырь Алого ордена, ЗулФаррак, Пик Чёрной горы, Глубины Чёрной горы, Логово Ониксии(10 игроков), Логово Ониксии(25 игроков), Открытие Тёмного портала, Открытие Тёмного портала(Героический)$b'),
('72001','ruRU','|TInterface\\Icons\\icons_rang_elite:25:25:-3:-3|t|cffffff00Элита|r $b$b|cff0000ffОписание:|r$B$BЭлита - это 2-й ранг в игре сразу после "Воина". $bВы начнете с самого низкого уровня "Элита III", и вам потребуется 5 звезд, чтобы подняться на следующий уровень, например: от "Элиты III" до "Элиты II". $bРанг имеет только 3 дивизиона, но каждый дивизион требует 5 звезд для повышения ранга вместо предыдущих 4. $bДля поднятия до ранга "Мастер", требуется ранг "Элита I" и 5 звезд.$b$b|cff0000ffДоступные подземелья на этом ранге:|r$b$b'),
('72002','ruRU','|TInterface\\Icons\\icons_rang_master:25:25:-3:-3|t|cffFFC125Мастер|r $b$b|cff0000ffОписание:|r$B$BМастер занимает 3-е место в игре сразу после элиты. $BНа этом ранге игра становится немного сложнее. $BИгрок начинает с самого низкого уровня "Мастер IV", и ему требуется 5 звезд, чтобы подняться на следующий уровень, например, с "Мастера IV" до "Мастера III". $BВ отличие от элиты, этот ранг имеет только 4 дивизиона. $BЧтобы получить ранг "Грандмастер" требуется получить "Мастер I" и 5 звезд.$b$b|cff0000ffДоступные подземелья на этом ранге:|r$b$b'),
('72003','ruRU','|TInterface\\Icons\\icons_rang_grandmaster:25:25:-3:-3|t|cffffcc00Грандмастер|r $b$b|cff0000ffОписание:|r$B$BГрандмастер - это 4-й ранг после Мастера. $BНа этом ранге игра будет более напряженной и сложной, в отличие от предыдущих рангов. $BИгрок начинает с самого низкого уровня "Грандмастер V", и ему требуется 6 звезд, чтобы подняться на следующий девизион, например от "Грандмастера V" до "Грандмастера IV". $BНаравне с рангами выше него, грандмастер имеет 5 дивизионов и требует 6 звезд для повышения ранга.$b$b|cff0000ffДоступные подземелья на этом ранге:|r$b$b'),
('72004','ruRU','|TInterface\\Icons\\icons_rang_epic:25:25:-3:-3|t|cff00ff00Эпик|r $b$b|cff0000ffОписание:|r$B$BЭпик занимает 5-е место после Грандмастера. К этому моменту игра становится все более сложной.$BИспользование стратегий, коммуникации и хорошей командной работы имеет решающее значение для победы, поскольку эти элементы являются ключом к достижению более высоких рангов. $BОбычно на этом ранге находится много опытных и средних игроков, и прогрессировать будет сложнее. $BРанг имеет 5 дивизионов и требует 6 звезд для повышения ранга. $BДля достижения ранга "Легенда", требуется "Эпик I" и 6 звезд.$b$b|cff0000ffДоступные подземелья на этом ранге:|r$b$b'),
('72005','ruRU','|TInterface\\Icons\\icons_rang_legend:25:25:-3:-3|t|cffFF6EB4Легенда|r $b$b|cff0000ffОписание:|r$B$BЛегенда-это 6-й ранг после Эпика. $BИгроки должны обладать отличными навыками общения между товарищами по команде и скоординированными стратегиями, чтобы обеспечить себе победу и продвижение к самому высокому рангу в игре, Мифический. $BРанг имеет 5 дивизионов и требует 6 звезд для повышения ранга. $BДля повышения до ранга "Мифический" требуется "Легенда I" и 6 звезд. $b$b|cff0000ffДоступные подземелья на этом ранге:|r$b$b'),
('72006','ruRU','|TInterface\\Icons\\icons_rang_mific:25:25:-3:-3|t|cffFF4500Мифический|r $b$b|cff0000ffОписание:|r$B$BМифический-это 7-й ранг, после Легенды. $BЭто самый высокий уровень в ранжированном игровом режиме сервера, игроки достигшие этого дивизиона, больше не получат никаких звезд.$b$b|cff0000ffДоступные подземелья на этом ранге:|r$b$b');

DELETE FROM `npc_text_locale` WHERE `ID` BETWEEN 70050 AND 70050;
INSERT INTO `npc_text_locale` (`ID`,`Locale`,`text0_0`) VALUES
('70050','ruRU','< - Назад');

DELETE FROM `gossip_menu` WHERE `MenuID` BETWEEN 50000 AND 50000;
INSERT INTO `gossip_menu` (`MenuID`,`TextID`,`VerifiedBuild`) VALUES
('50000','300000','0'),
('50000','300001','0');

DELETE FROM `gossip_menu` WHERE `MenuID` BETWEEN 50005 AND 50008;
INSERT INTO `gossip_menu` (`MenuID`,`TextID`,`VerifiedBuild`) VALUES
('50005','300004','0'),
('50006','300004','0'),
('50007','300004','0'),
('50008','300004','0');

DELETE FROM `gossip_menu` WHERE `MenuID` BETWEEN 50001 AND 50003;
INSERT INTO `gossip_menu` (`MenuID`,`TextID`,`VerifiedBuild`) VALUES
('50001','300002','0'),
('50002','300002','0'),
('50003','300002','0');

DELETE FROM `gossip_menu_option` WHERE `MenuID` BETWEEN 50000 AND 50000;
INSERT INTO `gossip_menu_option` (`MenuID`,`OptionID`,`OptionText`) VALUES
('50000','1','Server Information'),
('50000','2','Information about the rang'),
('50000','3','Voin  |TInterface\\Icons\\icons_rang_voin:21:21:-3:-3|t'),
('50000','4','Elite  |TInterface\\Icons\\icons_rang_elite:21:21:-3:-3|t'),
('50000','5','Master  |TInterface\\Icons\\icons_rang_master:21:21:-3:-3|t'),
('50000','6','Grandmaster  |TInterface\\Icons\\icons_rang_grandmaster:21:21:-3:-3|t'),
('50000','7','Epic  |TInterface\\Icons\\icons_rang_epic:21:21:-3:-3|t'),
('50000','8','Legenda  |TInterface\\Icons\\icons_rang_legend:21:21:-3:-3|t'),
('50000','9','Mific  |TInterface\\Icons\\icons_rang_mific:21:21:-3:-3|t'),
('50000','10','< - Back');

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` BETWEEN 50000 AND 50000;
INSERT INTO `gossip_menu_option_locale` (`MenuID`,`OptionID`,`Locale`,`OptionText`) VALUES
('50000','1','ruRU','Информация о сервере'),
('50000','2','ruRU','Информация о ранге'),
('50000','3','ruRU','|cffffffffВоин|r  |TInterface\\Icons\\icons_rang_voin:21:21:-3:-3|t'),
('50000','4','ruRU','|cffffff00Элита|r  |TInterface\\Icons\\icons_rang_elite:21:21:-3:-3|t'),
('50000','5','ruRU','|cffFFC125Мастер|r  |TInterface\\Icons\\icons_rang_master:21:21:-3:-3|t'),
('50000','6','ruRU','|cffffcc00Грандмастер|r  |TInterface\\Icons\\icons_rang_grandmaster:21:21:-3:-3|t'),
('50000','7','ruRU','|cff00ff00Эпик|r  |TInterface\\Icons\\icons_rang_epic:21:21:-3:-3|t'),
('50000','8','ruRU','|cffFF6EB4Легенда|r  |TInterface\\Icons\\icons_rang_legend:21:21:-3:-3|t'),
('50000','9','ruRU','|cffFF4500Мифический|r  |TInterface\\Icons\\icons_rang_mific:21:21:-3:-3|t'),
('50000','10','ruRU','< - Назад');

DELETE FROM `npc_text` WHERE `ID` BETWEEN 70000 AND 70001;
INSERT INTO `npc_text` (`ID`,`text0_0`,`VerifiedBuild`) VALUES
('70000','Server Information','-1'),
('70001','Ranks is a rating system for our server players that determines how experienced and not experienced a player is. $b$bPlayers are awarded one star for each win in a ranked game, but they lose a star for each loss in a ranked game. $bWhen a player has full stars at an intra-division level, the next win will result in promotion to the next level within the division. $bHowever, when a player has full stars at level I in their division (for example: Warrior I, Epic I, and Legend I), they will be promoted to the next division. $bWhen a player has no stars on a rank within their rank, the next defeat will cause them to be demoted to the previous lower rank (for example: Epic II → Epic III or Legend IV → Legend V) in their division.$b$b|cff0000ffExisting ranks at the moment:|r','-1');

DELETE FROM `npc_text` WHERE `ID` BETWEEN 71000 AND 71000;
INSERT INTO `npc_text` (`ID`,`text0_0`,`VerifiedBuild`) VALUES
('71000','Server Information','-1');

DELETE FROM `npc_text` WHERE `ID` BETWEEN 71100 AND 71107;
INSERT INTO `npc_text` (`ID`,`text0_0`,`VerifiedBuild`) VALUES
('71100','Rank Information','-1'),
('71101','Voin','-1'),
('71102','Elita','-1'),
('71103','Master','-1'),
('71104','Grandmaster','-1'),
('71105','Epic','-1'),
('71106','Legenda','-1'),
('71107','Mific','-1');

DELETE FROM `npc_text` WHERE `ID` BETWEEN 72000 AND 72006;
INSERT INTO `npc_text` (`ID`,`text0_0`,`VerifiedBuild`) VALUES
('72000','|TInterface\\Icons\\icons_rang_voin:25:25:-3:-3|t|cffffffffVoin|r$b$b|cff0000ffDescription:|r$B$BWarrior is the first rank in the game, which is given to new players when they enter the game. $BIn this rank, players will start at the lowest level, which is "Warrior III". $bTo advance to the next level, for example from "Warrior III" to "Warrior II", you need three stars. $bThis rank only has 3 divisions and it needs 4 stars to rise to the next division/rank. $bRanking up to Elite requires the rank of "Warrior I" and 4 stars to upgrade.$b$b|cff0000ffThe following dungeons are unlocked at this rank:|r$b$bShadowfang Hold, Stormwind Prison, Deadmines, Wailing Caverns, Needlehide Labyrinths, Blackfathom Deeps, Uldaman, Gnomeregan, Sunken Temple, Needlehide Mounds, Scarlet Monastery, ZulFarrak, Blackrock Peak, Blackrock Depths, Blackrock Depths ), Onyxia s Lair (25 player), Dark Portal Opening, Dark Portal Opening (Heroic)$b','-1'),
('72001','|TInterface\\Icons\\icons_rang_elite:25:25:-3:-3|t|cffffff00Elita|r $b$b|cff0000ffDescription:|r$B$BElite is the 2nd rank in the game right after "Warrior".$bYou will start at the lowest level of "Elite III", and you will need 5 stars to climb to the next level, for example: from "Elite III" to "Elite II". $bThe rank has only 3 divisions, but each division requires 5 stars to upgrade the rank instead of the previous 4. $bTo rise to the rank of "Master", you need the rank of "Elite I" and 5 stars.$b$b|cff0000ffAvailable dungeons at this rank:|r$b$b','-1'),
('72002','|TInterface\\Icons\\icons_rang_master:25:25:-3:-3|t|cffFFC125Master|r $b$b|cff0000ffDescription:|r$B$BThe Master takes the 3rd place in the game right after the elite. $BAt this rank, the game becomes a little more difficult. $BThe player starts at the lowest level, "Master IV", and needs 5 stars to advance to the next level, such as from" Master IV "to"Master III". $BUnlike the elite, this rank only has 4 divisions. $BTo get the "Grandmaster" rank, you need to get "Master I" and 5 stars.$b$b|cff0000ffAvailable dungeons at this rank:|r$b$b','-1'),
('72003','|TInterface\\Icons\\icons_rang_grandmaster:25:25:-3:-3|t|cffffcc00Grandmaster|r $b$b|cff0000ffDescription:|r$B$BA Grandmaster is the 4th rank after a Master. $BAt this rank, the game will be more intense and challenging, unlike the previous ranks. $BThe player starts at the lowest level of "Grandmaster V" and needs 6 stars to advance to the next division, such as from "Grandmaster V" to "Grandmaster IV". $BOn par with the ranks above him, the grandmaster has 5 divisions and requires 6 stars to increase the rank.$b$b|cff0000ffAvailable dungeons at this rank:|r$b$b','-1'),
('72004','|TInterface\\Icons\\icons_rang_epic:25:25:-3:-3|t|cff00ff00Epic|r $b$b|cff0000ffDescription:|r$B$BEpic is in the 5th place after the Grandmaster. $B$By this point, the game is getting more and more challenging.$BUsing strategies, communication, and good teamwork is critical to winning, as these elements are key to achieving higher ranks. $BUsually there are a lot of experienced and average players at this rank, and it will be more difficult to progress. $BThe rank has 5 divisions and requires 6 stars to increase the rank. $BTo reach the "Legend" rank, you need an "Epic I" and 6 stars.$b$b|cff0000ffAvailable dungeons at this rank:|r$b$b','-1'),
('72005','|TInterface\\Icons\\icons_rang_legend:25:25:-3:-3|t|cffFF6EB4Legenda|r $b$b|cff0000ffDescription:|r$B$BLegend is the 6th rank after Epic. $BPlayers must have excellent communication skills between teammates and coordinated strategies to ensure they win and advance to the highest rank in the game, Mythical. $BThe rank has 5 divisions and requires 6 stars to increase the rank. $BTo upgrade to the "Mythical" rank, you need "Legend I" and 6 stars. $b$b|cff0000ffAvailable dungeons at this rank:|r$b$b','-1'),
('72006','|TInterface\\Icons\\icons_rang_mific:25:25:-3:-3|t|cffFF4500Mythical|r $b$b|cff0000ffDescription:|r$B$BMythical is the 7th rank, after Legend. $BThis is the highest level in the ranked server game mode, players who reach this division will no longer receive any stars.$b$b|cff0000ffAvailable dungeons at this rank:|r$b$b','-1');

DELETE FROM `npc_text` WHERE `ID` BETWEEN 70050 AND 70050;
INSERT INTO `npc_text` (`ID`,`text0_0`,`VerifiedBuild`) VALUES
('70050','Rank Information','-1');
