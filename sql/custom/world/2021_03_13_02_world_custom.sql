--
UPDATE `creature_template` SET `gossip_menu_id` = 59000, `ScriptName` = 'npc_server_manager' WHERE `entry` = 70000;

DELETE FROM `gossip_menu` WHERE `MenuID` = 50000;
DELETE FROM `gossip_menu_option` WHERE `MenuID` = 50000;
DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 50000;

DELETE FROM `npc_text` WHERE `ID` = 300000;
DELETE FROM `npc_text` WHERE `ID` = 300001;
DELETE FROM `npc_text_locale` WHERE `ID` = 300000;
DELETE FROM `npc_text_locale` WHERE `ID` = 300001;

DELETE FROM `npc_text` WHERE `ID` = 70000;
DELETE FROM `npc_text` WHERE `ID` = 70001;
DELETE FROM `npc_text_locale` WHERE `ID` = 70000;
DELETE FROM `npc_text_locale` WHERE `ID` = 70001;

DELETE FROM `npc_text` WHERE `ID` = 72000;
DELETE FROM `npc_text` WHERE `ID` = 72001;
DELETE FROM `npc_text` WHERE `ID` = 72002;
DELETE FROM `npc_text` WHERE `ID` = 72003;
DELETE FROM `npc_text` WHERE `ID` = 72004;
DELETE FROM `npc_text` WHERE `ID` = 72005;
DELETE FROM `npc_text` WHERE `ID` = 72006;
DELETE FROM `npc_text_locale` WHERE `ID` = 72000;
DELETE FROM `npc_text_locale` WHERE `ID` = 72001;
DELETE FROM `npc_text_locale` WHERE `ID` = 72002;
DELETE FROM `npc_text_locale` WHERE `ID` = 72003;
DELETE FROM `npc_text_locale` WHERE `ID` = 72004;
DELETE FROM `npc_text_locale` WHERE `ID` = 72005;
DELETE FROM `npc_text_locale` WHERE `ID` = 72006;

-- Creature text
DELETE FROM `creature_text` WHERE `CreatureID` = 70000;
INSERT INTO `creature_text` (`CreatureID`, `GroupID`, `ID`, `Text`, `Type`,`Language`, `Probability`, `Emote`, `Duration`, `Sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES
(70000, 0, 0, 'You have successfully applied the skin.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 1, 0, 'Your skin has been successfully reset.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 2, 0, 'You don\'t have an active skin.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 3, 0, 'You have already activated this skin.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 4, 0, 'Unfortunately, you don\'t have enough diamonds.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 5, 0, 'You do not have this profession or you already have the maximum skill in this specialty.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 6, 0, 'Congratulations! You have gained the maximum skill in your chosen profession.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 7, 0, 'In order to change the name, you need to re-sign.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 8, 0, 'You have purchased a character race change! In order to change the race, you need to re-enter.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 9, 0, 'Your level has reached the maximum.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 10, 0, 'You have successfully raised your level to 80.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 11, 0, 'You already have this title.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 12, 0, 'You have successfully acquired the title.', 15, 0, 100, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_text_locale` WHERE `CreatureID` = 70000;
INSERT INTO `creature_text_locale` (`CreatureID`, `GroupID`, `ID`, `Locale`, `Text`) VALUES
(70000, 0, 0, 'ruRU', 'Вы успешно применили скин.'),
(70000, 1, 0, 'ruRU', 'Ваш скин был успешно сброшен.'),
(70000, 2, 0, 'ruRU', 'У Вас нет активного скина.'),
(70000, 3, 0, 'ruRU', 'Вы уже активировали этот скин.'),
(70000, 4, 0, 'ruRU', 'К сожалению у вас недостаточно алмазов.'),
(70000, 5, 0, 'ruRU', 'У вас нет этой профессии или вы уже имеете максимальный навык по данной специальности.'),
(70000, 6, 0, 'ruRU', 'Поздравляем! Вы получили максимальный навык по выбранной профессии.'),
(70000, 7, 0, 'ruRU', 'Вы приобрели смену имени персонажа! Для того, чтобы поменять имя, вам нужно перезайти.'),
(70000, 8, 0, 'ruRU', 'Вы приобрели смену расы персонажа! Для того, чтобы поменять расу, вам нужно перезайти.'),
(70000, 9, 0, 'ruRU', 'Ваш уровень достиг максимального.'),
(70000, 10, 0, 'ruRU', 'Вы успешно повысили свой уровень до 80.'),
(70000, 11, 0, 'ruRU', 'У вас уже имеется данное звание.'),
(70000, 12, 0, 'ruRU', 'Вы успешно приобрели звание.');

-- Main menu

DELETE FROM `npc_text` WHERE `ID` = 76000;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (76000, 'Hello $n! I\\m assistant of this server. You can always find out all the information about the innovations on the server and also about the promotions.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 76000;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(76000, 'ruRU', 'Привет, $n! Я помощник этого сервера. Ты всегда сможешь у меня узнать всю информацию и о нововведениях на сервере и так же о акциях.', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = 59000;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(59000, 76000, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59000;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59000, 0, 0, 'Info about server.', 0, 1, 1, 59001, 0, 0, 0, NULL, 0, 0),
(59000, 1, 0, 'Info about PvP ranks.', 0, 1, 1, 59002, 0, 0, 0, NULL, 0, 0),
(59000, 2, 0, 'Payable skins system.', 0, 1, 1, 59010, 0, 0, 0, NULL, 0, 0),
(59000, 3, 0, '|cff0000ffSale|r.', 0, 1, 1, 59013, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59000;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59000, 0, 'ruRU', 'Информация о сервере.', NULL),
(59000, 1, 'ruRU', 'Информация о рангах.', NULL),
(59000, 2, 'ruRU', 'Система внешнего вида.', NULL),
(59000, 3, 'ruRU', '|cff0000ffМагазин|r.', NULL);

-- Server info

DELETE FROM `npc_text` WHERE `ID` = 76001;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (76001, 'Server Information', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 76001;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(76001, 'ruRU', 'Информация о сервере', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = 59001;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(59001, 76001, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59001;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59001, 0, 0, '<- Prev page', 0, 1, 1, 59000, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59001;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59001, 0, 'ruRU', '<- Назад', NULL);

-- PvP rank info

DELETE FROM `npc_text` WHERE `ID` = 76002;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (76002, 'Ranks is a rating system for our server players that determines how experienced and not experienced a player is. $b$bPlayers are awarded one star for each win in a ranked game, but they lose a star for each loss in a ranked game. $bWhen a player has full stars at an intra-division level, the next win will result in promotion to the next level within the division. $bHowever, when a player has full stars at level I in their division (for example: Warrior I, Epic I, and Legend I), they will be promoted to the next division. $bWhen a player has no stars on a rank within their rank, the next defeat will cause them to be demoted to the previous lower rank (for example: Epic II → Epic III or Legend IV → Legend V) in their division.$b$b|cff0000ffExisting ranks at the moment:|r', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 76002;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(76002, 'ruRU', 'Ранги - это система оценки для игроков нашего сервера, которая определяет, насколько опытен и не опытен игрок. $b$bИгроки награждаются одной звездой за каждую победу в рейтинговой игре, но они теряют звезду за каждое поражение в рейтинговой игре. $bКогда игрок имеет полные звезды на уровне внутри дивизиона, следующая победа приведет к продвижению на следующий уровень внутри дивизиона. $bОднако, когда игрок имеет полные звезды на уровне I в своем дивизионе (например: Воин I, Эпик I и Легенда I), они будут переведены в следующий дивизион. $bКогда у игрока нет звезд на ранге в пределах своего ранга, следующее поражение приведет к тому, что он будет понижен до предыдущего более низкого ранга (например: Эпик II до Эпик III или Легенда IV до Легенда V) в своем дивизионе.$b$b|cff0000ffСуществующие ранги в данный момент:|r', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = 59002;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(59002, 76002, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59002;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59002, 0, 0, 'Voin  |TInterface\\Icons\\icons_rang_voin:21:21:-3:-3|t', 0, 1, 1, 59003, 0, 0, 0, NULL, 0, 0),
(59002, 1, 0, 'Elite  |TInterface\\Icons\\icons_rang_elite:21:21:-3:-3|t', 0, 1, 1, 59004, 0, 0, 0, NULL, 0, 0),
(59002, 2, 0, 'Master  |TInterface\\Icons\\icons_rang_master:21:21:-3:-3|t', 0, 1, 1, 59005, 0, 0, 0, NULL, 0, 0),
(59002, 3, 0, 'Grandmaster  |TInterface\\Icons\\icons_rang_grandmaster:21:21:-3:-3|t', 0, 1, 1, 59006, 0, 0, 0, NULL, 0, 0),
(59002, 4, 0, 'Epic  |TInterface\\Icons\\icons_rang_epic:21:21:-3:-3|t', 0, 1, 1, 59007, 0, 0, 0, NULL, 0, 0),
(59002, 5, 0, 'Legenda  |TInterface\\Icons\\icons_rang_legend:21:21:-3:-3|t', 0, 1, 1, 59008, 0, 0, 0, NULL, 0, 0),
(59002, 6, 0, 'Mific  |TInterface\\Icons\\icons_rang_mific:21:21:-3:-3|t', 0, 1, 1, 59009, 0, 0, 0, NULL, 0, 0),
(59002, 7, 0, '<- Prev page', 0, 1, 1, 59000, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59002;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59002, 0, 'ruRU', '|cffffffffВоин|r  |TInterface\\Icons\\icons_rang_voin:21:21:-3:-3|t', NULL),
(59002, 1, 'ruRU', '|cffffff00Элита|r  |TInterface\\Icons\\icons_rang_elite:21:21:-3:-3|t', NULL),
(59002, 2, 'ruRU', '|cffFFC125Мастер|r  |TInterface\\Icons\\icons_rang_master:21:21:-3:-3|t', NULL),
(59002, 3, 'ruRU', '|cffffcc00Грандмастер|r  |TInterface\\Icons\\icons_rang_grandmaster:21:21:-3:-3|t', NULL),
(59002, 4, 'ruRU', '|cff00ff00Эпик|r  |TInterface\\Icons\\icons_rang_epic:21:21:-3:-3|t', NULL),
(59002, 5, 'ruRU', '|cffFF6EB4Легенда|r  |TInterface\\Icons\\icons_rang_legend:21:21:-3:-3|t', NULL),
(59002, 6, 'ruRU', '|cffFF4500Мифический|r  |TInterface\\Icons\\icons_rang_mific:21:21:-3:-3|t', NULL),
(59002, 7, 'ruRU', '<- Назад', NULL);

-- Voin

DELETE FROM `npc_text` WHERE `ID` = 76003;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (76003, '|TInterface\\Icons\\icons_rang_voin:25:25:-3:-3|t|cffffffffVoin|r$b$b|cff0000ffDescription:|r$B$BWarrior is the first rank in the game, which is given to new players when they enter the game. $BIn this rank, players will start at the lowest level, which is "Warrior III". $bTo advance to the next level, for example from "Warrior III" to "Warrior II", you need three stars. $bThis rank only has 3 divisions and it needs 4 stars to rise to the next division/rank. $bRanking up to Elite requires the rank of "Warrior I" and 4 stars to upgrade.$b$b', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 76003;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(76003, 'ruRU', '|TInterface\\Icons\\icons_rang_voin:25:25:-3:-3|t|cffffffffВоин|r$b$b|cff0000ffОписание:|r$B$BВоин-это первый ранг в игре, который дается новым игрокам, когда они зашли в игру. $BВ этом ранге игроки начнут с самого низкого уровня, который является "Воин III". $bЧтобы подняться на следующий уровень, например от "Воина III" до "Воина II", требуется три звезды. $bЭтот ранг имеет только 3 дивизиона, и ему требуется 4 звезды, чтобы подняться до следующего дивизиона/ранга. $bРейтинг до Элиты требует ранга "Воин I" и 4 звезды для повышения.$b$b', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = 59003;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(59003, 76003, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59003;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59003, 0, 0, '<- Prev page', 0, 1, 1, 59002, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59003;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59003, 0, 'ruRU', '<- Назад', NULL);

-- Elite

DELETE FROM `npc_text` WHERE `ID` = 76004;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (76004, '|TInterface\\Icons\\icons_rang_elite:25:25:-3:-3|t|cffffff00Elita|r $b$b|cff0000ffDescription:|r$B$BElite is the 2nd rank in the game right after "Warrior".$bYou will start at the lowest level of "Elite III", and you will need 5 stars to climb to the next level, for example: from "Elite III" to "Elite II". $bThe rank has only 3 divisions, but each division requires 5 stars to upgrade the rank instead of the previous 4. $bTo rise to the rank of "Master", you need the rank of "Elite I" and 5 stars.$b$b', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 76004;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(76004, 'ruRU', '|TInterface\\Icons\\icons_rang_elite:25:25:-3:-3|t|cffffff00Элита|r $b$b|cff0000ffОписание:|r$B$BЭлита - это 2-й ранг в игре сразу после "Воина". $bВы начнете с самого низкого уровня "Элита III", и вам потребуется 5 звезд, чтобы подняться на следующий уровень, например: от "Элиты III" до "Элиты II". $bРанг имеет только 3 дивизиона, но каждый дивизион требует 5 звезд для повышения ранга вместо предыдущих 4. $bДля поднятия до ранга "Мастер", требуется ранг "Элита I" и 5 звезд.$b$b', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = 59004;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(59004, 76004, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59004;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59004, 0, 0, '<- Prev page', 0, 1, 1, 59002, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59004;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59004, 0, 'ruRU', '<- Назад', NULL);

-- MASTER

DELETE FROM `npc_text` WHERE `ID` = 76005;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (76005, '|TInterface\\Icons\\icons_rang_master:25:25:-3:-3|t|cffFFC125Master|r $b$b|cff0000ffDescription:|r$B$BThe Master takes the 3rd place in the game right after the elite. $BAt this rank, the game becomes a little more difficult. $BThe player starts at the lowest level, "Master IV", and needs 5 stars to advance to the next level, such as from" Master IV "to"Master III". $BUnlike the elite, this rank only has 4 divisions. $BTo get the "Grandmaster" rank, you need to get "Master I" and 5 stars.$b$b', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 76005;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(76005, 'ruRU', '|TInterface\\Icons\\icons_rang_master:25:25:-3:-3|t|cffFFC125Мастер|r $b$b|cff0000ffОписание:|r$B$BМастер занимает 3-е место в игре сразу после элиты. $BНа этом ранге игра становится немного сложнее. $BИгрок начинает с самого низкого уровня "Мастер IV", и ему требуется 5 звезд, чтобы подняться на следующий уровень, например, с "Мастера IV" до "Мастера III". $BВ отличие от элиты, этот ранг имеет только 4 дивизиона. $BЧтобы получить ранг "Грандмастер" требуется получить "Мастер I" и 5 звезд.$b$b', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = 59005;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(59005, 76005, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59005;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59005, 0, 0, '<- Prev page', 0, 1, 1, 59002, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59005;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59005, 0, 'ruRU', '<- Назад', NULL);

-- Grandmaster

DELETE FROM `npc_text` WHERE `ID` = 76006;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (76006, '|TInterface\\Icons\\icons_rang_grandmaster:25:25:-3:-3|t|cffffcc00Grandmaster|r $b$b|cff0000ffDescription:|r$B$BA Grandmaster is the 4th rank after a Master. $BAt this rank, the game will be more intense and challenging, unlike the previous ranks. $BThe player starts at the lowest level of "Grandmaster V" and needs 6 stars to advance to the next division, such as from "Grandmaster V" to "Grandmaster IV". $BOn par with the ranks above him, the grandmaster has 5 divisions and requires 6 stars to increase the rank.$b$b', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 76006;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(76006, 'ruRU', '|TInterface\\Icons\\icons_rang_grandmaster:25:25:-3:-3|t|cffffcc00Грандмастер|r $b$b|cff0000ffОписание:|r$B$BГрандмастер - это 4-й ранг после Мастера. $BНа этом ранге игра будет более напряженной и сложной, в отличие от предыдущих рангов. $BИгрок начинает с самого низкого уровня "Грандмастер V", и ему требуется 6 звезд, чтобы подняться на следующий девизион, например от "Грандмастера V" до "Грандмастера IV". $BНаравне с рангами выше него, грандмастер имеет 5 дивизионов и требует 6 звезд для повышения ранга.$b$b', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = 59006;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(59006, 76006, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59006;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59006, 0, 0, '<- Prev page', 0, 1, 1, 59002, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59006;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59006, 0, 'ruRU', '<- Назад', NULL);

-- Epic

DELETE FROM `npc_text` WHERE `ID` = 76007;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (76007, '|TInterface\\Icons\\icons_rang_epic:25:25:-3:-3|t|cff00ff00Epic|r $b$b|cff0000ffDescription:|r$B$BEpic is in the 5th place after the Grandmaster. $B$By this point, the game is getting more and more challenging.$BUsing strategies, communication, and good teamwork is critical to winning, as these elements are key to achieving higher ranks. $BUsually there are a lot of experienced and average players at this rank, and it will be more difficult to progress. $BThe rank has 5 divisions and requires 6 stars to increase the rank. $BTo reach the "Legend" rank, you need an "Epic I" and 6 stars.$b$b', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 76007;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(76007, 'ruRU', '|TInterface\\Icons\\icons_rang_epic:25:25:-3:-3|t|cff00ff00Эпик|r $b$b|cff0000ffОписание:|r$B$BЭпик занимает 5-е место после Грандмастера. К этому моменту игра становится все более сложной.$BИспользование стратегий, коммуникации и хорошей командной работы имеет решающее значение для победы, поскольку эти элементы являются ключом к достижению более высоких рангов. $BОбычно на этом ранге находится много опытных и средних игроков, и прогрессировать будет сложнее. $BРанг имеет 5 дивизионов и требует 6 звезд для повышения ранга. $BДля достижения ранга "Легенда", требуется "Эпик I" и 6 звезд.$b$b', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = 59007;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(59007, 76007, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59007;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59007, 0, 0, '<- Prev page', 0, 1, 1, 59002, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59007;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59007, 0, 'ruRU', '<- Назад', NULL);

-- Legenda

DELETE FROM `npc_text` WHERE `ID` = 76008;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (76008, '|TInterface\\Icons\\icons_rang_legend:25:25:-3:-3|t|cffFF6EB4Legenda|r $b$b|cff0000ffDescription:|r$B$BLegend is the 6th rank after Epic. $BPlayers must have excellent communication skills between teammates and coordinated strategies to ensure they win and advance to the highest rank in the game, Mythical. $BThe rank has 5 divisions and requires 6 stars to increase the rank. $BTo upgrade to the "Mythical" rank, you need "Legend I" and 6 stars. $b$b', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 76008;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(76008, 'ruRU', '|TInterface\\Icons\\icons_rang_legend:25:25:-3:-3|t|cffFF6EB4Легенда|r $b$b|cff0000ffОписание:|r$B$BЛегенда-это 6-й ранг после Эпика. $BИгроки должны обладать отличными навыками общения между товарищами по команде и скоординированными стратегиями, чтобы обеспечить себе победу и продвижение к самому высокому рангу в игре, Мифический. $BРанг имеет 5 дивизионов и требует 6 звезд для повышения ранга. $BДля повышения до ранга "Мифический" требуется "Легенда I" и 6 звезд. $b$b', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = 59008;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(59008, 76008, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59008;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59008, 0, 0, '<- Prev page', 0, 1, 1, 59002, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59008;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59008, 0, 'ruRU', '<- Назад', NULL);

-- Mific

DELETE FROM `npc_text` WHERE `ID` = 76009;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (76009, '|TInterface\\Icons\\icons_rang_mific:25:25:-3:-3|t|cffFF4500Mythical|r $b$b|cff0000ffDescription:|r$B$BMythical is the 7th rank, after Legend. $BThis is the highest level in the ranked server game mode, players who reach this division will no longer receive any stars.$b$b', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 76009;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(76009, 'ruRU', '|TInterface\\Icons\\icons_rang_mific:25:25:-3:-3|t|cffFF4500Мифический|r $b$b|cff0000ffОписание:|r$B$BМифический-это 7-й ранг, после Легенды. $BЭто самый высокий уровень в ранжированном игровом режиме сервера, игроки достигшие этого дивизиона, больше не получат никаких звезд.$b$b', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = 59009;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(59009, 76009, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59009;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59009, 0, 0, '<- Prev page', 0, 1, 1, 59002, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59009;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59009, 0, 'ruRU', '<- Назад', NULL);

-- End of PvP Rank menu

-- Payable skins

DELETE FROM `npc_text` WHERE `ID` = 76010;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (76010, 'Info about payable skins here', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 76010;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(76010, 'ruRU', 'Здесь напишешь инфу о системе скинов', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = 59010;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(59010, 76010, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59010;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59010, 0, 0, 'Payable skins control panel.', 0, 1, 1, 0, 0, 0, 0, NULL, 0, 0),
(59010, 1, 0, '<- Prev page', 0, 1, 1, 59000, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59010;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59010, 0, 'ruRU', 'Панель управления скинами.', NULL),
(59010, 1, 'ruRU', '<- Назад', NULL);

-- Empty payable skins

DELETE FROM `npc_text` WHERE `ID` = 76011;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (76011, 'You don\'t have any purchased skins.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 76011;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(76011, 'ruRU', 'У вас нету купленых скинов.', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = 59011;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(59011, 76011, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59011;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59011, 0, 0, '<- Main menu', 0, 1, 1, 59000, 0, 0, 0, NULL, 0, 0),
(59011, 1, 0, '<- Prev page', 0, 1, 1, 59010, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59011;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59011, 0, 'ruRU', '<- Главное меню', NULL),
(59011, 1, 'ruRU', '<- Назад', NULL);

-- Payable skins control panel

DELETE FROM `npc_text` WHERE `ID` = 76012;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `Probability0`, `EmoteDelay0_0`, `Emote0_0`, `EmoteDelay0_1`, `Emote0_1`, `EmoteDelay0_2`, `Emote0_2`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `Probability1`, `EmoteDelay1_0`, `Emote1_0`, `EmoteDelay1_1`, `Emote1_1`, `EmoteDelay1_2`, `Emote1_2`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `Probability2`, `EmoteDelay2_0`, `Emote2_0`, `EmoteDelay2_1`, `Emote2_1`, `EmoteDelay2_2`, `Emote2_2`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `Probability3`, `EmoteDelay3_0`, `Emote3_0`, `EmoteDelay3_1`, `Emote3_1`, `EmoteDelay3_2`, `Emote3_2`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `Probability4`, `EmoteDelay4_0`, `Emote4_0`, `EmoteDelay4_1`, `Emote4_1`, `EmoteDelay4_2`, `Emote4_2`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `Probability5`, `EmoteDelay5_0`, `Emote5_0`, `EmoteDelay5_1`, `Emote5_1`, `EmoteDelay5_2`, `Emote5_2`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `Probability6`, `EmoteDelay6_0`, `Emote6_0`, `EmoteDelay6_1`, `Emote6_1`, `EmoteDelay6_2`, `Emote6_2`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `Probability7`, `EmoteDelay7_0`, `Emote7_0`, `EmoteDelay7_1`, `Emote7_1`, `EmoteDelay7_2`, `Emote7_2`, `VerifiedBuild`) VALUES (76012, 'This is list of your payable skins. Choose any of them is you want to apply payable skin.', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

DELETE FROM `npc_text_locale` WHERE `ID` = 76012;
INSERT INTO `npc_text_locale` (`ID`, `Locale`, `Text0_0`, `Text0_1`, `Text1_0`, `Text1_1`, `Text2_0`, `Text2_1`, `Text3_0`, `Text3_1`, `Text4_0`, `Text4_1`, `Text5_0`, `Text5_1`, `Text6_0`, `Text6_1`, `Text7_0`, `Text7_1`) VALUES
(76012, 'ruRU', 'Это список доступных вам скинов. Выберите любой, если хотите его применить.', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

DELETE FROM `gossip_menu` WHERE `MenuID` = 59012;
INSERT INTO `gossip_menu` (`MenuID`, `TextID`, `VerifiedBuild`) VALUES
(59012, 76012, 0);

DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59012;
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59012, 0, 0, 'Reset my appearance.', 0, 1, 1, 0, 0, 0, 0, 'Are you want to reset your appearance?', 0, 0),
(59012, 1, 0, '<- Prev page', 0, 1, 1, 0, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59012;
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59012, 0, 'ruRU', 'Сбросить мой скин.', 'Вы действительно хотите сбросить скин?'),
(59012, 1, 'ruRU', '<- Назад', NULL);
