--
DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59013 AND `OptionID` IN (9, 10);
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59013, 9, 0, 'Change the faction for 400 |TInterface\\Icons\\almaz:20:20:-3:-3|t', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 400 diamonds?', 0, 0),
(59013, 10, 0, '<- Prev page', 0, 1, 1, 59000, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59013 AND `OptionID` IN (9, 10);
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59013, 9, 'ruRU', 'Сменить фракцию за 400 |TInterface\\Icons\\almaz:20:20:-3:-3|t', 'Вы уверены, что хотите потратить 400 алмазов?'),
(59013, 10, 'ruRU', '<- Назад', NULL);

DELETE FROM `creature_text` WHERE `CreatureID` = 70000 AND `GroupID` IN (27, 28);
INSERT INTO `creature_text` (`CreatureID`, `GroupID`, `ID`, `Text`, `Type`,`Language`, `Probability`, `Emote`, `Duration`, `Sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES
(70000, 27, 0, 'You have successfully purchased the service of changing the character\'s faction. To use it, you need to re-enter the game world.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 28, 0, 'You already have the service of changing the character\'s faction. To use it, you need to re-enter the game world.', 15, 0, 100, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_text_locale` WHERE `CreatureID` = 70000 AND `GroupID` IN (27, 28);
INSERT INTO `creature_text_locale` (`CreatureID`, `GroupID`, `ID`, `Locale`, `Text`) VALUES
(70000, 27, 0, 'ruRU', 'Вы успешно приобрели услугу смены фракции персонажа. Чтобы воспользоваться ею, вам нужно перезайти в игровой мир.'),
(70000, 28, 0, 'ruRU', 'У вас уже есть услуга смены фракции персонажа. Чтобы воспользоваться ею, вам нужно перезайти в игровой мир.');
