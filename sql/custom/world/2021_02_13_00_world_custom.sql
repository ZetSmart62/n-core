--
DROP TABLE IF EXISTS `rank_item_rewards`;
CREATE TABLE `rank_item_rewards` (
`rank`  int(11) UNSIGNED NOT NULL ,
`itemReward`  mediumint(8) UNSIGNED NOT NULL ,
`count`  int(11) UNSIGNED NOT NULL ,
PRIMARY KEY (`rank`, `itemReward`));

-- Example
DELETE FROM `rank_item_rewards` WHERE `rank` = 1;
INSERT INTO `rank_item_rewards` (`rank`, `itemReward`, `count`) VALUES
(1, 52, 1),
(1, 53, 2);
