--
-- UNIT_NPC_FLAG_BATTLEMASTER
UPDATE `creature_template` SET `npcflag` = (`npcflag` & ~0x00100000) WHERE `entry` NOT IN (SELECT `entry` FROM `battlemaster_entry`);
