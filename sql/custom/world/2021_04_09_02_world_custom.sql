--
DELETE FROM `gossip_menu_option` WHERE `MenuID` = 59013 AND `OptionID` IN (5, 6, 7, 8);
INSERT INTO `gossip_menu_option` (`MenuID`, `OptionID`, `OptionIcon`, `OptionText`, `OptionBroadcastTextID`, `OptionType`, `OptionNpcFlag`, `ActionMenuID`, `ActionPoiID`, `BoxCoded`, `BoxMoney`, `BoxText`, `BoxBroadcastTextID`, `VerifiedBuild`) VALUES
(59013, 5, 0, 'Reset the CD on the dungeon for 200 |TInterface\\Icons\\almaz:20:20:-3:-3|t', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 200 diamonds?', 0, 0),
(59013, 6, 0, 'Exchange 50 |TInterface\\Icons\\almaz:20:20:-3:-3|t for 200 arena points', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 50 diamonds?', 0, 0),
(59013, 7, 0, 'Exchange 50 |TInterface\\Icons\\almaz:20:20:-3:-3|t for 10000 honor points', 0, 1, 1, 0, 0, 0, 0, 'Are you sure you want to spend 50 diamonds?', 0, 0),
(59013, 8, 0, '<- Prev page', 0, 1, 1, 59000, 0, 0, 0, NULL, 0, 0);

DELETE FROM `gossip_menu_option_locale` WHERE `MenuID` = 59013 AND `OptionID` IN (5, 6, 7, 8);
INSERT INTO `gossip_menu_option_locale` (`MenuID`, `OptionID`, `Locale`, `OptionText`, `BoxText`) VALUES
(59013, 5, 'ruRU', 'Сбросить КД на подземелья за 200 |TInterface\\Icons\\almaz:20:20:-3:-3|t', 'Вы уверены, что хотите потратить 200 алмазов?'),
(59013, 6, 'ruRU', 'Обменять 50 |TInterface\\Icons\\almaz:20:20:-3:-3|t на 200 очков арены', 'Вы уверены, что хотите потратить 50 алмазов?'),
(59013, 7, 'ruRU', 'Обменять 50 |TInterface\\Icons\\almaz:20:20:-3:-3|t на 10000 очков чести', 'Вы уверены, что хотите потратить 50 алмазов?'),
(59013, 8, 'ruRU', '<- Назад', NULL);

DELETE FROM `creature_text` WHERE `CreatureID` = 70000 AND `GroupID` IN (13, 14, 15, 16);
INSERT INTO `creature_text` (`CreatureID`, `GroupID`, `ID`, `Text`, `Type`,`Language`, `Probability`, `Emote`, `Duration`, `Sound`, `BroadcastTextId`, `TextRange`, `comment`) VALUES
(70000, 13, 0, 'You have successfully dropped the CD from all the dungeons.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 14, 0, 'You have successfully made the exchange.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 15, 0, 'You have the maximum number of arena points.', 15, 0, 100, 0, 0, 0, 0, 0, ''),
(70000, 16, 0, 'You have the maximum number of honor points.', 15, 0, 100, 0, 0, 0, 0, 0, '');

DELETE FROM `creature_text_locale` WHERE `CreatureID` = 70000 AND `GroupID` IN (13, 14, 15, 16);
INSERT INTO `creature_text_locale` (`CreatureID`, `GroupID`, `ID`, `Locale`, `Text`) VALUES
(70000, 13, 0, 'ruRU', 'Вы успешно сбросили КД со всех подземелий.'),
(70000, 14, 0, 'ruRU', 'Вы успешно совершили обмен.'),
(70000, 15, 0, 'ruRU', 'У вас максимальное количество очков арены.'),
(70000, 16, 0, 'ruRU', 'У вас максимальное количество очков чести.');
