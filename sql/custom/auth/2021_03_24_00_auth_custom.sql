--
-- Command: custom reload all
DELETE FROM `rbac_permissions` WHERE `id`=1000;
INSERT INTO `rbac_permissions` (`id`,`name`) VALUES
(1000, 'Command: custom reload all');

DELETE FROM `rbac_linked_permissions` WHERE `linkedId`=1000;
INSERT INTO `rbac_linked_permissions` (`id`,`linkedId`) VALUES
(196,1000);

-- Command: custom reload ranks

DELETE FROM `rbac_permissions` WHERE `id`=1001;
INSERT INTO `rbac_permissions` (`id`,`name`) VALUES
(1001, 'Command: custom reload ranks');

DELETE FROM `rbac_linked_permissions` WHERE `linkedId`=1001;
INSERT INTO `rbac_linked_permissions` (`id`,`linkedId`) VALUES
(196,1001);

-- Command: custom reload skins

DELETE FROM `rbac_permissions` WHERE `id`=1002;
INSERT INTO `rbac_permissions` (`id`,`name`) VALUES
(1002, 'Command: custom reload skins');

DELETE FROM `rbac_linked_permissions` WHERE `linkedId`=1002;
INSERT INTO `rbac_linked_permissions` (`id`,`linkedId`) VALUES
(196,1002);
