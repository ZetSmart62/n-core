--
DELETE FROM `rbac_permissions` WHERE `id`=1000;
INSERT INTO `rbac_permissions` (`id`,`name`) VALUES
(1000, 'Command: reload ranks');

DELETE FROM `rbac_linked_permissions` WHERE `linkedId`=1000;
INSERT INTO `rbac_linked_permissions` (`id`,`linkedId`) VALUES
(196,1000);

DELETE FROM `rbac_permissions` WHERE `id`=1001;
INSERT INTO `rbac_permissions` (`id`,`name`) VALUES
(1001, 'Command: reload payable_skins');

DELETE FROM `rbac_linked_permissions` WHERE `linkedId`=1001;
INSERT INTO `rbac_linked_permissions` (`id`,`linkedId`) VALUES
(196,1001);
