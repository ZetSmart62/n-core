--
-- Command: custom skin activate
DELETE FROM `rbac_permissions` WHERE `id`=1003;
INSERT INTO `rbac_permissions` (`id`,`name`) VALUES
(1003, 'Command: custom skin activate');

DELETE FROM `rbac_linked_permissions` WHERE `linkedId`=1003;
INSERT INTO `rbac_linked_permissions` (`id`,`linkedId`) VALUES
(196,1003);

-- Command: custom skin add

DELETE FROM `rbac_permissions` WHERE `id`=1004;
INSERT INTO `rbac_permissions` (`id`,`name`) VALUES
(1004, 'Command: custom skin add');

DELETE FROM `rbac_linked_permissions` WHERE `linkedId`=1004;
INSERT INTO `rbac_linked_permissions` (`id`,`linkedId`) VALUES
(196,1004);

-- Command: custom skin deactivates

DELETE FROM `rbac_permissions` WHERE `id`=1005;
INSERT INTO `rbac_permissions` (`id`,`name`) VALUES
(1005, 'Command: custom skin deactivate');

DELETE FROM `rbac_linked_permissions` WHERE `linkedId`=1005;
INSERT INTO `rbac_linked_permissions` (`id`,`linkedId`) VALUES
(196,1005);

-- Command: custom skin list

DELETE FROM `rbac_permissions` WHERE `id`=1006;
INSERT INTO `rbac_permissions` (`id`,`name`) VALUES
(1006, 'Command: custom skin list');

DELETE FROM `rbac_linked_permissions` WHERE `linkedId`=1006;
INSERT INTO `rbac_linked_permissions` (`id`,`linkedId`) VALUES
(196,1006);

-- Command: custom skin remove

DELETE FROM `rbac_permissions` WHERE `id`=1007;
INSERT INTO `rbac_permissions` (`id`,`name`) VALUES
(1007, 'Command: custom skin remove');

DELETE FROM `rbac_linked_permissions` WHERE `linkedId`=1007;
INSERT INTO `rbac_linked_permissions` (`id`,`linkedId`) VALUES
(196,1007);

-- Command: custom lookup skin

DELETE FROM `rbac_permissions` WHERE `id`=1008;
INSERT INTO `rbac_permissions` (`id`,`name`) VALUES
(1008, 'Command: custom lookup skin');

DELETE FROM `rbac_linked_permissions` WHERE `linkedId`=1008;
INSERT INTO `rbac_linked_permissions` (`id`,`linkedId`) VALUES
(196,1008);
