DROP TABLE IF EXISTS `character_pvp_stats`;
CREATE TABLE `character_pvp_stats` (
  `guid` int UNSIGNED NOT NULL,
  `bgType` tinyint UNSIGNED NOT NULL,
  `arenaType` tinyint UNSIGNED NOT NULL,
  `games` int UNSIGNED NULL DEFAULT 0,
  `wins` int UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`guid`, `bgType`, `arenaType`)
);
