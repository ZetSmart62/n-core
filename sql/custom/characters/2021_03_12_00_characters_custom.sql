--
DROP TABLE IF EXISTS `character_payable_skins`;
CREATE TABLE `character_payable_skins` (
`guid` int(10) UNSIGNED NOT NULL,
`payableSkin` int(10) UNSIGNED NOT NULL,
`active` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
PRIMARY KEY (`guid`, `payableSkin`));
