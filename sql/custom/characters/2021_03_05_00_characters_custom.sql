--
DROP TABLE IF EXISTS `character_pvp_rank_rewarded`;
CREATE TABLE `character_pvp_rank_rewarded` (
`guid`  int(11) UNSIGNED NOT NULL ,
`rank`  int(10) UNSIGNED NOT NULL ,
PRIMARY KEY (`guid`, `rank`));
