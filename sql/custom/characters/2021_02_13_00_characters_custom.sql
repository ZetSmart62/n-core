--
DROP TABLE IF EXISTS `character_pvp_ranks`;
CREATE TABLE `character_pvp_ranks` (
`guid`  int(10) UNSIGNED NOT NULL ,
`rank`  int(11) UNSIGNED NOT NULL ,
PRIMARY KEY (`guid`));
