#ifndef RANK_SYSTEM_H
#define RANK_SYSTEM_H

#include "Player.h"
#include <map>

namespace NewWoW::RankSystem
{
    inline constexpr uint32 ITEM_TOKEN = 70000;

    using ItemRewardMap = std::map<uint32, uint32>;

    struct Rank
    {
        std::string title;
        std::string icon;
        std::string color;
        ItemRewardMap rewards;
    };

    using RankMap = std::map<uint32, Rank>;

    class TC_GAME_API RankMgr
    {
    private:
        RankMgr();
        ~RankMgr();

    public:
        static RankMgr* instance();

        void LoadFromDB();

        std::string GetTitleFor(uint32 rank, bool withIcon = true, bool colored = true, bool withBrackets = true) const;
        std::string GetTitleFor(Player* player, bool withIcon = true, bool colored = true, bool withBrackets = true) const;

        ItemRewardMap const* GetItemRewardsForRank(uint32 rank) const;

        uint32 ValidateRank(uint32 rank) const;

    private:
        RankMap rankContainer;
    };
}

#define sRankMgr NewWoW::RankSystem::RankMgr::instance()

#endif
