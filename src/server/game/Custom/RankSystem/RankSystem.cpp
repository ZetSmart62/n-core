#include "RankSystem.h"
using namespace NewWoW::RankSystem;

RankMgr::RankMgr() { }
RankMgr::~RankMgr() { }

RankMgr* RankMgr::instance()
{
    static RankMgr instance;
    return &instance;
}

void RankMgr::LoadFromDB()
{
    rankContainer.clear();

    //                                                  0       1   2       3
    QueryResult result = WorldDatabase.Query("SELECT `rank`, title, icon, color FROM rank_titles");
    if (!result)
    {
        TC_LOG_INFO("server.loading", ">> Loaded 0 ranks and titles. DB table `rank_titles` is empty!");
        return;
    }

    uint32 oldMSTimer = getMSTime();
    uint32 rowsCount = 0;
    do
    {
        Field* fields = result->Fetch();

        Rank& rank  = rankContainer[fields[0].GetUInt32()];
        rank.title  = fields[1].GetString();
        rank.icon   = fields[2].GetString();
        rank.color  = fields[3].GetString();

        ++rowsCount;
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u ranks and titles in %u ms", rowsCount, GetMSTimeDiffToNow(oldMSTimer));

    //                                      0   1           2
    result = WorldDatabase.Query("SELECT `rank`, itemReward, count FROM rank_item_rewards");
    if (!result)
    {
        TC_LOG_INFO("server.loading", ">> Loaded 0 rank item rewards. DB table `rank_item_rewards` is empty!");
        return;
    }

    oldMSTimer = getMSTime();
    rowsCount = 0;
    do
    {
        Field* fields = result->Fetch();

        uint32 rank         = fields[0].GetUInt32();
        uint32 itemReward   = fields[1].GetUInt32();
        uint32 count        = fields[2].GetUInt32();

        if (!ValidateRank(rank))
        {
            TC_LOG_ERROR("sql.sql", "Table `rank_item_rewards` has a row with non-existing rank %u", rank);
            continue;
        }

        rankContainer[rank].rewards.emplace(itemReward, count);
        ++rowsCount;
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u rank item rewards in %u ms", rowsCount, GetMSTimeDiffToNow(oldMSTimer));
}

std::string RankMgr::GetTitleFor(uint32 rank, bool withIcon /*= true*/, bool colored /*= true*/, bool withBrackets /*= true*/) const
{
    RankMap::const_iterator itr = rankContainer.find(rank);
    if (itr == rankContainer.end())
        return "";

    std::string title;
    withIcon &= !itr->second.icon.empty();
    colored &= !itr->second.color.empty();

    if (withIcon)
        title += "|TInterface\\Icons\\" + itr->second.icon + ":18:18:-3:-3|t";

    if (withBrackets)
        title += "[";

    if (colored)
        title += "|c" + itr->second.color + itr->second.title + "|r";
    else
        title += itr->second.title;

    if (withBrackets)
        title += "]";

    return title;
}

std::string RankMgr::GetTitleFor(Player* player, bool withIcon /*= true*/, bool colored /*= true*/, bool withBrackets /*= true*/) const
{
    if (!player)
        return "";

    return GetTitleFor(player->GetPvPRank(), withIcon, colored, withBrackets);
}

ItemRewardMap const* RankMgr::GetItemRewardsForRank(uint32 rank) const
{
    RankMap::const_iterator itr = rankContainer.find(rank);
    if (itr == rankContainer.end())
        return nullptr;

    return &itr->second.rewards;
}

uint32 RankMgr::ValidateRank(uint32 rank) const
{
    if (rank == 0)
        return 0;

    if (rankContainer.find(rank) != rankContainer.end())
        return rank;

    return 0;
}
