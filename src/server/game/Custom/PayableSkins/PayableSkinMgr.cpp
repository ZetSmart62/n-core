#include "PayableSkinMgr.h"
#include "MapManager.h"
#include "CreatureAI.h"

PayableSkinMgr::PayableSkinMgr() { }

PayableSkinMgr::~PayableSkinMgr()
{
    ClearSystem();
}

PayableSkinMgr* PayableSkinMgr::instance()
{
    static PayableSkinMgr instance;
    return &instance;
}

void PayableSkinMgr::LoadFromDB()
{
    PayableSkinRegisteredVendorContainer savedVendors = m_payableSkinRegisteredVendorStore;
    ClearSystem();

    LoadPayableSkinTemplates();
    LoadPayableSkinCreatureVendors();

    ReloadVendors(savedVendors);
}

void PayableSkinMgr::LoadPayableSkinTemplates()
{
    uint32 oldMSTimer = getMSTime();
    uint32 count = 0;

    //                                              0       1       2           3       4       5       6       7       8
    QueryResult result = WorldDatabase.Query("SELECT id, name, nameColor, DisplayId, auras, spells, classmask, gender, price FROM payable_skin_template");
    if (!result)
    {
        TC_LOG_INFO("server.loading", ">> Loaded 0 payable skins. DB table `payable_skin_template` is empty.");
        return;
    }

    do
    {
        Field* fields = result->Fetch();
        PayableSkin* payableSkin = new PayableSkin();

        if (!payableSkin->LoadFromDB(fields))
        {
            delete payableSkin;
            continue;
        }

        AddPayableSkin(payableSkin);
        ++count;
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u payable skins in %u ms", count, GetMSTimeDiffToNow(oldMSTimer));
}

void PayableSkinMgr::LoadPayableSkinCreatureVendors()
{
    uint32 oldMSTimer = getMSTime();
    uint32 count = 0;

    //                                                  0               1
    QueryResult result = WorldDatabase.Query("SELECT npcEntryOrGuid, payableSkin FROM payable_skin_creature_vendors");
    if (!result)
    {
        TC_LOG_INFO("server.loading", ">> Loaded 0 payable skin vendors. DB table `payable_skin_creature_vendors` is empty.");
        return;
    }

    do
    {
        Field* fields                   = result->Fetch();
        int32 entryOrGuid               = fields[0].GetInt32();
        uint32 payableSkinId            = fields[1].GetUInt32();

        if (entryOrGuid >= 0)
        {
            if (!sObjectMgr->GetCreatureTemplate(static_cast<uint32>(entryOrGuid)))
            {
                TC_LOG_ERROR("sql.sql", "Table `payable_skin_creature_vendors` has a record with non-existing creature entry (%u)", static_cast<uint32>(entryOrGuid));
                continue;
            }
        }
        else
        {
            CreatureData const* creature = sObjectMgr->GetCreatureData(static_cast<uint32>(std::abs(entryOrGuid)));
            if (!creature)
            {
                TC_LOG_ERROR("sql.sql", "Table `payable_skin_creature_vendors` has a record with non-existing creature guid (%u)", static_cast<uint32>(std::abs(entryOrGuid)));
                continue;
            }

            if (!sObjectMgr->GetCreatureTemplate(creature->id))
            {
                TC_LOG_ERROR("sql.sql", "Table `payable_skin_creature_vendors` has a record with non-existing creature entry (%u) guid (%u)", creature->id, static_cast<uint32>(std::abs(entryOrGuid)));
                continue;
            }
        }

        PayableSkin const* payableSkin = GetPayableSkinById(payableSkinId);
        if (!payableSkin)
        {
            TC_LOG_ERROR("sql.sql", "Table `payable_skin_creature_vendors` has a record (`npcEntryOrGuid`: %u) with non-existing payable skin (%u)", entryOrGuid, payableSkinId);
            continue;
        }

        m_payableSkinVendorStore.emplace(entryOrGuid, payableSkin);
        ++count;
    } while (result->NextRow());

    TC_LOG_INFO("server.loading", ">> Loaded %u payable skin vendors in %u ms", count, GetMSTimeDiffToNow(oldMSTimer));
}

bool PayableSkinMgr::Buy(Player* player, uint32 payableSkinId)
{
    ASSERT_NOTNULL(player);
    PayableSkin const* payableSkin = GetPayableSkinById(payableSkinId);
    if (!payableSkin)
        return false;

    PlayerPayableSkin const* playerPayableSkin = player->GetPayableSkin(payableSkinId);
    if (playerPayableSkin && !playerPayableSkin->IsTemporary()) // Should not happen.
        return false;

    if (!payableSkin->IsAllowedFor(player->GetGUID())) // Should not happen.
        return false;

    if (!player->HasItemCount(NewWoW::Donate::Currency, payableSkin->GetPrice()))
    {
        player->SendBuyError(BUY_ERR_NOT_ENOUGHT_MONEY, nullptr, 0, 0);
        return false;
    }

    player->DestroyItemCount(NewWoW::Donate::Currency, payableSkin->GetPrice(), true);
    player->AddPayableSkin(payableSkinId);

    return true;
}

PayableSkinContainer const& PayableSkinMgr::GetPayableSkins() const
{
    return m_payableSkinStore;
}

PayableSkin const* PayableSkinMgr::GetPayableSkinById(uint32 payableSkinId) const
{
    PayableSkinContainer::const_iterator itr = m_payableSkinStore.find(payableSkinId);
    if (itr != m_payableSkinStore.end())
        return itr->second;

    return nullptr;
}

PayableSkin const* PayableSkinMgr::GetPayableSkinByVendor(Creature const* creature) const
{
    ASSERT_NOTNULL(creature);
    PayableSkinVendorContainer::const_iterator itr = m_payableSkinVendorStore.find(-static_cast<int32>(creature->GetGUID().GetCounter()));
    if (itr != m_payableSkinVendorStore.end())
        return itr->second;

    itr = m_payableSkinVendorStore.find(static_cast<int32>(creature->GetEntry()));
    if (itr != m_payableSkinVendorStore.end())
        return itr->second;

    return nullptr;
}

void PayableSkinMgr::AddPayableSkin(PayableSkin* payableSkin)
{
    m_payableSkinStore[payableSkin->GetId()] = payableSkin;
}

void PayableSkinMgr::RemovePayableSkin(uint32 payableSkinId)
{
    m_payableSkinStore.erase(payableSkinId);
}

void PayableSkinMgr::RegisterVendor(ObjectGuid guid)
{
    m_payableSkinRegisteredVendorStore.insert(guid);
}

void PayableSkinMgr::UnRegisterVendor(ObjectGuid guid)
{
    m_payableSkinRegisteredVendorStore.erase(guid);
}

void PayableSkinMgr::ClearSystem()
{
    for (PayableSkinContainer::iterator itr = m_payableSkinStore.begin(); itr != m_payableSkinStore.end(); ++itr)
        delete itr->second;

    m_payableSkinStore.clear();
    m_payableSkinVendorStore.clear();
    m_payableSkinRegisteredVendorStore.clear();
}

void PayableSkinMgr::ReloadVendors(PayableSkinRegisteredVendorContainer& vendors)
{
    sMapMgr->DoForAllMaps([&](Map* map)
    {
        for (ObjectGuid const& guid : vendors)
        {
            Creature* vendor = map->GetCreature(guid);
            if (!vendor)
                continue;

            vendor->AI()->InitializeAI();
            vendor->m_Events.AddEventAtOffset([vendor]()
            {
                vendor->CastSpell(vendor, SPELL_PAYABLE_SKINS_VISUAL_EFFECT, true);
            }, 0ms);
        }
    });
}
