#include "PayableSkin.h"
#include "StringConvert.h"
#include "SpellMgr.h"
#include "SpellInfo.h"
#include "CharacterCache.h"

bool PayableSkin::LoadFromDB(Field* fields)
{
    //          0   1       2           3       4       5       6           7       8
    // SELECT id, name, nameColor, DisplayId, auras, spells, classmask, gender, price FROM payable_skin_template
    m_id                                = fields[0].GetUInt32();
    m_name                              = fields[1].GetString();
    m_nameColor                         = fields[2].GetString();
    if (!ValidateDisplayId               (fields[3].GetUInt32()))           return false;
    if (!TokenizeAndValidateSpellString  (fields[4].GetStringView(), true)) return false;
    if (!TokenizeAndValidateSpellString  (fields[5].GetStringView()))       return false;
    if (!ValidateClassMask               (fields[6].GetUInt32()))           return false;
    if (!ValidateGender                  (fields[7].GetUInt8()))            return false;
    m_price                             = fields[8].GetUInt32();

    return true;
}

bool PayableSkin::IsAllowedFor(ObjectGuid playerGuid) const
{
    CharacterCacheEntry const* cache = sCharacterCache->GetCharacterCacheByGuid(playerGuid);
    if (!cache) // There is no such character
        return false;

    uint32 classMask = 1 << (cache->Class - 1);
    PayableSkinGender gender = cache->Sex == GENDER_MALE ? PayableSkinGender::Male : PayableSkinGender::Female;

    if (m_classmask != 0 && !(m_classmask & classMask))
        return false;

    if (m_gender != PayableSkinGender::All && m_gender != gender)
        return false;

    return true;
}

std::string PayableSkin::GetName(bool colored /*= true*/) const
{
    if (colored && !m_nameColor.empty())
        return "|c" + m_nameColor + m_name + "|r";
    else
        return m_name;
}

std::string PayableSkin::ToString(bool colored /*= true*/) const
{
    return GetName(colored) + " (ID: " + std::to_string(m_id) + ")";
}

bool PayableSkin::ValidateDisplayId(uint32 displayId)
{
    m_displayId = displayId;
    if (sCreatureDisplayInfoStore.LookupEntry(m_displayId) != nullptr)
        return true;

    TC_LOG_ERROR("sql.sql", "Payable skin (ID: %u) has model for nonexistent display id (%u) in `payable_skin_template`.", m_id, m_displayId);
    return false;
}

bool PayableSkin::TokenizeAndValidateSpellString(std::string_view string, bool isAuras /*= false*/)
{
    PayableSkinSpellsContainer& container = isAuras ? m_auras : m_spells;
    std::string field = isAuras ? "auras" : "spells";

    for (std::string_view spell : Trinity::Tokenize(string, ' ', false))
    {
        SpellInfo const* spellInfo = nullptr;
        if (Optional<uint32> spellId = Trinity::StringTo<uint32>(spell))
            spellInfo = sSpellMgr->GetSpellInfo(*spellId);

        if (!spellInfo)
        {
            TC_LOG_ERROR("sql.sql", "Payable skin (ID: %u) has wrong spell '%s' defined in `%s` field in `payable_skin_template`.", m_id, std::string(spell).c_str(), field.c_str());
            return false;
        }

        if (std::find(container.begin(), container.end(), spellInfo->Id) != container.end())
        {
            TC_LOG_ERROR("sql.sql", "Payable skin (ID: %u) has duplicate spell '%s' defined in `%s` field in `payable_skin_template`.", m_id, std::string(spell).c_str(), field.c_str());
            return false;
        }

        if (isAuras && spellInfo->GetDuration() > 0)
        {
            TC_LOG_ERROR("sql.sql", "Payable skin (ID: %u) has temporary aura '%s' defined in `auras` field in `payable_skin_template`.", m_id, std::string(spell).c_str());
            return false;
        }

        container.push_back(spellInfo->Id);
    }

    return true;
}

bool PayableSkin::ValidateClassMask(uint32 classmask)
{
    m_classmask = classmask;
    if (m_classmask == 0 || (m_classmask & CLASSMASK_ALL_PLAYABLE) != 0)
        return true;

    TC_LOG_ERROR("sql.sql", "Payable skin (ID: %u) has wrong class mask %u in `payable_skin_template`.", m_id, m_classmask);
    return false;
}

bool PayableSkin::ValidateGender(uint8 gender)
{
    m_gender = static_cast<PayableSkinGender>(gender);
    if (m_gender == PayableSkinGender::All || m_gender == PayableSkinGender::Male || m_gender == PayableSkinGender::Female)
        return true;

    TC_LOG_ERROR("sql.sql", "Payable skin (ID: %u) has wrong gender %u in `payable_skin_template`.", m_id, static_cast<uint8>(m_gender));
    return false;
}
