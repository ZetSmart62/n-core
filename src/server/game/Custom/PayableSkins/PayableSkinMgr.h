#ifndef _PAYABLE_SKIN_MGR_H
#define _PAYABLE_SKIN_MGR_H

#include "PayableSkin.h"

using PayableSkinContainer = std::unordered_map<uint32, PayableSkin*>;
using PayableSkinVendorContainer = std::unordered_map<int32, PayableSkin const*>;
using PayableSkinRegisteredVendorContainer = std::set<ObjectGuid>;

inline constexpr uint32 SPELL_PAYABLE_SKINS_VISUAL_EFFECT = 40162;

class TC_GAME_API PayableSkinMgr
{
private:
    PayableSkinMgr();
    ~PayableSkinMgr();
    PayableSkinMgr(PayableSkinMgr const&) = delete;
    PayableSkinMgr& operator=(PayableSkinMgr const&) = delete;

public:
    static PayableSkinMgr* instance();

    void LoadFromDB();

    bool Buy(Player* player, uint32 payableSkinId);

    PayableSkinContainer const& GetPayableSkins() const;
    PayableSkin const* GetPayableSkinById(uint32 payableSkinId) const;
    PayableSkin const* GetPayableSkinByVendor(Creature const* creature) const;

    void AddPayableSkin(PayableSkin* payableSkin);
    void RemovePayableSkin(uint32 payableSkinId);

    void RegisterVendor(ObjectGuid guid);
    void UnRegisterVendor(ObjectGuid guid);

private:
    void LoadPayableSkinTemplates();
    void LoadPayableSkinCreatureVendors();

    void ClearSystem();
    void ReloadVendors(PayableSkinRegisteredVendorContainer& vendors);

    PayableSkinContainer m_payableSkinStore;
    PayableSkinVendorContainer m_payableSkinVendorStore;
    PayableSkinRegisteredVendorContainer m_payableSkinRegisteredVendorStore;
};

#define sPayableSkinMgr PayableSkinMgr::instance()

#endif // _PAYABLE_SKIN_MGR_H
