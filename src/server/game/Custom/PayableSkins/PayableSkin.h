#ifndef _PAYABLE_SKIN_H
#define _PAYABLE_SKIN_H

#include <set>
#include <string>
#include "Define.h"

using PayableSkinAurasContainer = std::vector<uint32>;
using PayableSkinSpellsContainer = std::vector<uint32>;

enum PayableSkinGender
{
    All = 0,
    Male,
    Female
};

class TC_GAME_API PayableSkin
{
public:
    PayableSkin() : m_id(0), m_displayId(0), m_classmask(0), m_gender(0), m_price(0) { }
    ~PayableSkin() { }

    bool LoadFromDB(Field* fields);

    bool IsAllowedFor(ObjectGuid playerGuid) const;

    uint32 GetId() const { return m_id; }
    std::string GetName(bool colored = true) const;
    uint32 GetDisplayId() const { return m_displayId; }
    PayableSkinAurasContainer const& GetAuras() const { return m_auras; }
    PayableSkinSpellsContainer const& GetSpells() const { return m_spells; }
    uint32 GetPrice() const { return m_price; }

    std::string ToString(bool colored = true) const;

private:
    bool ValidateDisplayId(uint32 display);
    bool TokenizeAndValidateSpellString(std::string_view string, bool isAuras = false);
    bool ValidateClassMask(uint32 classmask);
    bool ValidateGender(uint8 genderId);

    uint32 m_id;
    std::string m_name;
    std::string m_nameColor;
    uint32 m_displayId;
    PayableSkinAurasContainer m_auras;
    PayableSkinSpellsContainer m_spells;
    uint32 m_classmask;
    uint8 m_gender;
    uint32 m_price;
};


#endif // _PAYABLE_SKIN_H
