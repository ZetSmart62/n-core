#include "Chat.h"
#include "Item.h"
#include "RankSystem.h"

class player_rank_system : public PlayerScript
{
public:
    player_rank_system() : PlayerScript("player_rank_system") { }

    void OnCurrencyChanged(Player* player, uint32 entry, uint32 count) override
    {
        if (entry != NewWoW::RankSystem::ITEM_TOKEN)
            return;

        uint32 rank = sRankMgr->ValidateRank(count);

        if (rank > player->GetPvPRank())
        {
            // Rank-up reward
            if (!player->IsPvPRankRewarded(rank))
            {
                if (NewWoW::RankSystem::ItemRewardMap const* rewards = sRankMgr->GetItemRewardsForRank(rank))
                {
                    for (auto [item, count] : *rewards)
                        player->AddItem(item, count);

                    player->SetPvPRankRewarded(rank, true);
                }
            }

            ChatHandler(player->GetSession()).PSendSysMessage(LANG_PVP_RANK_EARNED, sRankMgr->GetTitleFor(rank, false, true, false));
        }

        player->SetPvPRank(rank);
    }
};

class player_rank_system_chat_tags : public PlayerScript
{
public:
    player_rank_system_chat_tags() : PlayerScript("player_rank_system_chat_tags") { }

    void OnChat(Player* player, uint32 /*type*/, uint32 /*lang*/, std::string& msg) override
    {
        AddRankTagToMessage(player, msg);
    }

    void OnChat(Player* player, uint32 /*type*/, uint32 /*lang*/, std::string& msg, Player* /*receiver*/) override
    {
        AddRankTagToMessage(player, msg);
    }

    void OnChat(Player* player, uint32 /*type*/, uint32 /*lang*/, std::string& msg, Group* /*group*/) override
    {
        AddRankTagToMessage(player, msg);
    }

    void OnChat(Player* player, uint32 /*type*/, uint32 /*lang*/, std::string& msg, Guild* /*guild*/) override
    {
        AddRankTagToMessage(player, msg);
    }

    void OnChat(Player* player, uint32 /*type*/, uint32 /*lang*/, std::string& msg, Channel* /*channel*/) override
    {
        AddRankTagToMessage(player, msg);
    }

private:
    void AddRankTagToMessage(Player* player, std::string& msg)
    {
        msg.insert(0, sRankMgr->GetTitleFor(player) + " ");
    }
};

void AddSC_rank_system()
{
    new player_rank_system();
    new player_rank_system_chat_tags();
}
