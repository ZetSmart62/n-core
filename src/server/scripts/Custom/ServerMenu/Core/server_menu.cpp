#include "server_menu.h"
#include "PvPStatsCache/PvPStatsCacheMgr.h"
#include "Battleground.h"
#include "CharacterCache.h"
#include "ScriptedCreature.h"
#include "Player.h"
#include "WorldSession.h"
#include "Language.h"
#include "PayableSkin.h"
#include "PayableSkinMgr.h"
#include "RankSystem.h"
#include "DBCStores.h"
#include "World.h"
#include "Chat.h"
#include "Group.h"
#include "Guild.h"
#include "GameTime.h"
#include "CreatureTextMgr.h"
#include "ObjectMgr.h"

using namespace ServerMenu;

Manager::Manager() { }

Manager::~Manager() { }

Manager* Manager::instance()
{
    static Manager instance;
    return &instance;
}

static OptionHandlerStore const handlers =
{
    { { Menu::Id,                               Menu::Option::TeleportToClassArea                                   }, &Manager::HandleTeleportToClassAreaOption                     },
    { { Menu::PvPStatsInfo::Id,                 Menu::PvPStatsInfo::Option::ShowMyPvPStats                          }, &Manager::HandlePvPStatsInfoShowMyPvPStatsOption              },
    { { Menu::PvPStatsInfo::Id,                 Menu::PvPStatsInfo::Option::ShowPvPStatsOfMyTarget                  }, &Manager::HandlePvPStatsInfoShowPvPStatsOfMyTargetOption      },
    { { Menu::PvPStatsInfo::Id,                 Menu::PvPStatsInfo::Option::ShowPvPStatsOfAnotherPlayer             }, &Manager::HandlePvPStatsInfoShowPvPStatsOfAnotherPlayerOption },
    { { Menu::PvPStatsInfo::PvPStats::Id,       Menu::PvPStatsInfo::PvPStats::Option::ShareToSayChannel::Action     }, &Manager::HandlePvPStatsInfoPvPStatsShareToSayChannelOption   },
    { { Menu::PvPStatsInfo::PvPStats::Id,       Menu::PvPStatsInfo::PvPStats::Option::ShareToGroupChannel::Action   }, &Manager::HandlePvPStatsInfoPvPStatsShareToGroupChannelOption },
    { { Menu::PvPStatsInfo::PvPStats::Id,       Menu::PvPStatsInfo::PvPStats::Option::ShareToGuildChannel::Action   }, &Manager::HandlePvPStatsInfoPvPStatsShareToGuildChannelOption },
    { { Menu::PvPStatsInfo::PvPStats::Id,       Menu::PvPStatsInfo::PvPStats::Option::PrevPage::Action              }, &Manager::HandlePvPStatsInfoPvPStatsPrevPageOption            },
    { { Menu::PayableSkins::Id,                 Menu::PayableSkins::Option::ControlPanel                            }, &Manager::HandlePayableSkinsControlPanelOption                },
    { { Menu::PayableSkins::ControlPanel::Id,   Menu::PayableSkins::ControlPanel::Option::Reset::Action             }, &Manager::HandlePayableSkinsControlPanelResetOption           },
    { { Menu::PayableSkins::ControlPanel::Id,   Menu::PayableSkins::ControlPanel::Option::PrevPage::Action          }, &Manager::HandlePayableSkinsControlPanelPrevPageOption        },
    { { Menu::PayableSkins::ControlPanel::Id,   Any::Action                                                         }, &Manager::HandlePayableSkinsControlPanelActivateOption        },
    { { Menu::Shop::Id,                         Menu::Shop::Option::CharacterRename                                 }, &Manager::HandleShopCharacterRenameOption                     },
    { { Menu::Shop::Id,                         Menu::Shop::Option::CharacterChangeRace                             }, &Manager::HandleShopCharacterChangeRaceOption                 },
    { { Menu::Shop::Id,                         Menu::Shop::Option::CharacterLevelBoost                             }, &Manager::HandleShopCharacterLevelBoostOption                 },
    { { Menu::Shop::Id,                         Menu::Shop::Option::UnbindInstances                                 }, &Manager::HandleShopUnbindInstancesOption                     },
    { { Menu::Shop::Id,                         Menu::Shop::Option::ArenaPoints                                     }, &Manager::HandleShopArenaPointsOption                         },
    { { Menu::Shop::Id,                         Menu::Shop::Option::HonorPoints                                     }, &Manager::HandleShopHonorPointsOption                         },
    { { Menu::Shop::Professions::Id,            Any::Option                                                         }, &Manager::HandleShopProfessionsIncreaseOption                 },
    { { Menu::Shop::Titles::Id,                 Any::Option                                                         }, &Manager::HandleShopTitlesBuyOption                           },
    { { Menu::Shop::Id,                         Menu::Shop::Option::RepairItems                                     }, &Manager::HandleShopRepairItemsOption                         },
    { { Menu::Shop::Id,                         Menu::Shop::Option::ChangeFaction                                   }, &Manager::HandleShopCharacterChangeFactionOption              },
};

void Manager::Open(Player* player)
{
    ClearGossipMenuFor(player);
    SendDefaultGossipMenu(player, nullptr, Menu::Id);
}

bool Manager::HandleGossipSelect(Player* player, Creature* creature, uint32 menuId, uint32 gossipListId, Optional<char const*> code /*= std::nullopt*/)
{
    uint32 const action = player->PlayerTalkClass->GetGossipOptionAction(gossipListId);
    bool isAction = action > GOSSIP_ACTION_INFO_DEF;
    MenuOptionPair menuOption = { menuId, isAction ? action : gossipListId };

    OptionHandlerStore::const_iterator itr = handlers.find(menuOption);
    if (itr == handlers.end())
        itr = handlers.find(std::make_pair(menuId, isAction ? Any::Action : Any::Option));

    if (itr == handlers.end())
        return false;

    if (code && std::holds_alternative<MenuOptionWithCodeHandler>(itr->second))
        return (this->*std::get<MenuOptionWithCodeHandler>(itr->second))(player, creature, isAction ? action : gossipListId, *code);
    else if (!code && std::holds_alternative<MenuOptionHandler>(itr->second))
        return (this->*std::get<MenuOptionHandler>(itr->second))(player, creature, isAction ? action : gossipListId);
    else
        return false;
}

/*******************
 *     HANDLERS    *
 *******************/

bool Manager::HandleTeleportToClassAreaOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/)
{
    CloseGossipMenuFor(player);

    Map const* map = player->GetMap();

    if (map->IsBattleground())
    {
        Talk(player, creature, Result::Common::Error::NotInBattleground);
        return true;
    }
    else if (map->IsBattleArena())
    {
        Talk(player, creature, Result::Common::Error::NotInArena);
        return true;
    }

    if (!player->IsAlive())
    {
        Talk(player, creature, Result::Common::Error::YouAreDead);
        return true;
    }

    if (player->IsInCombat())
    {
        Talk(player, creature, Result::Common::Error::NotInCombat);
        return true;
    }

    if (WorldLocation const* loc = sObjectMgr->GetClassArea(player->GetClass()))
        player->TeleportTo(*loc);

    return true;
}

bool Manager::HandlePvPStatsInfoShowMyPvPStatsOption(Player* player, Creature* creature, uint32 const actionOrGossipListId)
{
    return HandlePvPStatsInfoShowPvPStatsOfAnotherPlayerOption(player, creature, actionOrGossipListId, player->GetName());
}

bool Manager::HandlePvPStatsInfoShowPvPStatsOfMyTargetOption(Player* player, Creature* creature, uint32 const actionOrGossipListId)
{
    Player* target = player->GetSelectedPlayer();
    if (!target)
    {
        CloseGossipMenuFor(player);
        Talk(player, creature, Result::PvPStatsInfo::ShowPvPStatsOfMyTarget::Error::CharacterNotFound);
        return true;
    }

    return HandlePvPStatsInfoShowPvPStatsOfAnotherPlayerOption(player, creature, actionOrGossipListId, target->GetName());
}

bool Manager::HandlePvPStatsInfoShowPvPStatsOfAnotherPlayerOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/, std::string const& name)
{
    ObjectGuid guid = sCharacterCache->GetCharacterGuidByName(name);
    if (guid.IsEmpty())
    {
        CloseGossipMenuFor(player);
        Talk(player, creature, Result::PvPStatsInfo::ShowPvPStatsOfAnotherPlayer::Error::CharacterNotFound);
        return true;
    }

    ClearGossipMenuFor(player);

    auto [rank, games, wins, winrate] = GetPvPStats(guid);
    std::string rankTitle = sRankMgr->GetTitleFor(rank, true, true, false);
    std::string text = Trinity::StringFormat(player->GetSession()->GetTrinityString(LANG_SERVER_MANAGER_PVP_STATS_DYNAMIC_TEXT), name, rankTitle, games, wins, winrate);

    SendUpdateDynamicNpcText(player, Menu::PvPStatsInfo::PvPStats::NpcTextId, text);

    if (player->GetGUID() == guid)
    {
        AddGossipItemFor(player, Menu::PvPStatsInfo::PvPStats::Id, Menu::PvPStatsInfo::PvPStats::Option::ShareToSayChannel::Id, GOSSIP_SENDER_MAIN,
            Menu::PvPStatsInfo::PvPStats::Option::ShareToSayChannel::Action);
        AddGossipItemFor(player, Menu::PvPStatsInfo::PvPStats::Id, Menu::PvPStatsInfo::PvPStats::Option::ShareToGroupChannel::Id, GOSSIP_SENDER_MAIN,
            Menu::PvPStatsInfo::PvPStats::Option::ShareToGroupChannel::Action);
        AddGossipItemFor(player, Menu::PvPStatsInfo::PvPStats::Id, Menu::PvPStatsInfo::PvPStats::Option::ShareToGuildChannel::Id, GOSSIP_SENDER_MAIN,
            Menu::PvPStatsInfo::PvPStats::Option::ShareToGuildChannel::Action);
    }

    AddGossipItemFor(player, Menu::PvPStatsInfo::PvPStats::Id, Menu::PvPStatsInfo::PvPStats::Option::PrevPage::Id, GOSSIP_SENDER_MAIN,
        Menu::PvPStatsInfo::PvPStats::Option::PrevPage::Action);

    player->PlayerTalkClass->GetGossipMenu().SetMenuId(Menu::PvPStatsInfo::PvPStats::Id);
    SendGossipMenuFor(player, creature, Menu::PvPStatsInfo::PvPStats::Id);

    return true;
}

bool Manager::HandlePvPStatsInfoPvPStatsShareToSayChannelOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/)
{
    CloseGossipMenuFor(player);

    if (!ProcessPvPStatsShareCooldown(player, creature))
        return true;

    WorldPacket data;
    BuildPvPStatsSharePacket(player, data, LANG_SERVER_MANAGER_PVP_STATS_SHARE_TO_SAY);
    player->SendMessageToSetInRange(&data, sWorld->getFloatConfig(CONFIG_LISTEN_RANGE_SAY), true, !player->GetSession()->HasPermission(rbac::RBAC_PERM_TWO_SIDE_INTERACTION_CHAT));

    return true;
}

bool Manager::HandlePvPStatsInfoPvPStatsShareToGroupChannelOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/)
{
    CloseGossipMenuFor(player);

    Group* group = player->GetGroup();
    if (!group)
    {
        Talk(player, creature, Result::PvPStatsInfo::Share::Error::NotInAGroup);
        return true;
    }

    if (!ProcessPvPStatsShareCooldown(player, creature))
        return true;

    WorldPacket data;
    BuildPvPStatsSharePacket(player, data, LANG_SERVER_MANAGER_PVP_STATS_SHARE_TO_GROUP);
    group->BroadcastPacket(&data, false);

    return true;
}

bool Manager::HandlePvPStatsInfoPvPStatsShareToGuildChannelOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/)
{
    CloseGossipMenuFor(player);

    Guild* guild = player->GetGuild();
    if (!guild)
    {
        Talk(player, creature, Result::PvPStatsInfo::Share::Error::NotAMemberOfAGuild);
        return true;
    }

    if (!ProcessPvPStatsShareCooldown(player, creature))
        return true;

    WorldPacket data;
    BuildPvPStatsSharePacket(player, data, LANG_SERVER_MANAGER_PVP_STATS_SHARE_TO_GUILD);
    guild->BroadcastPacket(&data);

    return true;
}

bool Manager::HandlePvPStatsInfoPvPStatsPrevPageOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/)
{
    return SendDefaultGossipMenu(player, creature, Menu::PvPStatsInfo::Id);
}

bool Manager::HandlePayableSkinsControlPanelOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/)
{
    if (!player->HasPayableSkins())
        return SendDefaultGossipMenu(player, creature, Menu::PayableSkins::ControlPanel::Empty::Id);

    ClearGossipMenuFor(player);
    FillPayableSkinsMenu(player);
    player->PlayerTalkClass->GetGossipMenu().SetMenuId(Menu::PayableSkins::ControlPanel::Id);
    SendGossipMenuFor(player, creature, Menu::PayableSkins::ControlPanel::Id);

    return true;
}

bool Manager::HandlePayableSkinsControlPanelResetOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/)
{
    CloseGossipMenuFor(player);

    if (player->GetActivePayableSkin() == 0)
    {
        Talk(player, creature, Result::PayableSkin::Reset::Error::DontHaveActiveSkin);
        return true;
    }

    player->DeactivatePayableSkin();
    Talk(player, creature, Result::PayableSkin::Reset::Successfully);

    return true;
}

bool Manager::HandlePayableSkinsControlPanelPrevPageOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/)
{
    return SendDefaultGossipMenu(player, creature, Menu::PayableSkins::Id);
}

bool Manager::HandlePayableSkinsControlPanelActivateOption(Player* player, Creature* creature, uint32 const actionOrGossipListId)
{
    CloseGossipMenuFor(player);

    if (actionOrGossipListId <= Menu::PayableSkins::ControlPanel::PayableSkinOffset)
        return true;

    uint32 payableSkinId = actionOrGossipListId - Menu::PayableSkins::ControlPanel::PayableSkinOffset;

    if (!player->GetPayableSkin(payableSkinId))
        return true;

    if (player->GetActivePayableSkin() == payableSkinId)
    {
        Talk(player, creature, Result::PayableSkin::Activate::Error::AlreadyActivated);
        return true;
    }

    player->ActivatePayableSkin(payableSkinId);
    Talk(player, creature, Result::PayableSkin::Activate::Successfully);

    return true;
}

bool Manager::HandleShopCharacterRenameOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/)
{
    CloseGossipMenuFor(player);

    static AtLoginFlags const flag = AT_LOGIN_RENAME;

    if (player->HasAtLoginFlag(flag))
    {
        Talk(player, creature, Result::Shop::CharacterRename::Error::AlreadyHave);
        return true;
    }

    if (!CheckAndTakeDonateCurrency(player, creature, NewWoW::Donate::Cost::CharacterRename))
        return true;

    player->SetAtLoginFlag(flag);
    Talk(player, creature, Result::Shop::CharacterRename::Successfully);

    return true;
}

bool Manager::HandleShopCharacterChangeRaceOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/)
{
    CloseGossipMenuFor(player);

    static AtLoginFlags const flag = AT_LOGIN_CHANGE_RACE;

    if (player->HasAtLoginFlag(flag))
    {
        Talk(player, creature, Result::Shop::CharacterChangeRace::Error::AlreadyHave);
        return true;
    }

    if (!CheckAndTakeDonateCurrency(player, creature, NewWoW::Donate::Cost::CharacterChangeRace))
        return true;

    player->SetAtLoginFlag(flag);
    Talk(player, creature, Result::Shop::CharacterChangeRace::Successfully);

    return true;
}

bool Manager::HandleShopCharacterLevelBoostOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/)
{
    CloseGossipMenuFor(player);

    static uint8 const level = DEFAULT_MAX_LEVEL;

    if (player->GetLevel() >= level)
    {
        Talk(player, creature, Result::Shop::CharacterLevelBoost::Error::AlreadyMaxLevel);
        return true;
    }

    if (!CheckAndTakeDonateCurrency(player, creature, NewWoW::Donate::Cost::CharacterLevelBoost))
        return true;

    player->GiveLevel(level);
    player->InitTalentForLevel();
    player->SetXP(0);

    Talk(player, creature, Result::Shop::CharacterLevelBoost::Successfully);

    return true;
}

bool Manager::HandleShopUnbindInstancesOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/)
{
    CloseGossipMenuFor(player);

    if (!CheckAndTakeDonateCurrency(player, creature, NewWoW::Donate::Cost::UnbindInstances))
        return true;

    for (uint8 i = 0; i < MAX_DIFFICULTY; ++i)
    {
        Player::BoundInstancesMap& binds = player->GetBoundInstances(Difficulty(i));
        for (Player::BoundInstancesMap::iterator itr = binds.begin(); itr != binds.end();)
        {
            InstanceSave* save = itr->second.save;
            if (itr->first != player->GetMapId())
                player->UnbindInstance(itr, Difficulty(i));
            else
                ++itr;
        }
    }

    Talk(player, creature, Result::Shop::UnbindInstances::Successfully);

    return true;
}

bool Manager::HandleShopArenaPointsOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/)
{
    CloseGossipMenuFor(player);

    static uint32 const ARENA_POINTS_COUNT = 200;
    uint32 maxArenaPoints = sWorld->getIntConfig(CONFIG_MAX_ARENA_POINTS);

    if (ARENA_POINTS_COUNT > maxArenaPoints)
        return true; // Internal error

    if (player->GetArenaPoints() > maxArenaPoints - ARENA_POINTS_COUNT)
    {
        Talk(player, creature, Result::Shop::ArenaPoints::Error::TooMany);
        return true;
    }

    if (!CheckAndTakeDonateCurrency(player, creature, NewWoW::Donate::Cost::ArenaPoints))
        return true;

    player->ModifyArenaPoints(ARENA_POINTS_COUNT);
    Talk(player, creature, Result::Shop::ArenaPoints::Successfully);

    return true;
}

bool Manager::HandleShopHonorPointsOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/)
{
    CloseGossipMenuFor(player);

    static uint32 const HONOR_POINTS_COUNT = 10000;
    uint32 maxHonorPoints = sWorld->getIntConfig(CONFIG_MAX_HONOR_POINTS);

    if (HONOR_POINTS_COUNT > maxHonorPoints)
        return true; // Internal error

    if (player->GetHonorPoints() > maxHonorPoints - HONOR_POINTS_COUNT)
    {
        Talk(player, creature, Result::Shop::HonorPoints::Error::TooMany);
        return true;
    }

    if (!CheckAndTakeDonateCurrency(player, creature, NewWoW::Donate::Cost::HonorPoints))
        return true;

    player->ModifyHonorPoints(HONOR_POINTS_COUNT);
    Talk(player, creature, Result::Shop::HonorPoints::Successfully);

    return true;
}

bool Manager::HandleShopProfessionsIncreaseOption(Player* player, Creature* creature, uint32 const actionOrGossipListId)
{
    static std::array<uint32, 14> const professions =
    {
        SKILL_ALCHEMY, SKILL_BLACKSMITHING, SKILL_ENCHANTING,
        SKILL_ENGINEERING, SKILL_HERBALISM, SKILL_INSCRIPTION,
        SKILL_JEWELCRAFTING, SKILL_LEATHERWORKING, SKILL_MINING,
        SKILL_SKINNING, SKILL_TAILORING, SKILL_COOKING,
        SKILL_FIRST_AID, SKILL_FISHING
    };

    if (actionOrGossipListId >= professions.size())
        return false;

    CloseGossipMenuFor(player);

    uint32 skillId = professions[actionOrGossipListId];

    if (!player->HasSkill(skillId))
    {
        Talk(player, creature, Result::Shop::IncreaseProfession::Error::DontHaveProfession);
        return true;
    }

    uint16 currentValue = player->GetSkillValue(skillId);
    uint16 maxValue = player->GetMaxSkillValue(skillId);

    if (currentValue >= maxValue)
    {
        Talk(player, creature, Result::Shop::IncreaseProfession::Error::AlreadyMaxSkill);
        return true;
    }

    if (!CheckAndTakeDonateCurrency(player, creature, NewWoW::Donate::Cost::ProfessionIncrease))
        return true;

    player->SetSkill(skillId, player->GetSkillStep(skillId), maxValue, maxValue);
    Talk(player, creature, Result::Shop::IncreaseProfession::Successfully);

    return true;
}

bool Manager::HandleShopTitlesBuyOption(Player* player, Creature* creature, uint32 const actionOrGossipListId)
{
    static std::array<uint32, 18> const titles =
    {
        42, 62, 71, 77, 80, 82, 83, 124, 128,
        133, 135, 138, 155, 157, 167, 168, 169, 177
    };

    if (actionOrGossipListId >= titles.size())
        return false;

    CloseGossipMenuFor(player);

    CharTitlesEntry const* titleEntry = sCharTitlesStore.LookupEntry(titles[actionOrGossipListId]);
    if (!titleEntry)
        return true;

    if (player->HasTitle(titleEntry))
    {
        Talk(player, creature, Result::Shop::Title::Error::AlreadyHave);
        return true;
    }

    if (!CheckAndTakeDonateCurrency(player, creature, NewWoW::Donate::Cost::Title))
        return true;

    player->SetTitle(titleEntry);
    Talk(player, creature, Result::Shop::Title::Successfully);
    return true;
}

bool Manager::HandleShopRepairItemsOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/)
{
    CloseGossipMenuFor(player);

    if (!CheckAndTakeDonateCurrency(player, creature, NewWoW::Donate::Cost::RepairItems))
        return true;

    player->DurabilityRepairAll(false, 0, false);
    Talk(player, creature, Result::Shop::RepairItems::Successfully);

    return true;
}

bool Manager::HandleShopCharacterChangeFactionOption(Player* player, Creature* creature, uint32 const /*actionOrGossipListId*/)
{
    CloseGossipMenuFor(player);

    static AtLoginFlags const flag = AT_LOGIN_CHANGE_FACTION;

    if (player->HasAtLoginFlag(flag))
    {
        Talk(player, creature, Result::Shop::CharacterChangeFaction::Error::AlreadyHave);
        return true;
    }

    if (!CheckAndTakeDonateCurrency(player, creature, NewWoW::Donate::Cost::CharacterChangeFaction))
        return true;

    player->SetAtLoginFlag(flag);
    Talk(player, creature, Result::Shop::CharacterChangeFaction::Successfully);

    return true;
}

/*******************
 *     HELPERS     *
 *******************/

template<typename... Args>
void Manager::Talk(Player* player, Creature* creature, uint32 textId, Args&&... args)
{
    std::string text = ChatHandler(player->GetSession()).PGetParseString(textId, std::forward<Args>(args)...);

    if (creature)
        creature->Whisper(text, LANG_UNIVERSAL, player);
    else
        ChatHandler(player->GetSession()).SendSysMessage(text);
}

void Manager::FillPayableSkinsMenu(Player* player)
{
    for (auto const& [skinId, playerPayableSkin] : player->GetPayableSkins())
    {
        PayableSkin const* payableSkin = sPayableSkinMgr->GetPayableSkinById(skinId);
        if (!payableSkin)
            continue;

        bool isActive = player->GetActivePayableSkin() == skinId;
        std::string name = payableSkin->GetName();

        if (playerPayableSkin.IsTemporary())
            name += std::string(" ") + player->GetSession()->GetTrinityString(LANG_PAYABLE_SKIN_TEMPORARY);

        if (isActive)
            name += std::string(" ") + player->GetSession()->GetTrinityString(LANG_PAYABLE_SKIN_ACTIVE);

        AddGossipItemFor(player, GOSSIP_ICON_DOT, name, GOSSIP_SENDER_MAIN,
            Menu::PayableSkins::ControlPanel::PayableSkinOffset + skinId,
            isActive ? "" : Trinity::StringFormat(player->GetSession()->GetTrinityString(LANG_PAYABLE_SKIN_ACTIVATE_POPUP), payableSkin->GetName()),
            0, false);
    }

    AddGossipItemFor(player, Menu::PayableSkins::ControlPanel::Id, Menu::PayableSkins::ControlPanel::Option::Reset::Id,
        GOSSIP_SENDER_MAIN, Menu::PayableSkins::ControlPanel::Option::Reset::Action);
    AddGossipItemFor(player, Menu::PayableSkins::ControlPanel::Id, Menu::PayableSkins::ControlPanel::Option::PrevPage::Id,
        GOSSIP_SENDER_MAIN, Menu::PayableSkins::ControlPanel::Option::PrevPage::Action);
}

bool Manager::SendDefaultGossipMenu(Player* player, Creature* creature, uint32 menuId)
{
    if (creature)
    {
        player->PrepareGossipMenu(creature, menuId);
        player->SendPreparedGossip(creature);
    }
    else
    {
        player->PrepareGossipMenu(player, menuId);
        player->SendPreparedGossip(player);
    }

    return true;
}

void Manager::SendGossipMenuFor(Player* player, Creature* creature, uint32 menuId)
{
    using ::SendGossipMenuFor;

    if (creature)
        SendGossipMenuFor(player, player->GetGossipTextId(menuId, creature), creature);
    else
        SendGossipMenuFor(player, player->GetGossipTextId(menuId, player), player->GetGUID());
}

bool Manager::CheckAndTakeDonateCurrency(Player* player, Creature* creature, uint32 amount)
{
    bool hasEnoughDonateCurrency = player->HasItemCount(NewWoW::Donate::Currency, amount);
    if (hasEnoughDonateCurrency)
        player->DestroyItemCount(NewWoW::Donate::Currency, amount, true);
    else
        Talk(player, creature, Result::Shop::Error::NotEnoughCurrency);

    return hasEnoughDonateCurrency;
}

void Manager::SendUpdateDynamicNpcText(Player* player, uint32 npcTextId, std::string_view text)
{
    WorldPacket data(SMSG_NPC_TEXT_UPDATE, 4 + 4 + (text.size() + 1) * 2 + 7 * 4);

    data << npcTextId;
    data << float(0);

    for (uint8 i = 0; i < 2; ++i)
        data << text;

    for (uint8 i = 0; i < 7; ++i)
        data << uint32(0);

    player->SendDirectMessage(&data);
}

std::tuple<uint8, uint32, uint32, float> Manager::GetPvPStats(ObjectGuid const& guid)
{
    uint8   rank        = 0;
    uint32  games       = 0;
    uint32  wins        = 0;

    Player* target = ObjectAccessor::FindConnectedPlayer(guid);
    if (target)
    {
        rank                    = target->GetPvPRank();
        std::tie(games, wins)   = target->GetPvPStats([](PvPStatsBgTypeArenaTypePair const& bgArenaInfo) -> bool
        {
            return bgArenaInfo.first == BATTLEGROUND_WS || bgArenaInfo.first == BATTLEGROUND_AB || bgArenaInfo.second == ARENA_TYPE_5v5;
        });
    }
    else
    {
        rank                    = sPvPStatsCacheMgr->GetRank(guid);
        std::tie(games, wins)   = sPvPStatsCacheMgr->GetPvPStats(guid, [](PvPStatsBgTypeArenaTypePair const& bgArenaInfo) -> bool
        {
            return bgArenaInfo.first == BATTLEGROUND_WS || bgArenaInfo.first == BATTLEGROUND_AB || bgArenaInfo.second == ARENA_TYPE_5v5;
        });
    }

    float winrate = games > 0 ? static_cast<float>(wins) * 100.f / static_cast<float>(games) : 0;

    return { rank, games, wins, winrate };
}

void Manager::BuildPvPStatsSharePacket(Player* player, WorldPacket& packet, uint32 stringId)
{
    auto [rank, games, wins, winrate] = GetPvPStats(player->GetGUID());
    std::string nameLink = std::string("|Hplayer:") + player->GetName() + "|h[" + player->GetName() + "]|h";
    std::string rankTitle = sRankMgr->GetTitleFor(rank, true, true, false);
    std::string text = Trinity::StringFormat(player->GetSession()->GetTrinityString(stringId), nameLink, rankTitle, games, wins, winrate);
    ChatHandler::BuildChatPacket(packet, CHAT_MSG_SYSTEM, LANG_UNIVERSAL, nullptr, nullptr, text);
}

bool Manager::ProcessPvPStatsShareCooldown(Player* player, Creature* creature)
{
    time_t now  = GameTime::GetGameTime();
    time_t& last = m_lastPvPStatsShareTimeStore[player->GetGUID()];
    time_t passed = now - last;

    if (passed < SharePvPStatsCooldown)
    {
        Talk(player, creature, Result::PvPStatsInfo::Share::Error::NeedToWait, static_cast<uint32>(SharePvPStatsCooldown - passed));
        return false;
    }

    last = now;
    return true;
}
