#include "PvPStatsCacheMgr.h"
#include "DatabaseEnv.h"
#include "Log.h"

using namespace PvPStatsCache;

Manager::Manager() { }

Manager::~Manager() { }

Manager* Manager::instance()
{
    static Manager instance;
    return &instance;
}

void Manager::LoadFromDB()
{
    m_cacheStore.clear();
    uint32 oldMSTime = getMSTime();

    // Load PvP-ranks
    {
        //                                                      0   1
        QueryResult result = CharacterDatabase.Query("SELECT guid, `rank` FROM character_pvp_ranks");
        if (result)
        {
            do 
            {
                Field* fields               = result->Fetch();
                ObjectGuid guid             = ObjectGuid::Create<HighGuid::Player>(fields[0].GetUInt32());
                uint32 rank                 = fields[1].GetUInt32();

                Cache& cache = m_cacheStore[guid];
                cache.rank   = rank;
            } while (result->NextRow());
        }
    }

    // Load PvP-stats
    {
        //                                                      0   1       2           3       4
        QueryResult result = CharacterDatabase.Query("SELECT guid, bgType, arenaType, games, wins FROM character_pvp_stats");
        if (result)
        {
            do
            {
                Field* fields   = result->Fetch();
                ObjectGuid guid = ObjectGuid::Create<HighGuid::Player>(fields[0].GetUInt32());
                uint8 bgType    = fields[1].GetUInt8();
                uint8 arenaType = fields[2].GetUInt8();
                uint32 games    = fields[3].GetUInt32();
                uint32 wins     = fields[4].GetUInt32();

                Cache& cache = m_cacheStore[guid];
                cache.stats.emplace(std::make_pair(bgType, arenaType), std::make_pair(games, wins));
            } while (result->NextRow());
        }
    }

    TC_LOG_INFO("server.loading", "Loaded PvP data cache for " SZFMTD " characters in %u ms", m_cacheStore.size(), GetMSTimeDiffToNow(oldMSTime));
}

void Manager::UpdateCacheFor(Player* player)
{
    Cache& cache    = m_cacheStore[player->GetGUID()];
    cache.rank      = player->GetPvPRank();
    cache.stats     = player->MovePvPStatsContainer();
}

uint32 Manager::GetRank(ObjectGuid const& guid) const
{
    CacheContainer::const_iterator itr = m_cacheStore.find(guid);
    if (itr != m_cacheStore.end())
        return itr->second.rank;

    return 0;
}

PvPStatsGamesWinsPair Manager::GetPvPStats(ObjectGuid const& guid, std::function<bool(PvPStatsBgTypeArenaTypePair const&)> const& predicate) const
{
    CacheContainer::const_iterator itr = m_cacheStore.find(guid);
    if (itr == m_cacheStore.end())
        return { 0, 0 };

    Cache const& cache = itr->second;
    uint32 games = 0;
    uint32 wins = 0;

    for (auto const& [bgArenaInfo, stats] : cache.stats)
    {
        if (!predicate(bgArenaInfo))
            continue;

        games += stats.first;
        wins += stats.second;
    }

    return { games, wins };
}
