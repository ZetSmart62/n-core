#include "PvPStatsCacheMgr.h"
#include "ScriptMgr.h"
#include "Log.h"

class world_pvp_stats_cache_updater : public WorldScript
{
public:
    world_pvp_stats_cache_updater() : WorldScript("world_pvp_stats_cache_updater") { }

    void OnStartup() override
    {
        TC_LOG_INFO("server.loading", "Loading character PvP data cache store...");
        sPvPStatsCacheMgr->LoadFromDB();
    }
};

class player_pvp_stats_cache_saver : public PlayerScript
{
public:
    player_pvp_stats_cache_saver() : PlayerScript("player_pvp_stats_cache_saver") { }

    void OnLogout(Player* player)
    {
        sPvPStatsCacheMgr->UpdateCacheFor(player);
    }
};

void AddSC_pvp_stats_cache()
{
    new world_pvp_stats_cache_updater();
    new player_pvp_stats_cache_saver();
}
