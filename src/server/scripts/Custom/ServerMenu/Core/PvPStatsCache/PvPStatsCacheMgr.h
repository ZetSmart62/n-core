#ifndef _PVP_STATS_CACHE_MGR_H
#define _PVP_STATS_CACHE_MGR_H

#include "AsyncCallbackProcessor.h"
#include "DatabaseEnvFwd.h"
#include "Player.h"

// TODO: Use runtime async queries instead of loading all data on start-up

namespace PvPStatsCache
{
    struct Cache
    {
        Cache() : rank(0) { }
        uint32 rank;
        PvPStatsContainer stats;
    };

    using CacheContainer = std::map<ObjectGuid, Cache>;

    class Manager
    {
    private:
        Manager();
        ~Manager();
        Manager(Manager const&) = delete;
        Manager& operator=(Manager const&) = delete;

    public:
        static Manager* instance();

        void LoadFromDB();

        void UpdateCacheFor(Player* player);

        uint32 GetRank(ObjectGuid const& guid) const;
        PvPStatsGamesWinsPair GetPvPStats(ObjectGuid const& guid, std::function<bool(PvPStatsBgTypeArenaTypePair const&)> const& predicate) const;

    private:
        CacheContainer m_cacheStore;
    };
}

#define sPvPStatsCacheMgr PvPStatsCache::Manager::instance()

#endif // _PVP_STATS_CACHE_MGR_H
