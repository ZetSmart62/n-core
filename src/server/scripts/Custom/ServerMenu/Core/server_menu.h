#ifndef _SERVER_MENU_H
#define _SERVER_MENU_H

#include <variant>
#include <tuple>
#include "Define.h"
#include "ScriptedGossip.h"

namespace ServerMenu
{
    class Manager;

    using MenuOptionPair            = std::pair<uint32, int32>;
    using MenuOptionHandler         = bool(Manager::*)(Player*, Creature*, uint32 const);
    using MenuOptionWithCodeHandler = bool(Manager::*)(Player*, Creature*, uint32 const, std::string const&);
    using MenuOptionHandlerVariant  = std::variant<MenuOptionHandler, MenuOptionWithCodeHandler>;
    using OptionHandlerStore        = std::unordered_map<MenuOptionPair, MenuOptionHandlerVariant>;

    class Manager
    {
    private:
        Manager();
        ~Manager();
        Manager(Manager const&) = delete;
        Manager& operator=(Manager const&) = delete;

    public:
        static Manager* instance();

        void Open(Player* player); // for player gossip menu case
        bool HandleGossipSelect(Player* player, Creature* creature, uint32 menuId, uint32 gossipListId, Optional<char const*> code = std::nullopt);

        // Handlers
        bool HandleTeleportToClassAreaOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandlePvPStatsInfoShowMyPvPStatsOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandlePvPStatsInfoShowPvPStatsOfMyTargetOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandlePvPStatsInfoShowPvPStatsOfAnotherPlayerOption(Player* player, Creature*, uint32 const actionOrGossipListId, std::string const& name);
        bool HandlePvPStatsInfoPvPStatsShareToSayChannelOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandlePvPStatsInfoPvPStatsShareToGroupChannelOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandlePvPStatsInfoPvPStatsShareToGuildChannelOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandlePvPStatsInfoPvPStatsPrevPageOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandlePayableSkinsControlPanelOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandlePayableSkinsControlPanelResetOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandlePayableSkinsControlPanelPrevPageOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandlePayableSkinsControlPanelActivateOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandleShopCharacterRenameOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandleShopCharacterChangeRaceOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandleShopCharacterLevelBoostOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandleShopUnbindInstancesOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandleShopArenaPointsOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandleShopHonorPointsOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandleShopProfessionsIncreaseOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandleShopTitlesBuyOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandleShopRepairItemsOption(Player* player, Creature*, uint32 const actionOrGossipListId);
        bool HandleShopCharacterChangeFactionOption(Player* player, Creature*, uint32 const actionOrGossipListId);

        // Helpers
        template<typename... Args>
        void Talk(Player* player, Creature* creature, uint32 textId, Args&&... args);
        void FillPayableSkinsMenu(Player* player);
        bool SendDefaultGossipMenu(Player* player, Creature* creature, uint32 menuId);
        void SendGossipMenuFor(Player* player, Creature* creature, uint32 menuId);
        bool CheckAndTakeDonateCurrency(Player* player, Creature* creature, uint32 amount);
        void SendUpdateDynamicNpcText(Player* player, uint32 npcTextId, std::string_view text);
        std::tuple<uint8, uint32, uint32, float> GetPvPStats(ObjectGuid const& guid);
        void BuildPvPStatsSharePacket(Player* player, WorldPacket& packet, uint32 stringId);
        bool ProcessPvPStatsShareCooldown(Player* player, Creature* creature);

    private:
        std::unordered_map<ObjectGuid, time_t> m_lastPvPStatsShareTimeStore;
    };

    enum Any : int32
    {
        Action = -1,
        Option = -2
    };

    static inline constexpr uint32 SharePvPStatsCooldown                        = 15;

    struct Menu
    {
        static inline constexpr uint32 Id                                       = 59000;
        struct Option
        {
            static inline constexpr uint32 TeleportToClassArea                  = 1;
        };

        struct PvPStatsInfo
        {
            static inline constexpr uint32 Id = 59016;
            struct Option
            {
                static inline constexpr uint32 ShowMyPvPStats                   = 0;
                static inline constexpr uint32 ShowPvPStatsOfMyTarget           = 1;
                static inline constexpr uint32 ShowPvPStatsOfAnotherPlayer      = 2;
            };

            struct PvPStats
            {
                static inline constexpr uint32 Id                               = 59017;
                static inline constexpr uint32 NpcTextId                        = 76014;
                struct Option
                {
                    struct ShareToSayChannel
                    {
                        static inline constexpr uint32 Id                       = 0;
                        static inline constexpr uint32 Action                   = GOSSIP_ACTION_INFO_DEF + 1;
                    };

                    struct ShareToGroupChannel
                    {
                        static inline constexpr uint32 Id                       = 1;
                        static inline constexpr uint32 Action                   = GOSSIP_ACTION_INFO_DEF + 2;
                    };

                    struct ShareToGuildChannel
                    {
                        static inline constexpr uint32 Id                       = 2;
                        static inline constexpr uint32 Action                   = GOSSIP_ACTION_INFO_DEF + 3;
                    };

                    struct PrevPage
                    {
                        static inline constexpr uint32 Id                       = 3;
                        static inline constexpr uint32 Action                   = GOSSIP_ACTION_INFO_DEF + 4;
                    };
                };
            };
        };

        struct PayableSkins
        {
            static inline constexpr uint32 Id                                   = 59010;
            struct Option
            {
                static inline constexpr uint32 ControlPanel                     = 0;
            };

            struct ControlPanel
            {
                static inline constexpr uint32 Id                               = 59012;
                static inline constexpr uint32 PayableSkinOffset                = GOSSIP_ACTION_INFO_DEF + 100;
                struct Option
                {
                    struct Reset
                    {
                        static inline constexpr uint32 Id                       = 0;
                        static inline constexpr uint32 Action                   = GOSSIP_ACTION_INFO_DEF + 1;
                    };

                    struct PrevPage
                    {
                        static inline constexpr uint32 Id                       = 1;
                        static inline constexpr uint32 Action                   = GOSSIP_ACTION_INFO_DEF + 2;
                    };
                };

                struct Empty
                {
                    static inline constexpr uint32 Id                           = 59011;
                };
            };
        };

        struct Shop
        {
            static inline constexpr uint32 Id                                   = 59013;
            struct Option
            {
                static inline constexpr uint32 CharacterRename                  = 1;
                static inline constexpr uint32 CharacterChangeRace              = 2;
                static inline constexpr uint32 CharacterLevelBoost              = 3;
                static inline constexpr uint32 UnbindInstances                  = 5;
                static inline constexpr uint32 ArenaPoints                      = 6;
                static inline constexpr uint32 HonorPoints                      = 7;
                static inline constexpr uint32 RepairItems                      = 8;
                static inline constexpr uint32 ChangeFaction                    = 9;
            };

            struct Professions
            {
                static inline constexpr uint32 Id                               = 59014;
            };

            struct Titles
            {
                static inline constexpr uint32 Id                               = 59015;
            };
        };
    };

    struct Result
    {
        struct PayableSkin
        {
            struct Activate
            {
                static inline constexpr uint32 Successfully                     = 30042;
                struct Error
                {
                    static inline constexpr uint32 AlreadyActivated             = 30043;
                };
            };

            struct Reset
            {
                static inline constexpr uint32 Successfully                     = 30044;
                struct Error
                {
                    static inline constexpr uint32 DontHaveActiveSkin           = 30045;
                };
            };
        };

        struct Shop
        {
            struct Error
            {
                static inline constexpr uint32 NotEnoughCurrency                = 30046;
            };

            struct CharacterRename
            {
                static inline constexpr uint32 Successfully                     = 30047;
                struct Error
                {
                    static inline constexpr uint32 AlreadyHave                  = 30048;
                };
            };

            struct CharacterChangeRace
            {
                static inline constexpr uint32 Successfully                     = 30049;
                struct Error
                {
                    static inline constexpr uint32 AlreadyHave                  = 30050;
                };
            };

            struct CharacterLevelBoost
            {
                static inline constexpr uint32 Successfully                     = 30051;
                struct Error
                {
                    static inline constexpr uint32 AlreadyMaxLevel              = 30052;
                };
            };

            struct UnbindInstances
            {
                static inline constexpr uint32 Successfully                     = 30053;
            };

            struct ArenaPoints
            {
                static inline constexpr uint32 Successfully                     = 30054;
                struct Error
                {
                    static inline constexpr uint32 TooMany                      = 30055;
                };
            };

            struct HonorPoints
            {
                static inline constexpr uint32 Successfully                     = 30056;
                struct Error
                {
                    static inline constexpr uint32 TooMany                      = 30057;
                };
            };

            struct IncreaseProfession
            {
                static inline constexpr uint32 Successfully                     = 30058;
                struct Error
                {
                    static inline constexpr uint32 DontHaveProfession           = 30059;
                    static inline constexpr uint32 AlreadyMaxSkill              = 30060;
                };
            };

            struct Title
            {
                static inline constexpr uint32 Successfully                     = 30061;
                struct Error
                {
                    static inline constexpr uint32 AlreadyHave                  = 30062;
                };
            };

            struct RepairItems
            {
                static inline constexpr uint32 Successfully                     = 30068;
            };

            struct CharacterChangeFaction
            {
                static inline constexpr uint32 Successfully                     = 30069;
                struct Error
                {
                    static inline constexpr uint32 AlreadyHave                  = 30070;
                };
            };
        };

        struct PvPStatsInfo
        {
            struct ShowPvPStatsOfMyTarget
            {
                struct Error
                {
                    static inline constexpr uint32 CharacterNotFound            = 30063;
                };
            };

            struct ShowPvPStatsOfAnotherPlayer
            {
                struct Error
                {
                    static inline constexpr uint32 CharacterNotFound            = 30064;
                };
            };

            struct Share
            {
                struct Error
                {
                    static inline constexpr uint32 NeedToWait                   = 30065;
                    static inline constexpr uint32 NotInAGroup                  = 30066;
                    static inline constexpr uint32 NotAMemberOfAGuild           = 30067;
                };
            };
        };

        struct Common
        {
            struct Error
            {
                static inline constexpr uint32 NotInCombat                      = 30071;
                static inline constexpr uint32 NotInBattleground                = 30072;
                static inline constexpr uint32 NotInArena                       = 30073;
                static inline constexpr uint32 YouAreDead                       = 30074;
            };
        };
    };
}

#define sServerMenu ServerMenu::Manager::instance()

#endif // _SERVER_MENU_H
