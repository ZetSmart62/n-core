#include "Core/server_menu.h"
#include "ScriptedGossip.h"

struct npc_server_menu : public ScriptedAI
{
    npc_server_menu(Creature* creature) : ScriptedAI(creature) { }

    bool OnGossipSelect(Player* player, uint32 menuId, uint32 gossipListId) override
    {
        return sServerMenu->HandleGossipSelect(player, me, menuId, gossipListId);
    }

    bool OnGossipSelectCode(Player* player, uint32 menuId, uint32 gossipListId, char const* code) override
    {
        return sServerMenu->HandleGossipSelect(player, me, menuId, gossipListId, code);
    }
};

void AddSC_npc_server_menu()
{
    RegisterCreatureAI(npc_server_menu);
}
