#include "Core/server_menu.h"
#include "ScriptedGossip.h"

class player_server_menu : public PlayerScript
{
public:
    player_server_menu() : PlayerScript("player_server_menu") { }

    void OnGossipSelect(Player* player, uint32 menuId, uint32 gossipListId, bool& result) override
    {
        result = sServerMenu->HandleGossipSelect(player, nullptr, menuId, gossipListId);
    }

    void OnGossipSelectCode(Player* player, uint32 menuId, uint32 gossipListId, const char* code, bool& result) override
    {
        result = sServerMenu->HandleGossipSelect(player, nullptr, menuId, gossipListId, code);
    }
};

// 90000 - Server Menu
class spell_server_menu : public SpellScript
{
    PrepareSpellScript(spell_server_menu);

    bool Load() override
    {
        return GetCaster()->GetTypeId() == TYPEID_PLAYER;
    }

    void HandleDummy(SpellEffIndex /*effIndex*/)
    {
        sServerMenu->Open(GetCaster()->ToPlayer());
    }

    void Register() override
    {
        OnEffectHit += SpellEffectFn(spell_server_menu::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

void AddSC_player_server_menu()
{
    new player_server_menu();
    RegisterSpellScript(spell_server_menu);
}
