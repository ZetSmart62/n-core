#include "Chat.h"

class player_chat_censure : public PlayerScript
{
public:
    player_chat_censure() : PlayerScript("player_chat_censure") {}

    void OnChat(Player* player, uint32 /*type*/, uint32 lang, std::string& msg) override
    {
        CheckMessage(player, msg, lang);
    }

    void OnChat(Player* player, uint32 /*type*/, uint32 lang, std::string& msg, Player* /*receiver*/) override
    {
        CheckMessage(player, msg, lang);
    }

    void OnChat(Player* player, uint32 /*type*/, uint32 lang, std::string& msg, Group* /*group*/) override
    {
        CheckMessage(player, msg, lang);
    }

    void OnChat(Player* player, uint32 /*type*/, uint32 lang, std::string& msg, Guild* /*guild*/) override
    {
        CheckMessage(player, msg, lang);
    }

    void OnChat(Player* player, uint32 /*type*/, uint32 lang, std::string& msg, Channel* /*channel*/) override
    {
        CheckMessage(player, msg, lang);
    }

    void CheckMessage(Player* player, std::string& msg, uint32 lang)
    {
        static std::array<std::string, 15> const checks =
        {
            "http://", ".com", "www.", ".net", ".org", ".ru", ".ua", "wow-", "-wow", ".pl", ".bg", ".eu", ".biz", ".-ip", ".-zapto.org"
        };

        if (lang == LANG_ADDON || player->IsGameMaster())
            return;

        std::string lower = msg;
        std::transform(lower.begin(), lower.end(), lower.begin(), ::tolower);

        for (std::string const& check : checks)
        {
            if (lower.find(check) == std::string::npos)
                continue;

            msg = "";
            ChatHandler(player->GetSession()).PSendSysMessage("It is forbidden to write websites on the server!");
            return;
        }
    }
};

void AddSC_System_Censure()
{
    new player_chat_censure();
}
