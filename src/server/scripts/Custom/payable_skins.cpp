#include "PayableSkinMgr.h"
#include "ScriptedCreature.h"
#include "ScriptedGossip.h"
#include "ObjectMgr.h"
#include "ItemTemplate.h"
#include "SpellAuraEffects.h"
#include "Player.h"
#include "Language.h"
#include "Chat.h"

enum PayableSkinsVendorOption
{
    BuySkin = 0
};

enum PayableSkinsVendorSay
{
    OnSuccessBuy = 0
};

struct npc_payable_skins_vendor : public ScriptedAI
{
    npc_payable_skins_vendor(Creature* creature) : ScriptedAI(creature), m_payableSkin(nullptr) { }

    void InitializeAI() override
    {
        if (m_payableSkin = sPayableSkinMgr->GetPayableSkinByVendor(me))
        {
            me->SetDisplayId(m_payableSkin->GetDisplayId());
            me->SetNativeDisplayId(m_payableSkin->GetDisplayId());

            for (uint32 const& auraId : m_payableSkin->GetAuras())
                if (!me->HasAura(auraId))
                    DoCastSelf(auraId, true);
        }

        sPayableSkinMgr->RegisterVendor(me->GetGUID());

        ScriptedAI::InitializeAI();
    }

    void OnDespawn() override
    {
        sPayableSkinMgr->UnRegisterVendor(me->GetGUID());
        ScriptedAI::OnDespawn();
    }

    bool OnGossipHello(Player* player) override
    {
        if (!m_payableSkin)
            return false;

        if (me->IsQuestGiver())
            player->PrepareQuestMenu(me->GetGUID());

        PlayerPayableSkin const* playerPayableSkin = player->GetPayableSkin(m_payableSkin->GetId());
        if ((!playerPayableSkin || playerPayableSkin->IsTemporary()) && m_payableSkin->IsAllowedFor(player->GetGUID()))
            AddGossipItemFor(player, Player::GetDefaultGossipMenuForSource(me), PayableSkinsVendorOption::BuySkin, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF + 1);

        SendGossipMenuFor(player, player->GetGossipTextId(me), me->GetGUID());
        return true;
    }

    bool OnGossipSelect(Player* player, uint32 /*menuId*/, uint32 gossipListId) override
    {
        if (!m_payableSkin)
            return false;

        uint32 const action = player->PlayerTalkClass->GetGossipOptionAction(gossipListId);

        if (action != GOSSIP_ACTION_INFO_DEF + 1)
            return false;

        ClearGossipMenuFor(player);
        CloseGossipMenuFor(player);

        if (sPayableSkinMgr->Buy(player, m_payableSkin->GetId()))
            Talk(PayableSkinsVendorSay::OnSuccessBuy, player);

        return true;
    }

private:
    PayableSkin const* m_payableSkin;
};

class spell_add_payable_skin : public SpellScript
{
    PrepareSpellScript(spell_add_payable_skin);

    bool Load() override
    {
        return GetCaster()->GetTypeId() == TYPEID_PLAYER;
    }

    SpellCastResult CheckCast()
    {
        if (sPayableSkinMgr->GetPayableSkinById(GetSpellInfo()->GetEffect(EFFECT_0).BasePoints) == nullptr)
            return SPELL_FAILED_ERROR;
        return SPELL_CAST_OK;
    }

    void HandleDummy(SpellEffIndex effIndex)
    {
        Player* player = GetCaster()->ToPlayer();
        PayableSkin const* payableSkin = sPayableSkinMgr->GetPayableSkinById(GetEffectValue());

        if (!payableSkin->IsAllowedFor(player->GetGUID()))
        {
            ChatHandler(player->GetSession()).SendSysMessage(LANG_SPELL_SKIN_UNAVAILABLE);
            AlternativeReward(player, effIndex);
            return;
        }

        PlayerPayableSkin const* playerPayableSkin = player->GetPayableSkin(payableSkin->GetId());
        if (playerPayableSkin && !playerPayableSkin->IsTemporary())
        {
            ChatHandler(player->GetSession()).SendSysMessage(LANG_SPELL_SKIN_ALREADY_HAVE);
            AlternativeReward(player, effIndex);
            return;
        }

        player->AddPayableSkin(payableSkin->GetId());
        ChatHandler(player->GetSession()).PSendSysMessage(LANG_SPELL_SKIN_GOT, payableSkin->GetName());
    }

    void AlternativeReward(Player* player, SpellEffIndex effIndex)
    {
        ItemTemplate const* reward = sObjectMgr->GetItemTemplate(GetSpellInfo()->GetEffect(effIndex).MiscValue);
        uint32 count = GetSpellInfo()->GetEffect(effIndex).MiscValueB;
        if (reward && count > 0)
            player->AddItem(reward->ItemId, count);
    }

    void Register() override
    {
        OnCheckCast += SpellCheckCastFn(spell_add_payable_skin::CheckCast);
        OnEffectHit += SpellEffectFn(spell_add_payable_skin::HandleDummy, EFFECT_0, SPELL_EFFECT_DUMMY);
    }
};

class spell_add_temporary_payable_skin : public SpellScript
{
    PrepareSpellScript(spell_add_temporary_payable_skin);

    bool Load() override
    {
        return GetCaster()->GetTypeId() == TYPEID_PLAYER;
    }

    SpellCastResult CheckCast()
    {
        if (sPayableSkinMgr->GetPayableSkinById(GetSpellInfo()->GetEffect(EFFECT_0).BasePoints) == nullptr)
            return SPELL_FAILED_ERROR;
        return SPELL_CAST_OK;
    }

    void HandleApplyAura(SpellEffIndex effIndex)
    {
        Player* player = GetCaster()->ToPlayer();
        PayableSkin const* payableSkin = sPayableSkinMgr->GetPayableSkinById(GetEffectValue());

        if (!payableSkin->IsAllowedFor(player->GetGUID()))
        {
            ChatHandler(player->GetSession()).SendSysMessage(LANG_SPELL_SKIN_UNAVAILABLE);
            AlternativeReward(player, effIndex);
            PreventHitAura();
            return;
        }

        PlayerPayableSkin const* playerPayableSkin = player->GetPayableSkin(payableSkin->GetId());
        if (playerPayableSkin && !playerPayableSkin->IsTemporary())
        {
            ChatHandler(player->GetSession()).SendSysMessage(LANG_SPELL_SKIN_ALREADY_HAVE);
            AlternativeReward(player, effIndex);
            PreventHitAura();
            return;
        }
    }

    void AlternativeReward(Player* player, SpellEffIndex effIndex)
    {
        ItemTemplate const* reward = sObjectMgr->GetItemTemplate(GetSpellInfo()->GetEffect(effIndex).MiscValue);
        uint32 count = GetSpellInfo()->GetEffect(effIndex).MiscValueB;
        if (reward && count > 0)
            player->AddItem(reward->ItemId, count);
    }
    
    void Register() override
    {
        OnEffectHitTarget += SpellEffectFn(spell_add_temporary_payable_skin::HandleApplyAura, EFFECT_0, SPELL_EFFECT_APPLY_AURA);
    }
};

class spell_add_temporary_payable_skin_aura : public AuraScript
{
    PrepareAuraScript(spell_add_temporary_payable_skin_aura);

    void OnApply(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
    {
        Player* player = GetTarget()->ToPlayer();
        if (!player)
            return;

        PayableSkin const* payableSkin = sPayableSkinMgr->GetPayableSkinById(aurEff->GetAmount());
        if (!payableSkin)
            return;

        player->AddTemporaryPayableSkin(aurEff->GetAmount(), GetSpellInfo()->Id);
        ChatHandler(player->GetSession()).PSendSysMessage(LANG_SPELL_SKIN_TEMPORARY_GOT, payableSkin->GetName());
    }

    void OnRemove(AuraEffect const* aurEff, AuraEffectHandleModes /*mode*/)
    {
        Player* player = GetTarget()->ToPlayer();
        if (!player)
            return;

        uint32 payableSkinId = aurEff->GetAmount();

        PlayerPayableSkin* playerPayableSkin = player->GetPayableSkin(payableSkinId);
        if (!playerPayableSkin || !playerPayableSkin->IsTemporary())
            return;

        playerPayableSkin->auras.erase(GetSpellInfo()->Id);

        if (playerPayableSkin->auras.size() == 0)
            player->RemovePayableSkin(payableSkinId);
    }

    void Register() override
    {
        OnEffectApply += AuraEffectApplyFn(spell_add_temporary_payable_skin_aura::OnApply, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
        OnEffectRemove += AuraEffectRemoveFn(spell_add_temporary_payable_skin_aura::OnRemove, EFFECT_0, SPELL_AURA_DUMMY, AURA_EFFECT_HANDLE_REAL);
    }
};

void AddSC_payable_skins()
{
    RegisterCreatureAI(npc_payable_skins_vendor);

    RegisterSpellScript(spell_add_payable_skin);
    RegisterSpellAndAuraScriptPair(spell_add_temporary_payable_skin, spell_add_temporary_payable_skin_aura);
}
