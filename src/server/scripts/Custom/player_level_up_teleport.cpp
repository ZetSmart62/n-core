#include "ScriptMgr.h"
#include "Language.h"
#include "WorldSession.h"
#include "Player.h"
#include "DBCStores.h"
#include "Chat.h"

using LevelLocationsContainer = std::unordered_map<uint32, std::vector<WorldLocation>>;

static std::unordered_map<TeamId, LevelLocationsContainer> const LevelLocationsStore =
{
    {
        TEAM_ALLIANCE,
        {
            {
                12,
                {
                    WorldLocation(0,  -10515.3f,    1063.43f,    55.5734f),  // Western Edge
                    WorldLocation(0,  -5356.67f,   -2948.41f,    323.93f),   // Lok Modan
                    WorldLocation(1,   6506.04f,    459.044f,    6.03483f),  // Dark shores
                    WorldLocation(0,  -9269.71f,   -2190.67f,    64.0896f)   // Krasnogorye
                }
            },
            {
                20,
                {
                    WorldLocation(1,   2751.78f,    -347.53f,     107.096f), // Ash Forest
                    WorldLocation(0,  -3779.87f,    -841.512f,    9.89763f), // Bolotina
                    WorldLocation(0,  -10558.2f,    -1194.97f,    28.1062f)  // Twilight Forest
                }
            },
            {
                30,
                {
                    WorldLocation(1,   -6230.62f,   -3923.3f,     -58.7739f), // A thousand needles
                    WorldLocation(0,   -1243.95f,   -2530.35f,     20.8248f)  // Arati Highlands
                }
            },
            {
                40,
                { 
                    WorldLocation(1,   -6916.19f,   -4794.05f,     8.15484f), // Tanaris
                    WorldLocation(0,    260.856f,   -2148.96f,     118.752f)  // Inland lands
                }
            },
            {
                46,
                {
                    WorldLocation(0,   -6512.35f,   -1173.98f,     309.175f)  // Smoldering Gorge
                }
            },
            {
                50,
                {
                    WorldLocation(0,   -10997.5f,   -3437.5f,      62.3037f), // Scorched Lands
                    WorldLocation(1,   -6152.08f,   -1080.32f,    -200.307f), // Un'goro Crater
                    WorldLocation(1,    6728.68f,   -4669.05f,     720.826f)  // Winter Keys
                }
            }
        }
    },
    {
        TEAM_HORDE,
        {
            {
                12,
                {
                    WorldLocation(1,   -453.644f,    -2645.29f,    95.4827f), // Steppes
                    WorldLocation(0,    505.127f,     1599.6f,     125.603f)  // Silver Boron
                }
            },
            {
                21,
                {
                    WorldLocation(1,    2318.56f,    -2532.08f,    100.865f), // Ash Forest
                    WorldLocation(0,   -35.0325f,    -923.611f,    54.5545f)  // Hillsbrad Foothills
                }
            },
            {
                30,
                {
                    WorldLocation(0,   -986.007f,    -3509.74f,    56.7551f), // Arati Highlands
                    WorldLocation(1,   -1410.84f,     1498.34f,    59.6481f), // Barren lands
                    WorldLocation(0,   -10436.3f,    -3263.92f,    20.179f),  // Swamps of Sadness
                    WorldLocation(1,   -3111.17f,    -2848.38f,    34.8015f)  // Dust swamps
                }
            },
            {
                40,
                {
                    WorldLocation(1,   -7172.96f,    -3797.43f,    8.38023f), // Tanaris
                    WorldLocation(1,   -4376.68f,     234.492f,    25.4136f)  // Feralas
                }
            },
            {
                50,
                {
                    WorldLocation(1,    6715.66f,    -4667.82f,    720.92f) // Winter Keys
                }
            }
        }
    }
};

class player_level_up_teleport : public PlayerScript
{
public:
    player_level_up_teleport() : PlayerScript("player_level_up_teleport") { }

    void OnLevelChanged(Player* player, uint8 /*oldLevel*/) override
    {
        auto factionItr = LevelLocationsStore.find(player->GetTeamId());
        if (factionItr == LevelLocationsStore.end())
            return;

        auto levelItr = factionItr->second.find(player->GetLevel());
        if (levelItr == factionItr->second.end())
            return;

        std::vector<WorldLocation> const& locations = levelItr->second;
        if (locations.empty())
            return;

        player->TeleportTo(Trinity::Containers::SelectRandomContainerElement(locations));
    }

    //bool TpLevelUpCheckAlliance(Player* player, uint8 /*level*/)
    /*{
        static std::unordered_map<uint32, WorldLocation> const level =
        {
            { 1,            WorldLocation(822, -10742.7f,   429.4f,     24.4f)  },
            { 2,            WorldLocation(823,  605.8f,     637.1f,     380.7f) },
            { 3,            WorldLocation(824,  1417.2f,    1276.6f,    33.0f)  },
            { 4,            WorldLocation(824,  1417.2f,    1276.6f,    33.0f)  },
            { 5,            WorldLocation(823,  605.8f,     637.1f,     380.7f) },
            { 6,            WorldLocation(822, -10742.7f,   429.4f,     24.4f)  },
            { 7,            WorldLocation(825,  3581.5f,    5547.1f,    324.3f) },
            { 8,            WorldLocation(732,  2784.4f,    5998.4f,    4.1f)   },
            { 9,            WorldLocation(732,  2784.4f,    5998.4f,    4.1f)   },
            { 10,           WorldLocation(825,  3581.f,     5547.1f,    324.3f) }
        };

        MapEntry const* mapEntry = sMapStore.LookupEntry(player->GetMapId());
        if (!mapEntry)
            return true;

        auto itr = level.find(player->GetLevel());
        if (itr == level.end())
            return true;

        player->TeleportTo(itr->second);
        return true;
    }*/

    void OnUpdateZone(Player* player, uint32 newZone, uint32 /*newArea*/) override
    {
        if (player->GetTeam() == ALLIANCE)
        {
            if (player->GetLevel() >= 12 && player->GetLevel() <= 19)
            {
                if (newZone != 40 && newZone != 38 && newZone != 148 && newZone != 44 && !player->InBattleground())
                {
                    player->CastSpell(player, 9454, true);
                }
            }
        }
        else
            if (player->GetLevel() >= 12 && player->GetLevel() <= 19)
                {
                if (newZone != 17 && newZone != 130)
                    {
                        player->CastSpell(player, 9454, true);
                    }
                }
    }
};

void AddSC_player_level_up_teleport()
{
    new player_level_up_teleport();
}
