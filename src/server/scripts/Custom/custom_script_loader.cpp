/*
 * This file is part of the TrinityCore Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

// This is where scripts' loading functions should be declared:

// The name of this function should match:
// void Add${NameOfDirectory}Scripts()
void AddSC_rank_system();
void AddSC_LearnSpellsOnLevelUp();
void AddSC_npc_server_menu();
void AddSC_player_server_menu();
void AddSC_payable_skins();
void AddSC_custom_commandscript();
void AddSC_pvp_stats_cache();
void AddSC_System_Censure();
void AddSC_player_level_up_teleport();

void AddCustomScripts()
{
    AddSC_rank_system();
    AddSC_LearnSpellsOnLevelUp();
    AddSC_npc_server_menu();
    AddSC_player_server_menu();
    AddSC_payable_skins();
    AddSC_custom_commandscript();
    AddSC_pvp_stats_cache();
    AddSC_System_Censure();
    AddSC_player_level_up_teleport();
}
