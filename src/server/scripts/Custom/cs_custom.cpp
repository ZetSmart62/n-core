#include "ScriptMgr.h"
#include "Chat.h"
#include "DatabaseEnv.h"
#include "Log.h"
#include "WorldSession.h"
#include "RankSystem.h"
#include "PayableSkinMgr.h"
#include "World.h"

using namespace Trinity::ChatCommands;

class custom_commandscript : public CommandScript
{
public:
    custom_commandscript() : CommandScript("custom_commandscript") { }

    ChatCommandTable GetCommands() const override
    {
        static ChatCommandTable customLookupCommandTable =
        {
            { "skin",       HandleCustomLookupSkinCommand,      LANG_COMMAND_CUSTOM_LOOKUP_SKIN_HELP,       rbac::RBAC_PERM_COMMAND_CUSTOM_LOOKUP_SKIN,     Console::Yes },
        };
        static ChatCommandTable customReloadCommandTable =
        {
            { "all",        HandleCustomReloadAllCommand,       LANG_COMMAND_CUSTOM_RELOAD_ALL_HELP,        rbac::RBAC_PERM_COMMAND_CUSTOM_RELOAD_ALL,      Console::Yes },
            { "ranks",      HandleCustomReloadRanksCommand,     LANG_COMMAND_CUSTOM_RELOAD_RANKS_HELP,      rbac::RBAC_PERM_COMMAND_CUSTOM_RELOAD_RANKS,    Console::Yes },
            { "skins",      HandleCustomReloadSkinsCommand,     LANG_COMMAND_CUSTOM_RELOAD_SKINS_HELP,      rbac::RBAC_PERM_COMMAND_CUSTOM_RELOAD_SKINS,    Console::Yes },
        };
        static ChatCommandTable customSkinCommandTable =
        {
            { "activate",   HandleCustomSkinActivateCommand,    LANG_COMMAND_CUSTOM_SKIN_ACTIVATE_HELP,     rbac::RBAC_PERM_COMMAND_CUSTOM_SKIN_ACTIVATE,   Console::Yes },
            { "add",        HandleCustomSkinAddCommand,         LANG_COMMAND_CUSTOM_SKIN_ADD_HELP,          rbac::RBAC_PERM_COMMAND_CUSTOM_SKIN_ADD,        Console::Yes },
            { "deactivate", HandleCustomSkinDeactivateCommand,  LANG_COMMAND_CUSTOM_SKIN_DEACTIVATE_HELP,   rbac::RBAC_PERM_COMMAND_CUSTOM_SKIN_DEACTIVATE, Console::Yes },
            { "list",       HandleCustomSkinListCommand,        LANG_COMMAND_CUSTOM_SKIN_LIST_HELP,         rbac::RBAC_PERM_COMMAND_CUSTOM_SKIN_LIST,       Console::Yes },
            { "remove",     HandleCustomSkinRemoveCommand,      LANG_COMMAND_CUSTOM_SKIN_REMOVE_HELP,       rbac::RBAC_PERM_COMMAND_CUSTOM_SKIN_REMOVE,     Console::Yes },
        };
        static ChatCommandTable customCommandTable =
        {
            { "lookup", customLookupCommandTable },
            { "reload", customReloadCommandTable },
            { "skin",   customSkinCommandTable   },
        };
        static ChatCommandTable commandTable =
        {
            { "custom", customCommandTable },
        };
        return commandTable;
    }

    /*****************************
     *           LOOKUP          *
     *****************************/

    static bool HandleCustomLookupSkinCommand(ChatHandler* handler, WTail wNamePart)
    {
        if (wNamePart.empty())
            return false;

        Player* target = handler->getSelectedPlayerOrSelf(); // can be nullptr at console call
        wstrToLower(wNamePart);

        bool found = false;
        uint32 count = 0;
        uint32 maxResults = sWorld->getIntConfig(CONFIG_MAX_RESULTS_LOOKUP_COMMANDS);

        for (auto const& [payableSkinId, payableSkin] : sPayableSkinMgr->GetPayableSkins())
        {
            if (!Utf8FitTo(payableSkin->GetName(false), wNamePart))
                continue;

            if (maxResults && count++ == maxResults)
            {
                handler->PSendSysMessage(LANG_COMMAND_LOOKUP_MAX_RESULTS, maxResults);
                return true;
            }

            char const* known = "";
            char const* temporary = "";
            char const* active = "";

            if (target)
            {
                PlayerPayableSkin const* playerPayableSkin = target->GetPayableSkin(payableSkinId);
                if (playerPayableSkin)
                {
                    known = handler->GetTrinityString(LANG_KNOWN);
                    if (playerPayableSkin->IsTemporary())
                        temporary = handler->GetTrinityString(LANG_PAYABLE_SKIN_TEMPORARY);
                }

                if (target->GetActivePayableSkin() == payableSkinId)
                    active = handler->GetTrinityString(LANG_PAYABLE_SKIN_ACTIVE);
            }

            handler->PSendSysMessage(LANG_COMMAND_PAYABLE_SKIN, payableSkinId, payableSkin->GetName(!handler->IsConsole()).c_str(), known, temporary, active);

            if (!found)
                found = true;
        }

        if (!found)
            handler->SendSysMessage(LANG_COMMAND_NO_PAYABLE_SKINS_FOUND);

        return true;
    }

    /*****************************
     *           RELOAD          *
     *****************************/

    static bool HandleCustomReloadAllCommand(ChatHandler* handler)
    {
        HandleCustomReloadRanksCommand(handler);
        HandleCustomReloadSkinsCommand(handler);

        return true;
    }

    static bool HandleCustomReloadRanksCommand(ChatHandler* handler)
    {
        TC_LOG_INFO("misc", "Re-Loading rank system data...");
        sRankMgr->LoadFromDB();
        handler->SendGlobalGMSysMessage("Rank system data reloaded.");
        return true;
    }

    static bool HandleCustomReloadSkinsCommand(ChatHandler* handler)
    {
        TC_LOG_INFO("misc", "Re-Loading Payable skins...");
        sPayableSkinMgr->LoadFromDB();
        handler->SendGlobalGMSysMessage("Payable skins data reloaded.");
        return true;
    }

    /*****************************
     *        PAYABLE SKINS      *
     *****************************/

    static bool HandleCustomSkinActivateCommand(ChatHandler* handler, Optional<PlayerIdentifier> player, uint32 payableSkinId)
    {
        if (!player)
            player = PlayerIdentifier::FromTargetOrSelf(handler);
        if (!player)
        {
            handler->SendSysMessage(LANG_PLAYER_NOT_FOUND);
            handler->SetSentErrorMessage(true);
            return false;
        }

        PayableSkin const* payableSkin = sPayableSkinMgr->GetPayableSkinById(payableSkinId);
        if (!payableSkin)
        {
            handler->PSendSysMessage(LANG_COMMAND_PAYABLE_SKIN_NOT_FOUND, GetErrorString(handler), payableSkinId);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (Player* target = player->GetConnectedPlayer())
        {
            if (!target->GetPayableSkin(payableSkinId))
            {
                handler->PSendSysMessage(LANG_COMMAND_PLAYER_HAVE_NOT_PAYABLE_SKIN, GetErrorString(handler), handler->GetNameLink(target).c_str(), payableSkin->ToString(!handler->IsConsole()).c_str());
                handler->SetSentErrorMessage(true);
                return false;
            }

            if (target->GetActivePayableSkin() == payableSkinId)
            {
                handler->PSendSysMessage(LANG_COMMAND_PAYABLE_SKIN_ALREADY_ACTIVE, GetErrorString(handler), payableSkin->ToString(!handler->IsConsole()).c_str(), handler->GetNameLink(target).c_str());
                handler->SetSentErrorMessage(true);
                return false;
            }

            target->ActivatePayableSkin(payableSkinId);

            if (handler->needReportToTarget(target))
                ChatHandler(target->GetSession()).PSendSysMessage(LANG_COMMAND_YOURS_PAYABLE_SKIN_ACTIVATED, handler->GetNameLink().c_str(), payableSkin->ToString(!handler->IsConsole()).c_str());
        }
        else
        {
            //                                                      0
            QueryResult result = CharacterDatabase.PQuery("SELECT active FROM character_payable_skins WHERE guid = %u AND payableSkin = %u", player->GetGUID().GetCounter(), payableSkinId);
            if (!result)
            {
                handler->PSendSysMessage(LANG_COMMAND_PLAYER_HAVE_NOT_PAYABLE_SKIN, GetErrorString(handler), handler->playerLink(*player).c_str(), payableSkin->ToString(!handler->IsConsole()).c_str());
                handler->SetSentErrorMessage(true);
                return false;
            }

            if ((*result)[0].GetBool())
            {
                handler->PSendSysMessage(LANG_COMMAND_PAYABLE_SKIN_ALREADY_ACTIVE, GetErrorString(handler), payableSkin->ToString(!handler->IsConsole()).c_str(), handler->playerLink(*player).c_str());
                handler->SetSentErrorMessage(true);
                return false;
            }

            CharacterDatabaseTransaction trans = CharacterDatabase.BeginTransaction();
            trans->PAppend("UPDATE character_payable_skins SET active = 0 WHERE guid = %u", player->GetGUID().GetCounter());
            trans->PAppend("UPDATE character_payable_skins SET active = 1 WHERE guid = %u AND payableSkin = %u", player->GetGUID().GetCounter(), payableSkinId);
            CharacterDatabase.CommitTransaction(trans);
        }

        handler->PSendSysMessage(LANG_COMMAND_YOU_ACTIVATE_PAYABLE_SKIN, GetSuccessString(handler), payableSkin->ToString(!handler->IsConsole()).c_str(), handler->playerLink(*player).c_str());

        return true;
    }

    static bool HandleCustomSkinAddCommand(ChatHandler* handler, Optional<EXACT_SEQUENCE("force")> force, Optional<PlayerIdentifier> player, uint32 payableSkinId)
    {
        if (!player)
            player = PlayerIdentifier::FromTargetOrSelf(handler);
        if (!player)
        {
            handler->SendSysMessage(LANG_PLAYER_NOT_FOUND);
            handler->SetSentErrorMessage(true);
            return false;
        }

        PayableSkin const* payableSkin = sPayableSkinMgr->GetPayableSkinById(payableSkinId);
        if (!payableSkin)
        {
            handler->PSendSysMessage(LANG_COMMAND_PAYABLE_SKIN_NOT_FOUND, GetErrorString(handler), payableSkinId);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (!force && !payableSkin->IsAllowedFor(player->GetGUID()))
        {
            handler->PSendSysMessage(LANG_COMMAND_PAYABLE_SKIN_NOT_ALLOWED, GetErrorString(handler), payableSkin->ToString(!handler->IsConsole()).c_str(), handler->playerLink(*player).c_str());
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (Player* target = player->GetConnectedPlayer())
        {
            PlayerPayableSkin const* playerPayableSkin = target->GetPayableSkin(payableSkinId);
            if (playerPayableSkin && !playerPayableSkin->IsTemporary())
            {
                handler->PSendSysMessage(LANG_COMMAND_PLAYER_ALREADY_HAVE_PAYABLE_SKIN, GetErrorString(handler), handler->GetNameLink(target).c_str(), payableSkin->ToString(!handler->IsConsole()).c_str());
                handler->SetSentErrorMessage(true);
                return false;
            }

            target->AddPayableSkin(payableSkinId);

            if (handler->needReportToTarget(target))
                ChatHandler(target->GetSession()).PSendSysMessage(LANG_COMMAND_ADDED_PAYABLE_SKIN_TO_YOU, handler->GetNameLink().c_str(), payableSkin->ToString(!handler->IsConsole()).c_str());
        }
        else
        {
            QueryResult result = CharacterDatabase.PQuery("SELECT 1 FROM character_payable_skins WHERE guid = %u AND payableSkin = %u", player->GetGUID().GetCounter(), payableSkinId);
            if (result)
            {
                handler->PSendSysMessage(LANG_COMMAND_PLAYER_ALREADY_HAVE_PAYABLE_SKIN, GetErrorString(handler), handler->playerLink(*player).c_str(), payableSkin->ToString(!handler->IsConsole()).c_str());
                handler->SetSentErrorMessage(true);
                return false;
            }

            CharacterDatabasePreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_CHAR_PAYABLE_SKINS);
            stmt->setUInt32(0, player->GetGUID().GetCounter());
            stmt->setUInt32(1, payableSkinId);
            stmt->setBool  (2, false);
            CharacterDatabase.Execute(stmt);
        }

        handler->PSendSysMessage(LANG_COMMAND_YOU_ADDED_PAYABLE_SKIN_TO_PLAYER, GetSuccessString(handler), payableSkin->ToString(!handler->IsConsole()).c_str(), handler->playerLink(*player).c_str());

        return true;
    }

    static bool HandleCustomSkinDeactivateCommand(ChatHandler* handler, Optional<PlayerIdentifier> player)
    {
        if (!player)
            player = PlayerIdentifier::FromTargetOrSelf(handler);
        if (!player)
        {
            handler->SendSysMessage(LANG_PLAYER_NOT_FOUND);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (Player* target = player->GetConnectedPlayer())
        {
            if (!target->GetActivePayableSkin())
            {
                handler->PSendSysMessage(LANG_COMMAND_PLAYER_HAVE_NOT_ACTIVE_SKIN, GetErrorString(handler), handler->GetNameLink(target).c_str());
                handler->SetSentErrorMessage(true);
                return false;
            }

            target->DeactivatePayableSkin(true);

            if (handler->needReportToTarget(target))
                ChatHandler(target->GetSession()).PSendSysMessage(LANG_COMMAND_YOUR_PAYABLE_SKIN_DEACTIVATED, handler->GetNameLink().c_str());
        }
        else
        {
            QueryResult result = CharacterDatabase.PQuery("SELECT 1 FROM character_payable_skins WHERE guid = %u AND active = 1", player->GetGUID().GetCounter());
            if (!result)
            {
                handler->PSendSysMessage(LANG_COMMAND_PLAYER_HAVE_NOT_ACTIVE_SKIN, GetErrorString(handler), handler->playerLink(*player).c_str());
                handler->SetSentErrorMessage(true);
                return false;
            }

            CharacterDatabase.PExecute("UPDATE character_payable_skins SET active = 0 WHERE guid = %u", player->GetGUID().GetCounter());
        }

        handler->PSendSysMessage(LANG_COMMAND_YOU_DEACTIVATED_PLAYER_SKIN, GetSuccessString(handler), handler->playerLink(*player).c_str());

        return true;
    }

    static bool HandleCustomSkinListCommand(ChatHandler* handler, Optional<PlayerIdentifier> player)
    {
        if (!player)
            player = PlayerIdentifier::FromTargetOrSelf(handler);
        if (!player)
        {
            handler->SendSysMessage(LANG_PLAYER_NOT_FOUND);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (Player* target = player->GetConnectedPlayer())
        {
            if (!target->HasPayableSkins())
            {
                handler->PSendSysMessage(LANG_COMMAND_PLAYER_HAVE_NOT_ANY_SKINS, handler->GetNameLink(target).c_str());
                return true;
            }

            handler->PSendSysMessage(LANG_COMMAND_LIST_PLAYER_SKINS, handler->GetNameLink(target).c_str());

            for (auto const& [payableSkinId, playerPayableSkin] : target->GetPayableSkins())
            {
                PayableSkin const* payableSkin = sPayableSkinMgr->GetPayableSkinById(payableSkinId);
                if (!payableSkin)
                    continue;

                handler->PSendSysMessage(LANG_COMMAND_PLAYER_SKIN,
                    payableSkin->ToString(!handler->IsConsole()).c_str(), playerPayableSkin.IsTemporary() ? handler->GetTrinityString(LANG_PAYABLE_SKIN_TEMPORARY) : "",
                    target->GetActivePayableSkin() == payableSkinId ? handler->GetTrinityString(LANG_PAYABLE_SKIN_ACTIVE) : "");
            }
        }
        else
        {
            QueryResult result = CharacterDatabase.PQuery("SELECT payableSkin, active FROM character_payable_skins WHERE guid = %u", player->GetGUID().GetCounter());
            if (!result)
            {
                handler->PSendSysMessage(LANG_COMMAND_PLAYER_HAVE_NOT_ANY_SKINS, handler->playerLink(*player).c_str());
                return true;
            }

            handler->PSendSysMessage(LANG_COMMAND_LIST_PLAYER_SKINS, handler->playerLink(*player).c_str());

            do
            {
                Field* fields        = result->Fetch();
                uint32 payableSkinId = fields[0].GetUInt32();
                bool   active        = fields[1].GetBool();

                PayableSkin const* payableSkin = sPayableSkinMgr->GetPayableSkinById(payableSkinId);
                if (!payableSkin)
                    continue;

                handler->PSendSysMessage(LANG_COMMAND_PLAYER_SKIN,
                    payableSkin->ToString(!handler->IsConsole()).c_str(), active ? handler->GetTrinityString(LANG_PAYABLE_SKIN_ACTIVE) : "");
            } while (result->NextRow());
        }

        return true;
    }

    static bool HandleCustomSkinRemoveCommand(ChatHandler* handler, Optional<PlayerIdentifier> player, uint32 payableSkinId)
    {
        if (!player)
            player = PlayerIdentifier::FromTargetOrSelf(handler);
        if (!player)
        {
            handler->SendSysMessage(LANG_PLAYER_NOT_FOUND);
            handler->SetSentErrorMessage(true);
            return false;
        }

        PayableSkin const* payableSkin = sPayableSkinMgr->GetPayableSkinById(payableSkinId);
        if (!payableSkin)
        {
            handler->PSendSysMessage(LANG_COMMAND_PAYABLE_SKIN_NOT_FOUND, GetErrorString(handler), payableSkinId);
            handler->SetSentErrorMessage(true);
            return false;
        }

        if (Player* target = player->GetConnectedPlayer())
        {
            if (!target->GetPayableSkin(payableSkinId))
            {
                handler->PSendSysMessage(LANG_COMMAND_PLAYER_HAVE_NOT_PAYABLE_SKIN, GetErrorString(handler), handler->GetNameLink(target).c_str(), payableSkin->ToString(!handler->IsConsole()).c_str());
                handler->SetSentErrorMessage(true);
                return false;
            }

            target->RemovePayableSkin(payableSkinId);

            if (handler->needReportToTarget(target))
                ChatHandler(target->GetSession()).PSendSysMessage(LANG_COMMAND_YOUR_PAYABLE_SKIN_REMOVED, handler->GetNameLink().c_str(), payableSkin->ToString(!handler->IsConsole()).c_str());
        }
        else
        {
            QueryResult result = CharacterDatabase.PQuery("SELECT 1 FROM character_payable_skins WHERE guid = %u AND payableSkin = %u", player->GetGUID().GetCounter(), payableSkinId);
            if (!result)
            {
                handler->PSendSysMessage(LANG_COMMAND_PLAYER_HAVE_NOT_PAYABLE_SKIN, GetErrorString(handler), handler->playerLink(*player).c_str(), payableSkin->ToString(!handler->IsConsole()).c_str());
                handler->SetSentErrorMessage(true);
                return false;
            }

            CharacterDatabase.PExecute("DELETE FROM character_payable_skins WHERE guid = %u AND payableSkin = %u", player->GetGUID().GetCounter(), payableSkinId);
        }

        handler->PSendSysMessage(LANG_COMMAND_YOU_REMOVED_PAYABLE_SKIN, GetSuccessString(handler), payableSkin->ToString(!handler->IsConsole()).c_str(), handler->playerLink(*player).c_str());

        return true;
    }

    /*****************************
     *           HELPERS         *
     *****************************/

    static char const* GetSuccessString(ChatHandler* handler)
    {
        return handler->GetTrinityString(handler->IsConsole() ? LANG_COMMAND_SUCCESS : LANG_COMMAND_SUCCESS_COLORED);
    }

    static char const* GetErrorString(ChatHandler* handler)
    {
        return handler->GetTrinityString(handler->IsConsole() ? LANG_COMMAND_ERROR : LANG_COMMAND_ERROR_COLORED);
    }
};

void AddSC_custom_commandscript()
{
    new custom_commandscript();
}
